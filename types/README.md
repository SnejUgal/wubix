# Types

Just a crate that shares types between the kernel and `librust`.

## Building

```bash
wasm-pack build
```

Note that you don‘t need to build `types` separately, as `cargo` will build
it when building the kernel and `librust`. Running the above command is only
useful during development to check if `types` compiles.
