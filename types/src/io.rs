use serde::{Deserialize, Serialize};
use std::fmt::{self, Display, Formatter};

pub type IoResult<T> = Result<T, IoErrorKind>;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum IoErrorKind {
    NotFound,
    AlreadyExists,
    IsDirectory,
    NotDirectory,
    PermissionDenied,
    InvalidOffset,
    CorruptedExecutable,
    InvalidArgument,
    UnsupportedOperation,
    DeviceBusy,
    NotEmpty,
    CrossDevice,
    NoSpace,
}

impl Display for IoErrorKind {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        formatter.write_str(match self {
            Self::AlreadyExists => "Already exists",
            Self::CorruptedExecutable => "Corrupted executable",
            Self::InvalidArgument => "Invalid argument",
            Self::InvalidOffset => "Invalid offset",
            Self::IsDirectory => "Is a directory",
            Self::NotDirectory => "Not a directory",
            Self::NotFound => "Not found",
            Self::PermissionDenied => "Permission denied",
            Self::UnsupportedOperation => "Unsupported operation",
            Self::DeviceBusy => "Device busy",
            Self::NotEmpty => "Not empty",
            Self::CrossDevice => "Cross device",
            Self::NoSpace => "No space left on device",
        })
    }
}

impl From<IoErrorKind> for std::io::Error {
    fn from(from: IoErrorKind) -> Self {
        match from {
            IoErrorKind::AlreadyExists => std::io::ErrorKind::AlreadyExists.into(),
            IoErrorKind::CorruptedExecutable => std::io::ErrorKind::InvalidData.into(),
            IoErrorKind::InvalidArgument => std::io::ErrorKind::InvalidInput.into(),
            IoErrorKind::NotFound => std::io::ErrorKind::NotFound.into(),
            IoErrorKind::PermissionDenied => std::io::ErrorKind::PermissionDenied.into(),
            IoErrorKind::InvalidOffset
            | IoErrorKind::IsDirectory
            | IoErrorKind::NotDirectory
            | IoErrorKind::UnsupportedOperation
            | IoErrorKind::DeviceBusy
            | IoErrorKind::NotEmpty
            | IoErrorKind::CrossDevice
            | IoErrorKind::NoSpace => {
                std::io::Error::new(std::io::ErrorKind::Other, from.to_string())
            }
        }
    }
}
