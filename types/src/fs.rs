use crate::ids::{DeviceId, GroupId, Inode, UserId};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct ClassPermissions {
    pub can_read: bool,
    pub can_write: bool,
    pub can_execute: bool,
}

impl ClassPermissions {
    pub const ALL: Self = ClassPermissions {
        can_read: true,
        can_write: true,
        can_execute: true,
    };

    pub const READ_WRITE: Self = ClassPermissions {
        can_read: true,
        can_write: true,
        can_execute: false,
    };

    pub const READ_ONLY: Self = ClassPermissions {
        can_read: true,
        can_write: false,
        can_execute: false,
    };

    pub const READ_ONLY_EXECUTABLE: Self = ClassPermissions {
        can_read: true,
        can_write: false,
        can_execute: true,
    };

    pub const WRITE_ONLY: Self = ClassPermissions {
        can_read: false,
        can_write: true,
        can_execute: false,
    };

    pub const NONE: Self = ClassPermissions {
        can_read: false,
        can_write: false,
        can_execute: false,
    };
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Permissions {
    pub set_user_id: bool,
    pub set_group_id: bool,
    pub is_sticky: bool,
    pub owner: ClassPermissions,
    pub group: ClassPermissions,
    pub others: ClassPermissions,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum EntryKind {
    File,
    Directory,
    Pipe,
    CharacterDevice,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Metadata {
    pub kind: EntryKind,
    pub device: DeviceId,
    pub inode: Inode,
    pub owner_id: UserId,
    pub group_id: GroupId,
    pub permissions: Permissions,
    pub size: usize,
}

impl Metadata {
    pub fn can_read(self, user_id: UserId, group_id: GroupId) -> bool {
        if user_id == UserId(0) {
            true
        } else if self.owner_id == user_id {
            self.permissions.owner.can_read
        } else if self.group_id == group_id {
            self.permissions.group.can_read
        } else {
            self.permissions.others.can_read
        }
    }

    pub fn can_write(self, user_id: UserId, group_id: GroupId) -> bool {
        if user_id == UserId(0) {
            true
        } else if self.owner_id == user_id {
            self.permissions.owner.can_write
        } else if self.group_id == group_id {
            self.permissions.group.can_write
        } else {
            self.permissions.others.can_write
        }
    }

    pub fn can_execute(self, user_id: UserId, group_id: GroupId) -> bool {
        if self.owner_id == user_id {
            self.permissions.owner.can_execute
        } else if self.group_id == group_id {
            self.permissions.group.can_execute
        } else {
            self.permissions.others.can_execute
        }
    }
}
