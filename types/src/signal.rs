use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum Signal {
    Hangup = 1,
    Interrupt = 2,
    IllegalInstruction = 4,
    Kill = 9,
    User1 = 10,
    User2 = 12,
    Pipe = 13,
    Terminate = 15,
    Child = 17,
    WindowChange = 28,
}
