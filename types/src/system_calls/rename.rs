use crate::io::IoResult;

pub type RenameResult = IoResult<()>;
