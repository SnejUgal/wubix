use crate::io::IoResult;

pub type ReadResult = IoResult<Vec<u8>>;
