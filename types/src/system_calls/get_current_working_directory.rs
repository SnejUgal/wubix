use crate::io::IoResult;

pub type GetCurrentWorkingDirectoryResult = IoResult<Vec<u8>>;
