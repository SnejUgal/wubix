use crate::io::IoResult;

pub type RemoveDirectoryResult = IoResult<()>;
