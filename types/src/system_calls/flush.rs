use crate::io::IoResult;

pub type FlushResult = IoResult<()>;
