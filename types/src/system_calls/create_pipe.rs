use crate::{ids::FileDescriptorId, io::IoResult};

pub type CreatePipeResult = IoResult<(FileDescriptorId, FileDescriptorId)>;
