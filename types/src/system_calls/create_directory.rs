use crate::io::IoResult;

pub type CreateDirectoryResult = IoResult<()>;
