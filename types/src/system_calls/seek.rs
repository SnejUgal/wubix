use crate::io::IoResult;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub enum SeekFrom {
    Start(usize),
    Current(isize),
    End(isize),
}

pub type SeekResult = IoResult<usize>;
