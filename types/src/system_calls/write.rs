use crate::io::IoResult;

pub type WriteResult = IoResult<usize>;
