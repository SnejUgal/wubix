use crate::io::IoResult;

pub type CreateHardLinkResult = IoResult<()>;
