use crate::{fs::Metadata, io::IoResult};

pub type ReadMetadataResult = IoResult<Metadata>;
