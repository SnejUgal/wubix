use crate::io::IoResult;

pub type SetCurrentWorkingDirectoryResult = IoResult<()>;
