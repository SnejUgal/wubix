use crate::io::IoResult;

pub type CloseResult = IoResult<()>;
