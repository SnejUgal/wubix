use serde::{Deserialize, Serialize};

use crate::{
    ids::{ProcessGroupId, ProcessId},
    io::IoResult,
    signal::Signal,
};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Parameters {
    pub destination: Destination,
    pub signal: Option<Signal>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum Destination {
    Process(ProcessId),
    ProcessGroup(ProcessGroupId),
    MyProcessGroup,
    All,
}

pub type Result = IoResult<()>;
