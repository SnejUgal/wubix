use crate::io::IoResult;

pub type GetExecutablePathResult = IoResult<Vec<u8>>;
