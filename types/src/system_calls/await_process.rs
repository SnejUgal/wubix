use crate::{
    ids::{ProcessGroupId, ProcessId},
    io::IoResult,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Parameters {
    pub wait_for: WaitFor,
    pub only_try: bool,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum WaitFor {
    Process(ProcessId),
    ProcessGroup(ProcessGroupId),
    AnyChild,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct ExitedProcess {
    pub process_id: ProcessId,
    pub exit_code: i32,
}

pub type Result = IoResult<Option<ExitedProcess>>;
