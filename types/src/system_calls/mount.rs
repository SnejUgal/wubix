use crate::io::IoResult;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub enum FileSystem {
    Tmpfs,
    Devfs,
    Idbfs {
        id: String,
    }
}

#[derive(Serialize, Deserialize)]
pub struct MountOptions {
    pub file_system: FileSystem,
    pub should_remount: bool,
}

pub type MountResult = IoResult<()>;
