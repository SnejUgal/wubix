use crate::{
    ids::{FileDescriptorId, GroupId, ProcessId, UserId},
    io::IoResult,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type ExecuteResult = IoResult<ProcessId>;

#[derive(Serialize, Deserialize, Clone)]
pub struct ExecuteParameters {
    pub executable: Vec<u8>,
    pub arguments: Vec<Vec<u8>>,
    pub environment_variables: HashMap<Vec<u8>, Vec<u8>>,
    pub stdin: FileDescriptorId,
    pub stdout: FileDescriptorId,
    pub stderr: FileDescriptorId,
    pub user_id: Option<UserId>,
    pub group_id: Option<GroupId>,
    pub working_directory: Vec<u8>,
}
