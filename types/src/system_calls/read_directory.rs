use crate::{fs::Metadata, io::IoResult};
use std::collections::HashMap;

pub type ReadDirectoryResult = IoResult<HashMap<Vec<u8>, Metadata>>;
