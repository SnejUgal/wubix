use crate::io::IoResult;

pub type CanonicalizeResult = IoResult<Vec<u8>>;
