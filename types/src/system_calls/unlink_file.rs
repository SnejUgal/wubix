use crate::io::IoResult;

pub type UnlinkFileResult = IoResult<()>;
