use crate::{fs::Permissions, ids::FileDescriptorId, io::IoResult};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct CreateOptions {
    pub permissions: Permissions,
    pub create_only: bool,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Default, Serialize, Deserialize)]
pub struct WriteOptions {
    pub should_append: bool,
    pub should_truncate: bool,
    pub create_options: Option<CreateOptions>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum OpenMode {
    ReadOnly,
    WriteOnly(WriteOptions),
    ReadWrite(WriteOptions),
}

pub type OpenResult = IoResult<FileDescriptorId>;
