use crate::{ids::FileDescriptorId, io::IoResult};

pub type DuplicateResult = IoResult<FileDescriptorId>;
