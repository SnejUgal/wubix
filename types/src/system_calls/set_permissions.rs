use crate::io::IoResult;

pub type SetPermissionsResult = IoResult<()>;
