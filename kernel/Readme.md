# Wubix kernel

This directory contains Wubix's kernel. It manages devices, processes,
the file system, and virtual terminals.

## Buidling

```bash
wasm-pack build
```

If `wasm-pack` is not installed, run `cargo install wasm-pack`.
