use crate::{
    file_system::{self, DevFileSystem, FileDescriptor},
    frame_buffer::{FrameBuffer, FrameBufferView},
    util::ungzip,
    virtual_terminal::{VirtualTerminal, VirtualTerminals},
};
use js_sys::Promise;
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    panic::{set_hook, PanicInfo},
    rc::Rc,
};
use types::{
    ids::{GroupId, ProcessId, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::open::OpenMode,
};
use unix_path::{Path, PathBuf};
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;

mod virtual_file_system;
use virtual_file_system::VirtualFileSystem;

mod process;
use process::{Process, ProcessParameters};

mod system_calls;

mod keyboard;
use keyboard::KeyboardState;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = kernel)]
    fn now() -> f64;

    #[wasm_bindgen(js_namespace = kernel)]
    fn panic(message: String);

    #[wasm_bindgen(js_namespace = kernel, js_name = startProcess)]
    fn start_process(
        process_id: u32,
        executable: Vec<u8>,
        arguments: JsValue,
        variables: JsValue,
    ) -> Promise;

    #[wasm_bindgen(js_namespace = kernel, js_name = terminateProcess)]
    fn terminate_process(process_id: u32);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    pub fn console_log(message: String);
}

#[wasm_bindgen]
pub struct Parameters {
    screen_width: usize,
    screen_height: usize,
    screen_dpi: usize,
    initramfs: file_system::Any,
}

#[wasm_bindgen]
pub struct Kernel {
    frame_buffer: Rc<RefCell<FrameBuffer>>,
    virtual_terminals: VirtualTerminals,
    active_virtual_terminal: Rc<Cell<usize>>,
    virtual_file_system: VirtualFileSystem,
    processes: RefCell<HashMap<ProcessId, Rc<Process>>>,
    next_process_id: Cell<ProcessId>,
    keyboard_state: Cell<KeyboardState>,
}

#[wasm_bindgen]
#[derive(Clone)]
pub struct KernelRef {
    kernel: Rc<Kernel>,
}

#[wasm_bindgen]
impl Parameters {
    pub fn new(
        screen_width: usize,
        screen_height: usize,
        screen_dpi: usize,
        initramfs: file_system::AnyForWasm,
    ) -> Self {
        Self {
            screen_width,
            screen_height,
            screen_dpi,
            initramfs: initramfs.inner,
        }
    }
}

#[wasm_bindgen]
impl Kernel {
    pub fn new(parameters: Parameters) -> Self {
        set_hook(Box::new(panic_hook));

        let screen_size = (parameters.screen_width, parameters.screen_height);
        let dpi = parameters.screen_dpi.max(1);
        let frame_buffer = FrameBuffer::new(screen_size);
        let virtual_terminal = VirtualTerminal::new(screen_size, dpi);
        let active_virtual_terminal = Rc::new(Cell::new(0));

        let frame_buffer = Rc::new(RefCell::new(frame_buffer));
        let virtual_terminal = Rc::new(RefCell::new(virtual_terminal));
        let virtual_terminals = Rc::new(RefCell::new(vec![virtual_terminal]));

        let devfs = Rc::new(DevFileSystem::new(
            Rc::clone(&virtual_terminals),
            Rc::clone(&active_virtual_terminal),
            Rc::clone(&frame_buffer),
        ));

        Self {
            frame_buffer,
            virtual_terminals,
            active_virtual_terminal,
            virtual_file_system: VirtualFileSystem::new(parameters.initramfs, devfs),
            processes: RefCell::new(HashMap::new()),
            next_process_id: Cell::new(ProcessId(2)),
            keyboard_state: Cell::new(KeyboardState::default()),
        }
    }

    #[wasm_bindgen(js_name = getFrameBuffer)]
    pub fn get_frame_buffer(&self) -> FrameBufferView {
        self.frame_buffer.borrow().get_view()
    }

    #[wasm_bindgen(js_name = intoRef)]
    pub fn into_ref(self) -> KernelRef {
        KernelRef {
            kernel: Rc::new(self),
        }
    }

    fn panic(&self, message: &str, stack: Option<&str>) {
        let active_terminal = self.active_virtual_terminal.get();
        let terminals = self.virtual_terminals.borrow();
        let tty = &mut *terminals[active_terminal].borrow_mut();

        printk!(tty, self.frame_buffer; "Kernel {}", message);

        if let Some(stack) = stack {
            printk!(tty, self.frame_buffer; "Call trace:");
            for line in stack.lines() {
                printk!(tty, self.frame_buffer; "\t{}", line);
            }
        }
    }

    async fn start_init_system(&self) -> IoResult<()> {
        let descriptor = self
            .virtual_file_system
            .open(
                Path::new("/bin/init"),
                UserId(0),
                GroupId(0),
                OpenMode::ReadOnly,
            )
            .await?;
        let metadata = descriptor.read_metadata().await?;
        if !metadata.can_execute(UserId(0), GroupId(0)) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let gzipped_wef = descriptor.read(metadata.size).await?;
        let wef = ungzip(&gzipped_wef)?;
        let tty = self
            .virtual_file_system
            .open(
                Path::new("/dev/tty1"),
                UserId(0),
                GroupId(0),
                OpenMode::ReadWrite(Default::default()),
            )
            .await
            .unwrap();

        let arguments = serde_wasm_bindgen::to_value(&[b"/bin/init"]).unwrap();
        let variables = serde_wasm_bindgen::to_value(&HashMap::<Vec<u8>, Vec<u8>>::new()).unwrap();
        let init_future: JsFuture = start_process(1, wef, arguments, variables).into();
        init_future
            .await
            .or(Err(IoErrorKind::CorruptedExecutable))?;
        self.processes.borrow_mut().insert(
            ProcessId(1),
            Rc::new(Process::new(ProcessParameters {
                executable_path: PathBuf::from("/bin/init"),
                user_id: UserId(0),
                group_id: GroupId(0),
                stdin: tty.clone(),
                stdout: tty.clone(),
                stderr: tty,
                working_directory: "/".into(),
                parent_id: ProcessId(0),
            })),
        );

        Ok(())
    }

    fn get_process(&self, process_id: ProcessId) -> IoResult<Rc<Process>> {
        let processes_lock = self.processes.borrow();
        let process = processes_lock
            .get(&process_id)
            .ok_or(IoErrorKind::InvalidArgument)?;
        Ok(Rc::clone(process))
    }
}

#[wasm_bindgen]
impl KernelRef {
    pub fn clone(&self) -> Self {
        Clone::clone(self)
    }

    pub fn panic(&self, message: String, stack: String) {
        self.kernel.panic(&message, Some(&stack));
    }

    pub async fn init(self) {
        printk!(self.kernel; "Starting Wubix");

        if let Err(error) = self.kernel.start_init_system().await {
            panic!("IO error '{}' while starting /bin/init", error);
        }
    }

    #[wasm_bindgen(js_name = blinkInterrupt)]
    pub fn blink_interrupt(&self) {
        let active_terminal = self.kernel.active_virtual_terminal.get();
        let terminals = self.kernel.virtual_terminals.borrow();
        let mut terminal = terminals[active_terminal].borrow_mut();
        terminal.blink(self.kernel.frame_buffer.borrow_mut().lock());
    }
}

fn panic_hook(panic_info: &PanicInfo) {
    panic(panic_info.to_string());
}
