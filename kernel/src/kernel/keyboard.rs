use std::borrow::Cow;
use wasm_bindgen::prelude::*;

use super::{Kernel, KernelRef};
use crate::virtual_terminal::VirtualTerminal;

#[wasm_bindgen]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum KeyEventKind {
    Up = 0,
    Down = 1,
    Hold = 2,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
pub struct KeyboardState {
    is_left_ctrl_down: bool,
    is_right_ctrl_down: bool,
    is_left_shift_down: bool,
    is_right_shift_down: bool,
    has_caps_lock: bool,
    has_num_lock: bool,
}

impl KeyboardState {
    fn is_shift_down(self) -> bool {
        self.is_left_shift_down || self.is_right_shift_down
    }

    fn is_ctrl_down(self) -> bool {
        self.is_left_ctrl_down || self.is_right_ctrl_down
    }

    fn emits_lowercase(self) -> bool {
        self.has_caps_lock == self.is_shift_down()
    }

    fn numpad_emits_characters(self) -> bool {
        self.has_num_lock == self.is_shift_down()
    }
}

impl Kernel {
    pub fn handle_key_event(&self, kind: KeyEventKind, key: &str) {
        let keyboard_state = self.keyboard_state.get();
        let new_keyboard_state = match key {
            "ControlLeft" => Some(KeyboardState {
                is_left_ctrl_down: kind != KeyEventKind::Up,
                ..keyboard_state
            }),
            "ControlRight" => Some(KeyboardState {
                is_right_ctrl_down: kind != KeyEventKind::Up,
                ..keyboard_state
            }),
            "ShiftLeft" => Some(KeyboardState {
                is_left_shift_down: kind != KeyEventKind::Up,
                ..keyboard_state
            }),
            "ShiftRight" => Some(KeyboardState {
                is_right_shift_down: kind != KeyEventKind::Up,
                ..keyboard_state
            }),
            "CapsLock" if kind == KeyEventKind::Down => Some(KeyboardState {
                has_caps_lock: !keyboard_state.has_caps_lock,
                ..keyboard_state
            }),
            "NumLock" if kind == KeyEventKind::Down => Some(KeyboardState {
                has_num_lock: !keyboard_state.has_num_lock,
                ..keyboard_state
            }),
            _ => None,
        };

        if let Some(state) = new_keyboard_state {
            self.keyboard_state.set(state);
            return;
        }

        if kind == KeyEventKind::Up {
            return;
        }

        let active_terminal = self.active_virtual_terminal.get();
        let terminals = self.virtual_terminals.borrow();
        let mut terminal = terminals[active_terminal].borrow_mut();
        let frame_buffer = &self.frame_buffer;

        let is_shift_down = keyboard_state.is_left_shift_down || keyboard_state.is_right_shift_down;
        if is_shift_down && (key == "PageUp" || key == "PageDown") {
            let scroll = if key == "PageUp" {
                VirtualTerminal::scroll_page_up
            } else {
                VirtualTerminal::scroll_page_down
            };

            scroll(&mut terminal, frame_buffer.borrow_mut().lock());
            return;
        }

        if let Some(input) = decode_key(keyboard_state, &key) {
            terminal.process_key(&input);
            terminal.render(frame_buffer.borrow_mut().lock());
            return;
        }

        printk!(
            terminal, frame_buffer;
            "Warning: unexpected key {}. Ignoring the key event",
            key
        );
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = keyboardInterrupt)]
    pub fn keyboard_interrupt(&self, kind: KeyEventKind, key: String) {
        self.kernel.handle_key_event(kind, &key);
    }
}

fn decode_key(keyboard_state: KeyboardState, key: &str) -> Option<Cow<'_, str>> {
    if let Some(code) = key.strip_prefix("Key") {
        let code = code.as_bytes()[0] - b'A';
        let offset = if keyboard_state.is_ctrl_down() {
            1
        } else if keyboard_state.emits_lowercase() {
            b'a'
        } else {
            b'A'
        };

        return Some(Cow::Owned(((code + offset) as char).to_string()));
    }

    let is_shift_down = keyboard_state.is_shift_down();
    Some(Cow::Borrowed(match key {
        "Digit1" if is_shift_down => "!",
        "Digit2" if is_shift_down => "@",
        "Digit3" if is_shift_down => "#",
        "Digit4" if is_shift_down => "$",
        "Digit5" if is_shift_down => "%",
        "Digit6" if is_shift_down => "^",
        "Digit7" if is_shift_down => "&",
        "Digit8" if is_shift_down => "*",
        "Digit9" if is_shift_down => "(",
        "Digit0" if is_shift_down => ")",

        "Digit0" | "Digit1" | "Digit2" | "Digit3" | "Digit4" | "Digit5" | "Digit6" | "Digit7"
        | "Digit8" | "Digit9" => key.strip_prefix("Digit").unwrap(),

        "Space" => " ",
        "Tab" if is_shift_down => "\x1B[Z",
        "Tab" => "\t",
        "Home" => "\x1B[H",
        "End" => "\x1B[F",
        "PageUp" => "\x1B[5~",
        "PageDown" => "\x1B[6~",
        "Delete" => "\x1B[3~",
        "Backspace" => "\x08",
        "Enter" => "\n",

        "Minus" if is_shift_down => "_",
        "Minus" => "-",
        "Equal" if is_shift_down => "+",
        "Equal" => "=",
        "Backslash" if is_shift_down => "|",
        "Backslash" => "\\",
        "Comma" if is_shift_down => "<",
        "Comma" => ",",
        "Period" if is_shift_down => ">",
        "Period" => ".",
        "BracketLeft" if is_shift_down => "{",
        "BracketLeft" => "[",
        "BracketRight" if is_shift_down => "}",
        "BracketRight" => "]",
        "Slash" if is_shift_down => "?",
        "Slash" => "/",
        "Semicolon" if is_shift_down => ":",
        "Semicolon" => ";",
        "Quote" if is_shift_down => "\"",
        "Quote" => "'",
        "Backquote" if is_shift_down => "~",
        "Backquote" => "`",

        "ArrowUp" => "\x1B[A",
        "ArrowDown" => "\x1B[B",
        "ArrowRight" => "\x1B[C",
        "ArrowLeft" => "\x1B[D",

        "NumpadDivide" => "/",
        "NumpadMultiply" => "*",
        "NumpadSubtract" => "-",
        "NumpadAdd" => "+",
        "NumpadEnter" => "\n",
        "NumpadDecimal" if keyboard_state.numpad_emits_characters() => ".",
        "NumpadDecimal" => "\x1B[3~",

        "Numpad0" | "Numpad1" | "Numpad2" | "Numpad3" | "Numpad4" | "Numpad5" | "Numpad6"
        | "Numpad7" | "Numpad8" | "Numpad9"
            if keyboard_state.numpad_emits_characters() =>
        {
            key.strip_prefix("Numpad").unwrap()
        }

        "Numpad8" => "\x1B[A",
        "Numpad2" => "\x1B[B",
        "Numpad4" => "\x1B[D",
        "Numpad6" => "\x1B[C",
        "Numpad5" => "",
        "Numpad7" => "\x1B[H",
        "Numpad1" => "\x1B[F",
        "Numpad9" => "\x1B[5~",
        "Numpad3" => "\x1B[6~",

        _ => return None,
    }))
}
