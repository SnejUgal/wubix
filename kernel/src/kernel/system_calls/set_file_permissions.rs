use super::parse_argument;
use crate::{
    file_system::FileDescriptor,
    kernel::{Kernel, KernelRef},
};
use types::{
    fs::Permissions,
    ids::{FileDescriptorId, ProcessId},
    system_calls::set_permissions::SetPermissionsResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn set_file_permissions(
        &self,
        process_id: ProcessId,
        descriptor: FileDescriptorId,
        permissions: Permissions,
    ) -> SetPermissionsResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.set_permissions(permissions).await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = setFilePermissionsSystemCall)]
    pub async fn set_file_permissions(
        self,
        process_id: u32,
        descriptor: u32,
        permissions: JsValue,
    ) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);
        let permissions = match parse_argument(permissions) {
            Ok(permissions) => permissions,
            Err(error) => return error,
        };

        let result = self
            .kernel
            .set_file_permissions(process_id, descriptor, permissions)
            .await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
