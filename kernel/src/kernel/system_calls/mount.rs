use crate::{
    file_system::{DevFileSystem, IdbFileSystem, TemporaryFileSystem},
    kernel::{Kernel, KernelRef},
};
use std::rc::Rc;
use types::{
    ids::{ProcessId, UserId},
    io::IoErrorKind,
    system_calls::mount::{self, MountOptions, MountResult},
};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

use super::parse_argument;

impl Kernel {
    async fn mount(
        &self,
        process_id: ProcessId,
        path: &Path,
        mount_options: MountOptions,
    ) -> MountResult {
        let process = self.get_process(process_id)?;

        if process.user_id != UserId(0) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let file_system = match mount_options.file_system {
            mount::FileSystem::Tmpfs => TemporaryFileSystem::default().into(),
            mount::FileSystem::Idbfs { id } => IdbFileSystem::new(id).await.into(),
            mount::FileSystem::Devfs => DevFileSystem::new(
                Rc::clone(&self.virtual_terminals),
                Rc::clone(&self.active_virtual_terminal),
                Rc::clone(&self.frame_buffer),
            )
            .into(),
        };

        let path = process.current_working_directory.borrow().join(path);
        self.virtual_file_system
            .mount(
                path.as_ref(),
                file_system,
                mount_options.should_remount,
                process.user_id,
                process.group_id,
            )
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = mountSystemCall)]
    pub async fn mount(self, process_id: u32, path: Vec<u8>, mount_options: JsValue) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));
        let mount_options = match parse_argument(mount_options) {
            Ok(options) => options,
            Err(error) => return error,
        };

        let result = self.kernel.mount(process_id, path, mount_options).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
