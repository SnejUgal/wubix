use crate::kernel::{Kernel, KernelRef};
use types::{ids::ProcessId, system_calls::mount::MountResult};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn unmount(&self, process_id: ProcessId, path: &Path) -> MountResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);

        self.virtual_file_system
            .unmount(path.as_ref(), process.user_id, process.group_id)
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = unmountSystemCall)]
    pub async fn unmount(self, process_id: u32, path: Vec<u8>) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));

        let result = self.kernel.unmount(process_id, path).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
