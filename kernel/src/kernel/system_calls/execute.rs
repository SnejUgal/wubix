use crate::{
    file_system::FileDescriptor,
    kernel::{
        process::{Process, ProcessParameters},
        start_process, Kernel, KernelRef,
    },
    util::ungzip,
};
use std::rc::Rc;
use types::{
    ids::{ProcessId, UserId},
    io::IoErrorKind,
    system_calls::{
        execute::{ExecuteParameters, ExecuteResult},
        open::OpenMode,
    },
};
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;

use super::parse_argument;

impl Kernel {
    async fn execute(&self, process_id: ProcessId, parameters: ExecuteParameters) -> ExecuteResult {
        let process = self.get_process(process_id)?;

        let executable_path = process
            .current_working_directory
            .borrow()
            .join(UnixStr::from_bytes(&parameters.executable));
        let executable_path = self
            .virtual_file_system
            .canonicalize(&executable_path, process.user_id, process.group_id)
            .await?;

        let working_directory = process
            .current_working_directory
            .borrow()
            .join(UnixStr::from_bytes(&parameters.working_directory));
        let working_directory = self
            .virtual_file_system
            .canonicalize(&working_directory, process.user_id, process.group_id)
            .await?;

        self.virtual_file_system
            .assert_is_directory(&working_directory, process.user_id, process.group_id)
            .await?;

        let executable = self
            .virtual_file_system
            .open(
                &executable_path,
                process.user_id,
                process.group_id,
                OpenMode::ReadOnly,
            )
            .await?;
        let executable_metadata = executable.read_metadata().await?;

        if !executable_metadata.can_execute(process.user_id, process.group_id) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let executable_file = executable.read(executable_metadata.size).await?;
        let executable_file = ungzip(&executable_file)?;

        let child_user_id = if executable_metadata.permissions.set_user_id {
            executable_metadata.owner_id
        } else if let Some(user_id) = parameters.user_id {
            if process.user_id != user_id && process.user_id != UserId(0) {
                return Err(IoErrorKind::PermissionDenied);
            }
            user_id
        } else {
            process.user_id
        };

        let child_group_id = if executable_metadata.permissions.set_group_id {
            executable_metadata.group_id
        } else if let Some(group_id) = parameters.group_id {
            if process.group_id != group_id && process.user_id != UserId(0) {
                return Err(IoErrorKind::PermissionDenied);
            }
            group_id
        } else {
            process.group_id
        };

        let stdin = process.get_descriptor(parameters.stdin)?;
        let stdout = process.get_descriptor(parameters.stdout)?;
        let stderr = process.get_descriptor(parameters.stderr)?;

        let new_process_id = {
            let ProcessId(id) = self.next_process_id.get();
            self.next_process_id.replace(ProcessId(id + 1))
        };

        let arguments = serde_wasm_bindgen::to_value(&parameters.arguments).unwrap();
        let variables = serde_wasm_bindgen::to_value(&parameters.environment_variables).unwrap();
        let init_future: JsFuture =
            start_process(new_process_id.0, executable_file, arguments, variables).into();
        init_future
            .await
            .or(Err(IoErrorKind::CorruptedExecutable))?;

        self.processes.borrow_mut().insert(
            new_process_id,
            Rc::new(Process::new(ProcessParameters {
                executable_path,
                user_id: child_user_id,
                group_id: child_group_id,
                stdin,
                stdout,
                stderr,
                working_directory,
                parent_id: process_id,
            })),
        );
        process.children.borrow_mut().insert(new_process_id);

        Ok(new_process_id)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = executeSystemCall)]
    pub async fn execute_system_call(self, process_id: u32, parameters: JsValue) -> JsValue {
        let process_id = ProcessId(process_id);
        let parameters = match parse_argument(parameters) {
            Ok(parameters) => parameters,
            Err(error) => return error,
        };

        let result = self.kernel.execute(process_id, parameters).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
