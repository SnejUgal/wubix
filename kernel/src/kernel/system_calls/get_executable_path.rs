use crate::kernel::{Kernel, KernelRef};
use types::{ids::ProcessId, system_calls::get_executable_path::GetExecutablePathResult};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn get_executable_path(&self, process_id: ProcessId) -> GetExecutablePathResult {
        let process = self.get_process(process_id)?;
        let path = process.executable_path.as_unix_str().as_bytes().to_owned();
        Ok(path)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = getExecutablePathSystemCall)]
    pub async fn get_executable_path(self, process_id: u32) -> JsValue {
        let result = self.kernel.get_executable_path(ProcessId(process_id)).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
