use super::parse_argument;
use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::ProcessId,
    io::IoErrorKind,
    system_calls::await_process::{self, ExitedProcess, Parameters, WaitFor},
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn await_process(
        &self,
        process_id: ProcessId,
        Parameters { wait_for, only_try }: Parameters,
    ) -> await_process::Result {
        let process = self.get_process(process_id)?;

        let wait_for = match wait_for {
            WaitFor::Process(id) => id,
            WaitFor::ProcessGroup(..) | WaitFor::AnyChild => {
                // todo
                return Err(IoErrorKind::UnsupportedOperation);
            }
        };

        let awaited_process = self.get_process(wait_for).or(Err(IoErrorKind::NotFound))?;
        if !process.children.borrow().contains(&wait_for) {
            return Err(IoErrorKind::PermissionDenied);
        }

        if awaited_process.exit_code.get().is_none() {
            if only_try {
                return Ok(None);
            } else {
                awaited_process.waiters.wait().await;
            }
        }

        process.children.borrow_mut().remove(&wait_for);
        self.processes.borrow_mut().remove(&wait_for);
        Ok(Some(ExitedProcess {
            process_id: wait_for,
            exit_code: awaited_process.exit_code.get().unwrap(),
        }))
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = awaitProcessSystemCall)]
    pub async fn await_process(self, process_id: u32, parameters: JsValue) -> JsValue {
        let process_id = ProcessId(process_id);
        let parameters = match parse_argument(parameters) {
            Ok(parameters) => parameters,
            Err(error) => return error,
        };

        let result = self.kernel.await_process(process_id, parameters).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
