use crate::kernel::{Kernel, KernelRef};
use types::{
    fs::Permissions, ids::ProcessId, system_calls::create_directory::CreateDirectoryResult,
};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

use super::parse_argument;

impl Kernel {
    async fn create_directory(
        &self,
        process_id: ProcessId,
        path: &Path,
        permissions: Permissions,
    ) -> CreateDirectoryResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);
        self.virtual_file_system
            .create_directory(&path, process.user_id, process.group_id, permissions)
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = createDirectorySystemCall)]
    pub async fn create_directory(
        self,
        process_id: u32,
        path: Vec<u8>,
        permissions: JsValue,
    ) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));
        let permissions = match parse_argument(permissions) {
            Ok(permissions) => permissions,
            Err(error) => return error,
        };

        let result = self
            .kernel
            .create_directory(process_id, path, permissions)
            .await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
