use crate::kernel::{Kernel, KernelRef};
use std::rc::Rc;
use types::{
    ids::{ProcessId, UserId},
    io::IoErrorKind,
    signal::Signal,
    system_calls::kill::{self, Destination, Parameters},
};
use wasm_bindgen::prelude::*;

use super::parse_argument;

impl Kernel {
    pub fn kill(
        &self,
        process_id: ProcessId,
        Parameters {
            destination,
            signal,
        }: Parameters,
    ) -> kill::Result {
        let process = self.get_process(process_id)?;

        let destination = match destination {
            Destination::Process(signalled_process_id) => {
                let signalled_process = self
                    .get_process(signalled_process_id)
                    .or(Err(IoErrorKind::NotFound))?;

                if process.user_id != UserId(0) && signalled_process.user_id != process.user_id {
                    return Err(IoErrorKind::PermissionDenied);
                }

                if signalled_process_id == ProcessId(1) {
                    // todo: when signals are properly implemented, we should check whether init
                    // set its own handler for this signal
                    return Err(IoErrorKind::PermissionDenied);
                }

                vec![(signalled_process_id, signalled_process)]
            }
            Destination::ProcessGroup(..) | Destination::MyProcessGroup => {
                // todo
                return Err(IoErrorKind::UnsupportedOperation);
            }
            Destination::All => self
                .processes
                .borrow()
                .iter()
                .filter_map(|(&other_process_id, other_process)| {
                    if process_id == other_process_id
                        || other_process_id == ProcessId(1)
                        || (process.user_id != UserId(0)
                            && process.user_id != other_process.user_id)
                    {
                        None
                    } else {
                        Some((other_process_id, Rc::clone(&other_process)))
                    }
                })
                .collect(),
        };

        if destination.is_empty() {
            return Err(IoErrorKind::PermissionDenied);
        }

        let signal = match signal {
            None => return Ok(()),
            Some(signal @ Signal::Kill) => signal,
            Some(_) => {
                // todo: process other signals
                return Err(IoErrorKind::UnsupportedOperation);
            }
        };

        let init = self.get_process(ProcessId(1)).unwrap();
        for (process_id, process) in destination {
            if signal == Signal::Kill {
                crate::kernel::terminate_process(process_id.0);
                if process.exit_code.get().is_none() {
                    process.exit_code.set(Some(-(signal as i32)));
                }
                process.waiters.notify_all();
            }
            init.children
                .borrow_mut()
                .extend(process.children.borrow().iter().copied());
        }

        Ok(())
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = killSystemCall)]
    pub async fn kill(self, process_id: u32, parameters: JsValue) -> JsValue {
        let parameters = match parse_argument(parameters) {
            Ok(parameters) => parameters,
            Err(error) => return error,
        };

        let result = self.kernel.kill(ProcessId(process_id), parameters);
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
