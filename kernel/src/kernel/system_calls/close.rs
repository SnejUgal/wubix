use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::{FileDescriptorId, ProcessId},
    io::IoErrorKind,
    system_calls::close::CloseResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    fn close(&self, process_id: ProcessId, descriptor: FileDescriptorId) -> CloseResult {
        let process = self.get_process(process_id)?;
        let mut descriptors_lock = process.file_descriptors.borrow_mut();

        descriptors_lock
            .remove(&descriptor)
            .ok_or(IoErrorKind::InvalidArgument)?;
        Ok(())
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = closeSystemCall)]
    pub async fn close(self, process_id: u32, descriptor: u32) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.close(process_id, descriptor);
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
