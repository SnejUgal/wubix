use super::parse_argument;
use crate::kernel::{Kernel, KernelRef};
use types::{fs::Permissions, ids::ProcessId, system_calls::set_permissions::SetPermissionsResult};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn set_permissions(
        &self,
        process_id: ProcessId,
        path: &Path,
        permissions: Permissions,
    ) -> SetPermissionsResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);

        self.virtual_file_system
            .set_permissions(&path, process.user_id, process.group_id, permissions)
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = setPermissionsSystemCall)]
    pub async fn set_permissions(
        self,
        process_id: u32,
        path: Vec<u8>,
        permissions: JsValue,
    ) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));
        let permissions = match parse_argument(permissions) {
            Ok(permissions) => permissions,
            Err(error) => return error,
        };

        let result = self
            .kernel
            .set_permissions(process_id, path, permissions)
            .await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
