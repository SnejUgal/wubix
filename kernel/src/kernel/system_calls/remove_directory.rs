use crate::kernel::{Kernel, KernelRef};
use types::{ids::ProcessId, system_calls::remove_directory::RemoveDirectoryResult};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn remove_directory(&self, process_id: ProcessId, path: &Path) -> RemoveDirectoryResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);
        self.virtual_file_system
            .remove_directory(&path, process.user_id, process.group_id)
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = removeDirectorySystemCall)]
    pub async fn remove_directory(self, process_id: u32, path: Vec<u8>) -> JsValue {
        let result = self
            .kernel
            .remove_directory(ProcessId(process_id), Path::new(UnixStr::from_bytes(&path)))
            .await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
