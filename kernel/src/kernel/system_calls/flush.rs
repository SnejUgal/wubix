use crate::{
    file_system::FileDescriptor,
    kernel::{Kernel, KernelRef},
};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::flush::FlushResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn flush(&self, process_id: ProcessId, descriptor: FileDescriptorId) -> FlushResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.flush().await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = flushSystemCall)]
    pub async fn flush(self, process_id: u32, descriptor: u32) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.flush(process_id, descriptor).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
