use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::ProcessId, system_calls::set_current_working_directory::SetCurrentWorkingDirectoryResult,
};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn set_current_working_directory(
        &self,
        process_id: ProcessId,
        path: &Path,
    ) -> SetCurrentWorkingDirectoryResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);
        let real_path = self
            .virtual_file_system
            .canonicalize(&path, process.user_id, process.group_id)
            .await?;

        self.virtual_file_system
            .assert_is_directory(&real_path, process.user_id, process.group_id)
            .await?;

        *process.current_working_directory.borrow_mut() = real_path;
        Ok(())
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = setCurrentWorkingDirectorySystemCall)]
    pub async fn set_current_working_directory(self, process_id: u32, path: Vec<u8>) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));

        let result = self
            .kernel
            .set_current_working_directory(process_id, path)
            .await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
