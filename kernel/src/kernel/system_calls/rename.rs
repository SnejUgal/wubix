use crate::kernel::{Kernel, KernelRef};
use types::{ids::ProcessId, system_calls::rename::RenameResult};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn rename(&self, process_id: ProcessId, origin: &Path, target: &Path) -> RenameResult {
        let process = self.get_process(process_id)?;
        self.virtual_file_system
            .rename(&origin, &target, process.user_id, process.group_id)
            .await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = renameSystemCall)]
    pub async fn rename(self, process_id: u32, source: Vec<u8>, target: Vec<u8>) -> JsValue {
        let process_id = ProcessId(process_id);
        let source = Path::new(UnixStr::from_bytes(&source));
        let target = Path::new(UnixStr::from_bytes(&target));

        let result = self.kernel.rename(process_id, source, target).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
