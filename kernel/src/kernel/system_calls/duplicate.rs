use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::duplicate::DuplicateResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    fn duplicate(&self, process_id: ProcessId, descriptor: FileDescriptorId) -> DuplicateResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        let new_descriptor = process.add_descriptor(descriptor);
        Ok(new_descriptor)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = duplicateSystemCall)]
    pub fn duplicate(self, process_id: u32, descriptor: u32) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.duplicate(process_id, descriptor);
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
