use crate::{
    file_system::FileDescriptor,
    kernel::{Kernel, KernelRef},
};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::write::WriteResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn write(
        &self,
        process_id: ProcessId,
        descriptor: FileDescriptorId,
        buffer: &[u8],
    ) -> WriteResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.write(buffer).await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = writeSystemCall)]
    pub async fn write(self, process_id: u32, descriptor: u32, buffer: Vec<u8>) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.write(process_id, descriptor, &buffer).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
