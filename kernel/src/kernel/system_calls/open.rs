use super::parse_argument;
use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::ProcessId,
    system_calls::open::{OpenMode, OpenResult},
};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn open(&self, process_id: ProcessId, path: &Path, mode: OpenMode) -> OpenResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);

        let descriptor = self
            .virtual_file_system
            .open(&path, process.user_id, process.group_id, mode)
            .await?;
        let id = process.add_descriptor(descriptor);

        Ok(id)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = openSystemCall)]
    pub async fn open(self, process_id: u32, path: Vec<u8>, mode: JsValue) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));
        let mode = match parse_argument(mode) {
            Ok(mode) => mode,
            Err(error) => return error,
        };

        let result = self.kernel.open(process_id, path, mode).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
