use crate::kernel::{Kernel, KernelRef};
use types::ids::ProcessId;
use wasm_bindgen::prelude::*;

impl Kernel {
    pub fn exit(&self, process_id: ProcessId, return_code: i32) {
        if process_id == ProcessId(1) {
            panic!("Tried to kill init!");
        }

        let process = match self.get_process(process_id) {
            Ok(process) => process,
            Err(_) => return,
        };
        process.exit_code.set(Some(return_code & 0xff));
        process.waiters.notify_all();

        if !process.children.borrow().is_empty() {
            let init = self.get_process(ProcessId(1)).unwrap();
            init.children
                .borrow_mut()
                .extend(process.children.borrow().iter().copied());
        }
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = exitSystemCall)]
    pub async fn exit(self, process_id: u32, return_code: i32) {
        self.kernel.exit(ProcessId(process_id), return_code);
    }
}
