use super::parse_argument;
use crate::{
    file_system::FileDescriptor,
    kernel::{Kernel, KernelRef},
};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::seek::{SeekFrom, SeekResult},
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn seek(
        &self,
        process_id: ProcessId,
        descriptor: FileDescriptorId,
        seek_from: SeekFrom,
    ) -> SeekResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.seek(seek_from).await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = seekSystemCall)]
    pub async fn seek(self, process_id: u32, descriptor: u32, seek: JsValue) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);
        let seek = match parse_argument(seek) {
            Ok(seek) => seek,
            Err(error) => return error,
        };

        let result = self.kernel.seek(process_id, descriptor, seek).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
