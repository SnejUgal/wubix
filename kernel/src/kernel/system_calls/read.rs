use crate::{
    file_system::FileDescriptor,
    kernel::{Kernel, KernelRef},
};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::read::ReadResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn read(
        &self,
        process_id: ProcessId,
        descriptor: FileDescriptorId,
        count: usize,
    ) -> ReadResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.read(count).await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = readSystemCall)]
    pub async fn read(self, process_id: u32, descriptor: u32, count: usize) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.read(process_id, descriptor, count).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
