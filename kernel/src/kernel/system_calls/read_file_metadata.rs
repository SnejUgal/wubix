use crate::{file_system::FileDescriptor, kernel::{Kernel, KernelRef}};
use types::{
    ids::{FileDescriptorId, ProcessId},
    system_calls::read_metadata::ReadMetadataResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn read_file_metadata(
        &self,
        process_id: ProcessId,
        descriptor: FileDescriptorId,
    ) -> ReadMetadataResult {
        let process = self.get_process(process_id)?;
        let descriptor = process.get_descriptor(descriptor)?;
        descriptor.read_metadata().await
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = readFileMetadataSystemCall)]
    pub async fn read_file_metadata(self, process_id: u32, descriptor: u32) -> JsValue {
        let process_id = ProcessId(process_id);
        let descriptor = FileDescriptorId(descriptor);

        let result = self.kernel.read_file_metadata(process_id, descriptor).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
