use crate::{
    file_system::{any::AnyDescriptor, pipe::create_pipe},
    kernel::{Kernel, KernelRef},
};
use std::rc::Rc;
use types::{ids::ProcessId, system_calls::create_pipe::CreatePipeResult};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn create_pipe(&self, process_id: ProcessId) -> CreatePipeResult {
        let process = self.get_process(process_id)?;
        let (reading_half, writing_half) = create_pipe(process.user_id, process.group_id);
        let reading_descriptor = process.add_descriptor(AnyDescriptor::Pipe(Rc::new(reading_half)));
        let writing_descriptor = process.add_descriptor(AnyDescriptor::Pipe(Rc::new(writing_half)));

        Ok((reading_descriptor, writing_descriptor))
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = createPipeSystemCall)]
    pub async fn create_pipe(self, process_id: u32) -> JsValue {
        let result = self.kernel.create_pipe(ProcessId(process_id)).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
