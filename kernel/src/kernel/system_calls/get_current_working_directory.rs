use crate::kernel::{Kernel, KernelRef};
use types::{
    ids::ProcessId, system_calls::get_current_working_directory::GetCurrentWorkingDirectoryResult,
};
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn get_current_working_directory(
        &self,
        process_id: ProcessId,
    ) -> GetCurrentWorkingDirectoryResult {
        let process = self.get_process(process_id)?;
        let cwd = process.current_working_directory.borrow();
        let cwd = cwd.as_unix_str().as_bytes().to_owned();
        Ok(cwd)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = getCurrentWorkingDirectorySystemCall)]
    pub async fn get_current_working_directory(self, process_id: u32) -> JsValue {
        let process_id = ProcessId(process_id);

        let result = self.kernel.get_current_working_directory(process_id).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
