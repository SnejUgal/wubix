use crate::kernel::{Kernel, KernelRef};
use types::{ids::ProcessId, system_calls::read_directory::ReadDirectoryResult};
use unix_path::Path;
use unix_str::UnixStr;
use wasm_bindgen::prelude::*;

impl Kernel {
    async fn read_directory(&self, process_id: ProcessId, path: &Path) -> ReadDirectoryResult {
        let process = self.get_process(process_id)?;
        let path = process.current_working_directory.borrow().join(path);

        let files = self
            .virtual_file_system
            .read_directory(&path, process.user_id, process.group_id)
            .await?;
        let files = files
            .into_iter()
            .map(|(name, metadata)| (name.into_vec(), metadata))
            .collect();

        Ok(files)
    }
}

#[wasm_bindgen]
impl KernelRef {
    #[wasm_bindgen(js_name = readDirectorySystemCall)]
    pub async fn read_directory_system_call(self, process_id: u32, path: Vec<u8>) -> JsValue {
        let process_id = ProcessId(process_id);
        let path = Path::new(UnixStr::from_bytes(&path));

        let result = self.kernel.read_directory(process_id, path).await;
        serde_wasm_bindgen::to_value(&result).unwrap()
    }
}
