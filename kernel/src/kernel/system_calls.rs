use serde::de::DeserializeOwned;
use types::io::IoErrorKind;
use wasm_bindgen::JsValue;

mod await_process;
mod canonicalize;
mod close;
mod create_directory;
mod create_hard_link;
mod create_pipe;
mod duplicate;
mod execute;
mod exit;
mod flush;
mod get_current_working_directory;
mod get_executable_path;
mod kill;
mod mount;
mod open;
mod read;
mod read_directory;
mod read_file_metadata;
mod read_metadata;
mod remove_directory;
mod rename;
mod seek;
mod set_current_working_directory;
mod set_file_permissions;
mod set_permissions;
mod unlink_file;
mod unmount;
mod write;

fn parse_argument<T: DeserializeOwned>(argument: JsValue) -> Result<T, JsValue> {
    serde_wasm_bindgen::from_value(argument).map_err(|_| {
        serde_wasm_bindgen::to_value(&Err::<(), _>(IoErrorKind::InvalidArgument)).unwrap()
    })
}
