use crate::{file_system::any::AnyDescriptor, notify::Notify};
use std::{
    cell::{Cell, RefCell},
    collections::{HashMap, HashSet},
};
use types::{
    ids::{FileDescriptorId, GroupId, ProcessId, UserId},
    io::{IoErrorKind, IoResult},
};
use unix_path::PathBuf;

pub struct ProcessParameters {
    pub executable_path: PathBuf,
    pub user_id: UserId,
    pub group_id: GroupId,
    pub stdin: AnyDescriptor,
    pub stdout: AnyDescriptor,
    pub stderr: AnyDescriptor,
    pub working_directory: PathBuf,
    pub parent_id: ProcessId,
}

pub struct Process {
    pub user_id: UserId,
    pub group_id: GroupId,
    pub executable_path: PathBuf,
    pub next_file_descriptor_id: Cell<FileDescriptorId>,
    pub file_descriptors: RefCell<HashMap<FileDescriptorId, AnyDescriptor>>,
    pub current_working_directory: RefCell<PathBuf>,
    pub waiters: Notify,
    pub parent_id: ProcessId,
    pub children: RefCell<HashSet<ProcessId>>,
    pub exit_code: Cell<Option<i32>>,
}

impl Process {
    pub fn new(
        ProcessParameters {
            executable_path,
            user_id,
            group_id,
            stdin,
            stdout,
            stderr,
            working_directory,
            parent_id,
        }: ProcessParameters,
    ) -> Self {
        let mut descriptors = HashMap::new();
        descriptors.insert(FileDescriptorId(0), stdin);
        descriptors.insert(FileDescriptorId(1), stdout);
        descriptors.insert(FileDescriptorId(2), stderr);
        Self {
            user_id,
            group_id,
            executable_path,
            next_file_descriptor_id: Cell::new(FileDescriptorId(3)),
            file_descriptors: RefCell::new(descriptors),
            current_working_directory: RefCell::new(working_directory),
            waiters: Notify::default(),
            parent_id,
            children: RefCell::new(HashSet::new()),
            exit_code: Cell::new(None),
        }
    }

    pub fn add_descriptor(&self, descriptor: AnyDescriptor) -> FileDescriptorId {
        let id = {
            let FileDescriptorId(id) = self.next_file_descriptor_id.get();
            self.next_file_descriptor_id
                .replace(FileDescriptorId(id + 1))
        };
        self.file_descriptors.borrow_mut().insert(id, descriptor);
        id
    }

    pub fn get_descriptor(&self, descriptor: FileDescriptorId) -> IoResult<AnyDescriptor> {
        let descriptors = self.file_descriptors.borrow();
        let descriptor = descriptors
            .get(&descriptor)
            .ok_or(IoErrorKind::InvalidArgument)?;
        Ok(descriptor.clone())
    }
}
