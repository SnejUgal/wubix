use crate::file_system::{
    self,
    any::{AnyDescriptor, AnyDirectory},
    AnyEntry, DevFileSystem, Directory, Entry, File, FileDescriptor, FileSystem,
};
use futures::stream::{iter, StreamExt, TryStreamExt};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
use types::{
    fs::{Metadata, Permissions},
    ids::{GroupId, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::open::{CreateOptions, OpenMode, WriteOptions},
};
use unix_path::{Path, PathBuf};
use unix_str::UnixString;

#[derive(Debug)]
pub struct VirtualFileSystem {
    mounts: RefCell<HashMap<PathBuf, file_system::Any>>,
}

struct WalkedDirectory {
    stack: Vec<(file_system::Any, AnyDirectory)>,
    path: PathBuf,
}

struct RealPath {
    path: PathBuf,
    parent_directory: AnyDirectory,
    file_system: file_system::Any,
}

impl VirtualFileSystem {
    pub fn new(initramfs: file_system::Any, devfs: Rc<DevFileSystem>) -> Self {
        let mut mounts = HashMap::new();
        mounts.insert(PathBuf::from("/"), initramfs);
        mounts.insert(PathBuf::from("/dev"), file_system::Any::Dev(devfs));
        Self {
            mounts: RefCell::new(mounts),
        }
    }

    async fn walk_to_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<WalkedDirectory> {
        let mut current_path = PathBuf::from("/");
        let mut stack = Vec::with_capacity(path.iter().count());
        let root_file_system = self.mounts.borrow().get(Path::new("/")).unwrap().clone();
        let root = root_file_system.get_root().await?;
        stack.push((root_file_system, root));

        for component in path.strip_prefix("/").unwrap() {
            if component == ".." {
                if stack.len() > 1 {
                    current_path.pop();
                    stack.pop();
                }
                continue;
            }

            let (file_system, current_directory) = stack.last().unwrap().clone();
            let metadata = current_directory.read_metadata().await?;
            if !metadata.can_execute(user_id, group_id) {
                return Err(IoErrorKind::PermissionDenied);
            }

            current_path.push(component);

            if let Some(file_system) = self.mounts.borrow().get(&current_path) {
                stack.push((file_system.clone(), file_system.get_root().await?));
                continue;
            }

            let inode = current_directory.get_entry(component).await?;
            let directory = match file_system.get_entry(inode).await? {
                AnyEntry::Directory(directory) => directory,
                _ => return Err(IoErrorKind::NotDirectory),
            };
            stack.push((file_system, directory));
        }

        Ok(WalkedDirectory {
            stack,
            path: current_path,
        })
    }

    async fn get_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<(file_system::Any, AnyDirectory)> {
        let WalkedDirectory { mut stack, .. } =
            self.walk_to_directory(path, user_id, group_id).await?;
        Ok(stack.pop().unwrap())
    }

    async fn get_real_path(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<RealPath> {
        // None is only returned for `/`, which is always a mount point
        let parent = if let Some(parent) = path.parent() {
            parent
        } else {
            let root_mount = self.mounts.borrow().get(Path::new("/")).unwrap().clone();
            let root = root_mount.get_root().await?;
            return Ok(RealPath {
                path: PathBuf::from("/"),
                file_system: root_mount,
                parent_directory: root,
            });
        };
        let mut walked_directory = self.walk_to_directory(parent, user_id, group_id).await?;

        if let Some(name) = path.file_name() {
            walked_directory.path.push(name);
        } else if walked_directory.stack.len() > 1 {
            walked_directory.path.pop();
            walked_directory.stack.pop();
        }

        let (file_system, parent_directory) = walked_directory.stack.pop().unwrap();

        let metadata = parent_directory.read_metadata().await?;
        if !metadata.can_execute(user_id, group_id) {
            return Err(IoErrorKind::PermissionDenied);
        }

        Ok(RealPath {
            path: walked_directory.path,
            file_system,
            parent_directory,
        })
    }

    pub async fn open(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
        mode: OpenMode,
    ) -> IoResult<AnyDescriptor> {
        let parent = path.parent().ok_or(IoErrorKind::IsDirectory)?;
        let (file_system, parent_directory) = self.get_directory(parent, user_id, group_id).await?;

        let metadata = parent_directory.read_metadata().await?;
        if !metadata.can_execute(user_id, group_id) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let file_name = path.file_name().ok_or(IoErrorKind::IsDirectory)?;
        let inode = match (parent_directory.get_entry(file_name).await, mode) {
            (Ok(inode), mode) => {
                let create_only = match mode {
                    OpenMode::ReadOnly => false,
                    OpenMode::WriteOnly(WriteOptions { create_options, .. })
                    | OpenMode::ReadWrite(WriteOptions { create_options, .. }) => {
                        if let Some(CreateOptions { create_only, .. }) = create_options {
                            create_only
                        } else {
                            false
                        }
                    }
                };

                if create_only {
                    return Err(IoErrorKind::AlreadyExists);
                }

                inode
            }
            (
                Err(IoErrorKind::NotFound),
                OpenMode::WriteOnly(WriteOptions {
                    create_options: Some(CreateOptions { permissions, .. }),
                    ..
                }),
            )
            | (
                Err(IoErrorKind::NotFound),
                OpenMode::ReadWrite(WriteOptions {
                    create_options: Some(CreateOptions { permissions, .. }),
                    ..
                }),
            ) => {
                let file = file_system
                    .create_file(user_id, group_id, permissions)
                    .await?;
                parent_directory.put_entry(file_name, file.inode()).await?;
                file.inode()
            }
            (Err(error), _) => return Err(error),
        };

        let descriptor = match file_system.get_entry(inode).await? {
            AnyEntry::File(file) => file.open(mode).await?,
            AnyEntry::Directory(_) => return Err(IoErrorKind::IsDirectory),
        };

        let metadata = descriptor.read_metadata().await?;
        let (needs_read_access, needs_write_access) = match mode {
            OpenMode::ReadOnly => (true, false),
            OpenMode::WriteOnly(_) => (false, true),
            OpenMode::ReadWrite(_) => (true, true),
        };

        if (needs_read_access && !metadata.can_read(user_id, group_id))
            || (needs_write_access && !metadata.can_write(user_id, group_id))
        {
            return Err(IoErrorKind::PermissionDenied);
        }

        Ok(descriptor)
    }

    pub async fn read_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<HashMap<UnixString, Metadata>> {
        let (file_system, directory) = &self.get_directory(path, user_id, group_id).await?;

        let metadata = directory.read_metadata().await?;
        if !metadata.can_read(user_id, group_id) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let inode_map = directory.read().await?;
        let entries: HashMap<_, _> = iter(inode_map)
            .then(|(name, inode)| async move {
                let entry = file_system.get_entry(inode).await?;

                let metadata = match entry {
                    AnyEntry::Directory(directory) => directory.read_metadata().await?,
                    AnyEntry::File(file) => {
                        file.open(OpenMode::ReadOnly).await?.read_metadata().await?
                    }
                };

                Ok((name, metadata))
            })
            .try_collect()
            .await?;

        Ok(entries)
    }

    pub async fn unlink_file(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        let parent = path.parent().ok_or(IoErrorKind::NotDirectory)?;
        let (file_system, parent_directory) = self.get_directory(parent, user_id, group_id).await?;

        let file_name = path.file_name().ok_or(IoErrorKind::IsDirectory)?;
        let file_inode = parent_directory.get_entry(file_name).await?;
        let file_metadata = match file_system.get_entry(file_inode).await? {
            AnyEntry::File(file) => file.open(OpenMode::ReadOnly).await?.read_metadata().await?,
            _ => return Err(IoErrorKind::IsDirectory),
        };

        let parent_metadata = parent_directory.read_metadata().await?;
        let has_permission = if parent_metadata.permissions.is_sticky {
            [parent_metadata.owner_id, file_metadata.owner_id, UserId(0)].contains(&user_id)
        } else {
            parent_metadata.can_write(user_id, group_id)
        };

        if !has_permission {
            return Err(IoErrorKind::PermissionDenied);
        }

        parent_directory.remove_entry(file_name).await
    }

    pub async fn remove_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        let RealPath {
            path,
            file_system,
            parent_directory,
        } = self.get_real_path(path, user_id, group_id).await?;
        let directory_name = if let Some(name) = path.file_name() {
            name
        } else {
            // if `file_name` is `None`, then `path` is `/`
            return Err(IoErrorKind::DeviceBusy);
        };
        let directory_inode = parent_directory.get_entry(directory_name).await?;
        let directory = match file_system.get_entry(directory_inode).await? {
            AnyEntry::Directory(directory) => directory,
            _ => return Err(IoErrorKind::NotDirectory),
        };
        let directory_metadata = directory.read_metadata().await?;

        let parent_metadata = parent_directory.read_metadata().await?;
        let has_permission = if parent_metadata.permissions.is_sticky {
            [
                parent_metadata.owner_id,
                directory_metadata.owner_id,
                UserId(0),
            ]
            .contains(&user_id)
        } else {
            parent_metadata.can_write(user_id, group_id)
        };

        if !has_permission {
            return Err(IoErrorKind::PermissionDenied);
        }

        if self.mounts.borrow().contains_key(&path) {
            return Err(IoErrorKind::DeviceBusy);
        }

        if !directory.is_empty().await? {
            return Err(IoErrorKind::NotEmpty);
        }

        drop(directory);
        parent_directory.remove_entry(directory_name).await
    }

    pub async fn read_metadata(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<Metadata> {
        let RealPath {
            path,
            file_system,
            parent_directory,
        } = self.get_real_path(path, user_id, group_id).await?;

        if let Some(mount) = self.mounts.borrow().get(&path) {
            return mount.get_root().await?.read_metadata().await;
        }

        let inode = parent_directory
            .get_entry(path.file_name().unwrap())
            .await?;
        match file_system.get_entry(inode).await? {
            AnyEntry::File(file) => file.open(OpenMode::ReadOnly).await?.read_metadata().await,
            AnyEntry::Directory(directory) => directory.read_metadata().await,
        }
    }

    pub async fn set_permissions(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> IoResult<()> {
        let RealPath {
            path,
            file_system,
            parent_directory,
        } = self.get_real_path(path, user_id, group_id).await?;

        let parent_metadata = parent_directory.read_metadata().await?;
        let check_permission = |file_metadata: Metadata| {
            if parent_metadata.permissions.is_sticky {
                [parent_metadata.owner_id, file_metadata.owner_id, UserId(0)].contains(&user_id)
            } else {
                parent_metadata.can_write(user_id, group_id)
            }
        };

        let directory = if let Some(mount) = self.mounts.borrow().get(&path) {
            mount.get_root().await?
        } else {
            let inode = parent_directory
                .get_entry(path.file_name().unwrap())
                .await?;
            match file_system.get_entry(inode).await? {
                AnyEntry::File(file) => {
                    let descriptor = file.open(OpenMode::WriteOnly(Default::default())).await?;
                    let file_metadata = descriptor.read_metadata().await?;
                    if !check_permission(file_metadata) {
                        return Err(IoErrorKind::PermissionDenied);
                    }
                    return descriptor.set_permissions(permissions).await;
                }
                AnyEntry::Directory(directory) => directory,
            }
        };

        let directory_metadata = directory.read_metadata().await?;
        if !check_permission(directory_metadata) {
            return Err(IoErrorKind::PermissionDenied);
        }

        directory.set_permissions(permissions).await
    }

    pub async fn canonicalize(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<PathBuf> {
        let RealPath { path, .. } = self.get_real_path(path, user_id, group_id).await?;

        Ok(path)
    }

    pub async fn create_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> IoResult<()> {
        let RealPath {
            path,
            file_system,
            parent_directory,
        } = self.get_real_path(path, user_id, group_id).await?;
        let name = path.file_name().unwrap();

        if self.mounts.borrow().contains_key(&path) {
            return Err(IoErrorKind::AlreadyExists);
        }

        let parent_metadata = parent_directory.read_metadata().await?;
        if !parent_metadata.can_write(user_id, group_id) {
            return Err(IoErrorKind::PermissionDenied);
        }

        if parent_directory.has_entry(name).await? {
            return Err(IoErrorKind::AlreadyExists);
        }

        let permissions = Permissions {
            set_user_id: false,
            set_group_id: false,
            is_sticky: false,
            ..permissions
        };
        let group_id = if parent_metadata.permissions.set_group_id {
            parent_metadata.group_id
        } else {
            group_id
        };
        let new_directory = file_system
            .create_directory(user_id, group_id, permissions)
            .await?;
        parent_directory
            .put_entry(name, new_directory.inode())
            .await
    }

    pub async fn create_hard_link(
        &self,
        origin: &Path,
        target: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        let origin_real_path = self.get_real_path(origin, user_id, group_id).await?;
        let target_real_path = self.get_real_path(target, user_id, group_id).await?;

        if self.mounts.borrow().contains_key(&origin_real_path.path) {
            return Err(IoErrorKind::IsDirectory);
        }

        if self.mounts.borrow().contains_key(&target_real_path.path) {
            return Err(IoErrorKind::AlreadyExists);
        }

        if !origin_real_path
            .file_system
            .is_eq(&target_real_path.file_system)
        {
            return Err(IoErrorKind::CrossDevice);
        }

        let origin_name = origin_real_path.path.file_name().unwrap();
        let origin_inode = origin_real_path
            .parent_directory
            .get_entry(origin_name)
            .await?;
        if let AnyEntry::Directory(_) = origin_real_path.file_system.get_entry(origin_inode).await?
        {
            return Err(IoErrorKind::IsDirectory);
        }

        let target_name = target_real_path.path.file_name().unwrap();
        target_real_path
            .parent_directory
            .put_entry(target_name, origin_inode)
            .await
    }

    pub async fn rename(
        &self,
        origin: &Path,
        target: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        let origin_real_path = self.get_real_path(origin, user_id, group_id).await?;
        let target_real_path = self.get_real_path(target, user_id, group_id).await?;

        if target_real_path.path.starts_with(&origin_real_path.path) {
            return Err(IoErrorKind::InvalidArgument);
        }

        if self.mounts.borrow().contains_key(&origin_real_path.path)
            || self.mounts.borrow().contains_key(&target_real_path.path)
        {
            return Err(IoErrorKind::DeviceBusy);
        }

        if !origin_real_path
            .file_system
            .is_eq(&target_real_path.file_system)
        {
            return Err(IoErrorKind::CrossDevice);
        }

        let file_system = origin_real_path.file_system;

        let origin_name = origin_real_path.path.file_name().unwrap();
        let origin_inode = origin_real_path
            .parent_directory
            .get_entry(origin_name)
            .await?;
        let is_origin_directory = matches!(
            file_system.get_entry(origin_inode).await?,
            AnyEntry::Directory(_)
        );

        let target_name = target_real_path.path.file_name().unwrap();
        let target_inode = match target_real_path
            .parent_directory
            .get_entry(target_name)
            .await
        {
            Ok(inode) => Some(inode),
            Err(IoErrorKind::NotFound) => None,
            Err(error) => return Err(error),
        };

        if Some(origin_inode) == target_inode {
            return Ok(());
        }

        match (is_origin_directory, target_inode) {
            (_, None) => (),
            (true, Some(target_inode)) => {
                let directory = match file_system.get_entry(target_inode).await? {
                    AnyEntry::Directory(directory) => directory,
                    AnyEntry::File(_) => return Err(IoErrorKind::NotDirectory),
                };

                if !directory.is_empty().await? {
                    return Err(IoErrorKind::NotEmpty);
                }
            }
            (false, Some(target_inode)) => match file_system.get_entry(target_inode).await? {
                AnyEntry::Directory(_) => return Err(IoErrorKind::IsDirectory),
                AnyEntry::File(_) => (),
            },
        }

        if target_inode.is_some() {
            target_real_path
                .parent_directory
                .remove_entry(target_name)
                .await?;
        }

        target_real_path
            .parent_directory
            .put_entry(target_name, origin_inode)
            .await?;
        origin_real_path
            .parent_directory
            .remove_entry(origin_name)
            .await
    }

    pub async fn assert_is_directory(
        &self,
        path: &Path,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        let RealPath {
            path,
            parent_directory,
            file_system,
        } = self.get_real_path(path, user_id, group_id).await?;

        if self.mounts.borrow().contains_key(&path) {
            return Ok(());
        }

        let inode = parent_directory
            .get_entry(path.file_name().unwrap())
            .await?;

        match file_system.get_entry(inode).await? {
            AnyEntry::Directory(_) => Ok(()),
            AnyEntry::File(_) => Err(IoErrorKind::NotDirectory),
        }
    }

    pub async fn mount(
        &self,
        path: &Path,
        file_system: file_system::Any,
        should_remount: bool,
        user_id: UserId,
        group_id: GroupId,
    ) -> IoResult<()> {
        if user_id != UserId(0) {
            return Err(IoErrorKind::PermissionDenied);
        }

        self.assert_is_directory(path, user_id, group_id).await?;

        let RealPath { path, .. } = self.get_real_path(path, user_id, group_id).await?;

        if !should_remount && self.mounts.borrow().contains_key(&path) {
            return Err(IoErrorKind::DeviceBusy);
        }

        self.mounts.borrow_mut().insert(path, file_system);

        Ok(())
    }

    pub async fn unmount(&self, path: &Path, user_id: UserId, group_id: GroupId) -> IoResult<()> {
        if user_id != UserId(0) {
            return Err(IoErrorKind::PermissionDenied);
        }

        let RealPath { path, .. } = self.get_real_path(path, user_id, group_id).await?;

        let mut mounts = self.mounts.borrow_mut();
        if !mounts.contains_key(&path) {
            return Err(IoErrorKind::InvalidArgument);
        }

        if mounts
            .iter()
            .any(|(mount, _)| mount != &path && mount.starts_with(&path))
        {
            return Err(IoErrorKind::DeviceBusy);
        }
        mounts.remove(&path);

        Ok(())
    }
}
