use std::{
    fmt::{self, Debug},
    ops::{Deref, DerefMut, Index, IndexMut},
};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = kernel, final)]
    fn rerender();
}

#[wasm_bindgen]
#[repr(C)]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Pixel {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    alpha: u8, // needs to always be 255
}

#[wasm_bindgen]
pub struct FrameBuffer {
    buffer: Vec<Pixel>,
    width: usize,
    height: usize,
}

impl Debug for FrameBuffer {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str("FrameBuffer { .. }")
    }
}

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub struct FrameBufferView {
    pub buffer: *const Pixel,
    pub width: usize,
    pub height: usize,
}

pub struct FrameBufferLock<'a>(&'a mut FrameBuffer);

impl Default for Pixel {
    fn default() -> Self {
        Self::new(0, 0, 0)
    }
}

impl Pixel {
    pub const fn new(red: u8, green: u8, blue: u8) -> Self {
        Self {
            red,
            green,
            blue,
            alpha: 255,
        }
    }
}

impl FrameBuffer {
    pub fn new((width, height): (usize, usize)) -> Self {
        let mut buffer: Vec<Pixel> = Vec::with_capacity(width * height);
        buffer.resize_with(width * height, Default::default);

        Self {
            buffer,
            width,
            height,
        }
    }

    pub fn get_view(&self) -> FrameBufferView {
        FrameBufferView {
            buffer: self.buffer.as_ptr(),
            width: self.width,
            height: self.height,
        }
    }

    pub fn lock(&mut self) -> FrameBufferLock<'_> {
        FrameBufferLock(self)
    }
}

impl FrameBufferLock<'_> {
    pub fn get_width(&self) -> usize {
        self.0.width
    }

    pub fn get_height(&self) -> usize {
        self.0.height
    }
}

impl Deref for FrameBufferLock<'_> {
    type Target = [Pixel];

    fn deref(&self) -> &Self::Target {
        &self.0.buffer
    }
}

impl Index<(usize, usize)> for FrameBufferLock<'_> {
    type Output = Pixel;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(
            x < self.0.width,
            "Error indexing to FrameBufferLock: x = {}, width = {}",
            x,
            self.0.width
        );
        assert!(
            y < self.0.height,
            "Error indexing to FrameBufferLock: y = {}, height = {}",
            y,
            self.0.height
        );
        &self.0.buffer[y * self.0.width + x]
    }
}

impl IndexMut<(usize, usize)> for FrameBufferLock<'_> {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        assert!(
            x < self.0.width,
            "Error indexing to FrameBufferLock: x = {}, width = {}",
            x,
            self.0.width
        );
        assert!(
            y < self.0.height,
            "Error indexing to FrameBufferLock: y = {}, height = {}",
            y,
            self.0.height
        );
        &mut self.0.buffer[y * self.0.width + x]
    }
}

impl DerefMut for FrameBufferLock<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0.buffer
    }
}

impl Drop for FrameBufferLock<'_> {
    fn drop(&mut self) {
        rerender();
    }
}
