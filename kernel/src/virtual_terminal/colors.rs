use crate::frame_buffer::Pixel;

pub const BLACK: Pixel = Pixel::new(1, 1, 1);
pub const RED: Pixel = Pixel::new(255, 56, 43);
pub const GREEN: Pixel = Pixel::new(57, 181, 74);
pub const YELLOW: Pixel = Pixel::new(255, 199, 6);
pub const BLUE: Pixel = Pixel::new(0, 111, 184);
pub const MAGENTA: Pixel = Pixel::new(118, 38, 113);
pub const CYAN: Pixel = Pixel::new(44, 181, 233);
pub const WHITE: Pixel = Pixel::new(204, 204, 204);

pub const BRIGHT_BLACK: Pixel = Pixel::new(128, 128, 128);
pub const BRIGHT_RED: Pixel = Pixel::new(255, 0, 0);
pub const BRIGHT_GREEN: Pixel = Pixel::new(0, 255, 0);
pub const BRIGHT_YELLOW: Pixel = Pixel::new(255, 255, 0);
pub const BRIGHT_BLUE: Pixel = Pixel::new(0, 0, 255);
pub const BRIGHT_MAGENTA: Pixel = Pixel::new(255, 0, 255);
pub const BRIGHT_CYAN: Pixel = Pixel::new(0, 255, 255);
pub const BRIGHT_WHITE: Pixel = Pixel::new(255, 255, 255);
