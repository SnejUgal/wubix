use std::{
    collections::HashMap,
    convert::TryInto,
    fmt::{self, Debug, Formatter},
};

const FONT_LENGTH: usize = 256;

const PSF1_MAGIC0: u8 = 0x36;
const PSF1_MAGIC1: u8 = 0x04;
const PSF1_MODEHASTAB: u8 = 0x02;
const PSF1_SEPARATOR: u16 = 0xFFFF;
const PSF1_STARTSEQ: u16 = 0xFFFE;

type CharacterBitMap = [u8; super::CHAR_HEIGHT];

pub struct Font {
    glyphs: [CharacterBitMap; FONT_LENGTH],
    associating_table: HashMap<u16, usize>,
}

impl Debug for Font {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        formatter.write_str("Font { .. }")
    }
}

impl Font {
    pub fn get_glyph(&self, unicode: &[u16]) -> CharacterBitMap {
        let glyph_index = if unicode.len() == 1 {
            *self.associating_table.get(&unicode[0]).unwrap_or(&0)
        } else {
            0
        };

        self.glyphs[glyph_index]
    }
}

#[derive(Debug)]
pub struct Set {
    pub default: Font,
    pub bold: Font,
}

fn read_ps1(bytes: &[u8]) -> Font {
    let magic = &bytes[0..2];
    assert_eq!(magic, [PSF1_MAGIC0, PSF1_MAGIC1]);

    let flags = bytes[2];
    assert_ne!(flags & PSF1_MODEHASTAB, 0);

    let height = bytes[3] as usize;
    assert_eq!(height, super::CHAR_HEIGHT);

    let bytes = &bytes[4..];
    let mut glyphs = [[0; 16]; FONT_LENGTH];

    for i in 0..FONT_LENGTH {
        let character: CharacterBitMap = (&bytes[i * height..(i + 1) * height])
            .try_into()
            .expect("unreachable");

        glyphs[i] = character;
    }

    let mut glyph_index = 0;
    let mut bytes = (&bytes[FONT_LENGTH * height..]).iter();
    let mut is_seq = false;
    let mut associating_table = HashMap::new();

    while let Some(first_byte) = bytes.next() {
        let second_byte = bytes.next().unwrap();
        let byte = u16::from_le_bytes([*first_byte, *second_byte]);

        match byte {
            PSF1_STARTSEQ => is_seq = true,
            PSF1_SEPARATOR => {
                is_seq = false;
                glyph_index += 1
            }
            _ if is_seq => {}
            unicode => {
                associating_table.insert(unicode, glyph_index);
            }
        }
    }

    Font {
        glyphs,
        associating_table,
    }
}

pub fn load_set() -> Set {
    Set {
        default: read_ps1(include_bytes!("./fonts/Lat7-Terminus16.psf")),
        bold: read_ps1(include_bytes!("./fonts/Lat7-TerminusBold16.psf")),
    }
}
