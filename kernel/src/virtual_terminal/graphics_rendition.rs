use super::colors::*;
use crate::frame_buffer::Pixel;
use std::iter::from_fn;

#[derive(Debug, Clone, Copy)]
pub struct GraphicsRendition {
    pub foreground: Pixel,
    pub background: Pixel,
    pub underscore: bool,
    pub underline: bool,
    pub crossed: bool,
    pub reversed: bool,
    pub half_bright: bool,
    pub bold: bool,
    pub blink: bool,
}

impl GraphicsRendition {
    fn process_color(&self, color: Pixel) -> Pixel {
        if self.half_bright {
            Pixel::new(color.red / 2, color.green / 2, color.blue / 2)
        } else {
            color
        }
    }

    pub fn get_foreground(&self, is_blink_visible: bool) -> Pixel {
        self.process_color(match (self.reversed, self.blink, is_blink_visible) {
            (false, false, _) | (false, true, true) | (true, true, false) => self.foreground,
            (false, true, false) | (true, false, _) | (true, true, true) => self.background,
        })
    }

    pub fn get_background(&self) -> Pixel {
        self.process_color(if self.reversed {
            self.foreground
        } else {
            self.background
        })
    }

    fn apply_parameters<P>(&mut self, mut parameters: P)
    where
        P: Iterator<Item = String>,
    {
        while let Some(parameter) = parameters.next() {
            match parameter.as_str() {
                // defaults
                "0" | "" => *self = GraphicsRendition::default(),
                // 8 bit foreground
                "30" => self.foreground = BLACK,
                "31" => self.foreground = RED,
                "32" => self.foreground = GREEN,
                "33" => self.foreground = YELLOW,
                "34" => self.foreground = BLUE,
                "35" => self.foreground = MAGENTA,
                "36" => self.foreground = CYAN,
                "37" | "39" => self.foreground = WHITE,
                // 8 bit background
                "40" => self.background = BLACK,
                "41" => self.background = RED,
                "42" => self.background = GREEN,
                "43" => self.background = YELLOW,
                "44" => self.background = BLUE,
                "45" => self.background = MAGENTA,
                "46" => self.background = CYAN,
                "47" | "49" => self.background = WHITE,
                // bright versions of 8 bit foreground
                "90" => self.foreground = BRIGHT_BLACK,
                "91" => self.foreground = BRIGHT_RED,
                "92" => self.foreground = BRIGHT_GREEN,
                "93" => self.foreground = BRIGHT_YELLOW,
                "94" => self.foreground = BRIGHT_BLUE,
                "95" => self.foreground = BRIGHT_MAGENTA,
                "96" => self.foreground = BRIGHT_CYAN,
                "97" => self.foreground = BRIGHT_WHITE,
                // bright versions of 8 bit background
                "100" => self.background = BRIGHT_BLACK,
                "101" => self.background = BRIGHT_RED,
                "102" => self.background = BRIGHT_GREEN,
                "103" => self.background = BRIGHT_YELLOW,
                "104" => self.background = BRIGHT_BLUE,
                "105" => self.background = BRIGHT_MAGENTA,
                "106" => self.background = BRIGHT_CYAN,
                "107" => self.background = BRIGHT_WHITE,
                // underscore, underline, crossed
                "4" => self.underscore = true,
                "21" => {
                    self.underscore = true;
                    self.underline = true;
                }
                "24" => {
                    self.underscore = false;
                    self.underline = false;
                }
                "9" => self.crossed = true,
                "29" => self.crossed = false,
                // reverse
                "7" => self.reversed = true,
                "27" => self.reversed = false,
                // half-bright & bold
                "1" => self.bold = true,
                "2" => self.half_bright = true,
                "22" => {
                    self.half_bright = false;
                    self.bold = false;
                }
                // blink
                "5" => self.blink = true,
                // 24 bit foreground & background
                "38" | "48" => {
                    let color_type = match parameters.next() {
                        Some(color_type) => color_type,
                        None => return,
                    };

                    let color = match color_type.as_str() {
                        "5" => {
                            let value: u8 = match parameters.next().and_then(|i| i.parse().ok()) {
                                Some(value) => value,
                                None => return,
                            };

                            match value {
                                0 => BLACK,
                                1 => RED,
                                2 => GREEN,
                                3 => YELLOW,
                                4 => BLUE,
                                5 => MAGENTA,
                                6 => CYAN,
                                7 => WHITE,
                                8 => BRIGHT_BLACK,
                                9 => BRIGHT_RED,
                                10 => BRIGHT_GREEN,
                                11 => BRIGHT_YELLOW,
                                12 => BRIGHT_BLUE,
                                13 => BRIGHT_MAGENTA,
                                14 => BRIGHT_CYAN,
                                15 => BRIGHT_WHITE,
                                16..=231 => {
                                    let value = value - 16;

                                    let red = value / 36;
                                    let green = (value % 36) / 6;
                                    let blue = (value % 36) % 6;

                                    Pixel::new(red * 51, blue * 51, green * 51)
                                }
                                232..=255 => {
                                    let value = 8 + (value - 232) * 10;

                                    Pixel::new(value, value, value)
                                }
                            }
                        }
                        "2" => {
                            let red = match parameters.next().and_then(|i| i.parse().ok()) {
                                Some(red) => red,
                                None => return,
                            };
                            let green = match parameters.next().and_then(|i| i.parse().ok()) {
                                Some(green) => green,
                                None => return,
                            };
                            let blue = match parameters.next().and_then(|i| i.parse().ok()) {
                                Some(blue) => blue,
                                None => return,
                            };

                            Pixel::new(red, green, blue)
                        }
                        _ => return,
                    };

                    match parameter.as_str() {
                        "38" => self.foreground = color,
                        "48" => self.background = color,
                        _ => unreachable!(),
                    }
                }
                _ => {}
            }
        }
    }

    pub fn apply_escape_code<C>(&mut self, escape_code: C) -> usize
    where
        C: Iterator<Item = char> + Clone,
    {
        let length = get_escape_code_length(escape_code.clone());
        if length >= 3 {
            self.apply_parameters(parse_parameters(escape_code.skip(2)));
        }
        length
    }
}

impl Default for GraphicsRendition {
    fn default() -> Self {
        Self {
            foreground: WHITE,
            background: BLACK,
            underscore: false,
            underline: false,
            crossed: false,
            reversed: false,
            half_bright: false,
            bold: false,
            blink: false,
        }
    }
}

pub fn get_escape_code_length<C>(mut escape_code: C) -> usize
where
    C: Iterator<Item = char>,
{
    if escape_code.next() != Some('\x1B') || escape_code.next() != Some('[') {
        return 0;
    }

    for (index, character) in escape_code.enumerate() {
        match character {
            '0'..='9' | ';' => (),
            'm' => return index + 3,
            _ => return 0,
        }
    }

    0
}

fn parse_parameters<C>(mut escape_code: C) -> impl Iterator<Item = String>
where
    C: Iterator<Item = char>,
{
    let mut current_parameter = String::new();
    let mut has_ended = false;

    from_fn(move || {
        if has_ended {
            return None;
        }

        for character in &mut escape_code {
            match character {
                '0'..='9' => current_parameter.push(character),
                ';' => {
                    let current_parameter = std::mem::take(&mut current_parameter);
                    return Some(current_parameter);
                }
                'm' => {
                    has_ended = true;
                    return Some(std::mem::take(&mut current_parameter));
                }
                _ => unreachable!("got invalid escape code after verifying it"),
            }
        }

        unreachable!("got invalid escape code after verifying it");
    })
}
