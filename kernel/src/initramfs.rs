use crate::file_system::{
    self,
    any::{Any, AnyForWasm},
    AnyEntry, Directory, Entry, File, FileDescriptor, FileSystem, IdbFileSystem,
};
use std::rc::Rc;
use types::{
    fs::{ClassPermissions, Permissions},
    ids::{GroupId, UserId},
    system_calls::open::{OpenMode, WriteOptions},
};
use unix_path::Path;
use wasm_bindgen::prelude::*;

const FILE_CLASS_PERMISSIONS: ClassPermissions = ClassPermissions::READ_ONLY;
const EXECUTABLE_CLASS_PERMISSIONS: ClassPermissions = ClassPermissions::READ_ONLY_EXECUTABLE;

const FILE_PERMISSIONS: Permissions = Permissions {
    owner: FILE_CLASS_PERMISSIONS,
    group: FILE_CLASS_PERMISSIONS,
    others: FILE_CLASS_PERMISSIONS,
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
};

const EXECUTABLE_PERMISSIONS: Permissions = Permissions {
    owner: EXECUTABLE_CLASS_PERMISSIONS,
    group: EXECUTABLE_CLASS_PERMISSIONS,
    others: EXECUTABLE_CLASS_PERMISSIONS,
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
};

const ROOT: UserId = UserId(0);
const ROOT_GROUP: GroupId = GroupId(0);

#[wasm_bindgen]
pub struct Initramfs(file_system::TemporaryFileSystem);

#[wasm_bindgen]
impl Initramfs {
    pub fn new() -> Self {
        Self(file_system::TemporaryFileSystem::default())
    }

    pub async fn from_idbfs(id: String) -> AnyForWasm {
        AnyForWasm {
            inner: Any::Idb(Rc::new(IdbFileSystem::new(id).await)),
        }
    }

    async fn create_directory_(
        &mut self,
        path: &Path,
    ) -> Result<file_system::temporary::TemporaryDirectory, String> {
        let mut parent = self.0.get_root().await.unwrap();

        for name in path.iter().skip(1) {
            if let Some(&inode) = parent.read().await.unwrap().get(name) {
                match self.0.get_entry(inode).await.unwrap() {
                    AnyEntry::Directory(directory) => parent = directory,
                    AnyEntry::File(_) => {
                        return Err(String::from("met a file while creating a directory"))
                    }
                }
            } else {
                let directory = self
                    .0
                    .create_directory(ROOT, ROOT_GROUP, EXECUTABLE_PERMISSIONS)
                    .await
                    .unwrap();
                parent.put_entry(name, directory.inode()).await.unwrap();
                parent = directory;
            };
        }

        Ok(parent)
    }

    #[wasm_bindgen(js_name = createDirectory)]
    pub async fn create_directory(mut self, path: String) -> Result<Initramfs, String> {
        let path = Path::new(&path);
        if path.is_relative() {
            return Err(String::from("path is not absolute"));
        }
        self.create_directory_(path).await?;
        Ok(self)
    }

    async fn create_file_(
        mut self,
        path: &Path,
        contents: Vec<u8>,
        permissions: Permissions,
    ) -> Result<Initramfs, String> {
        if path.is_relative() {
            return Err(String::from("path is not absolute"));
        }

        let parent = path.parent().unwrap();
        let file_name = path.file_name().unwrap();

        let parent = self.create_directory_(parent).await?;
        let file = self
            .0
            .create_file(ROOT, ROOT_GROUP, permissions)
            .await
            .unwrap();
        file.open(OpenMode::WriteOnly(WriteOptions::default()))
            .await
            .unwrap()
            .write(&contents)
            .await
            .unwrap();
        parent.put_entry(file_name, file.inode()).await.unwrap();

        Ok(self)
    }

    #[wasm_bindgen(js_name = createFile)]
    pub async fn create_file(self, path: String, contents: Vec<u8>) -> Result<Initramfs, String> {
        self.create_file_(Path::new(&path), contents, FILE_PERMISSIONS)
            .await
    }

    #[wasm_bindgen(js_name = createExecutable)]
    pub async fn create_executable(
        self,
        path: String,
        contents: Vec<u8>,
    ) -> Result<Initramfs, String> {
        self.create_file_(Path::new(&path), contents, EXECUTABLE_PERMISSIONS)
            .await
    }

    #[wasm_bindgen(js_name = intoAny)]
    pub fn into_any(self) -> AnyForWasm {
        AnyForWasm {
            inner: Any::Temporary(Rc::new(self.0)),
        }
    }
}

impl Default for Initramfs {
    fn default() -> Self {
        Self::new()
    }
}
