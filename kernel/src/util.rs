use flate2::read::GzDecoder;
use std::io::Read;
use types::io::{IoErrorKind, IoResult};

pub fn usize_plus_isize(unsigned: usize, signed: isize) -> Option<usize> {
    if signed.is_positive() {
        unsigned.checked_add(signed as usize)
    } else if signed.is_negative() {
        let absolute_value = signed
            .checked_neg()
            .map(|x| x as usize)
            .unwrap_or_else(|| 1usize.rotate_right(1));
        unsigned.checked_sub(absolute_value)
    } else {
        Some(unsigned)
    }
}

pub fn ungzip(bytes: &[u8]) -> IoResult<Vec<u8>> {
    let mut decoder = GzDecoder::new(bytes);
    let mut buffer = Vec::with_capacity(bytes.len());
    decoder
        .read_to_end(&mut buffer)
        .or(Err(IoErrorKind::CorruptedExecutable))?;
    Ok(buffer)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn usize_plus_isuze_works() {
        assert_eq!(usize_plus_isize(10, 32), Some(42));
        assert_eq!(
            usize_plus_isize(10, isize::MAX),
            Some(10 + 1usize.rotate_right(1) - 1)
        );
        assert_eq!(
            usize_plus_isize(1usize.rotate_right(1) + 1, isize::MAX),
            None
        );
        assert_eq!(usize_plus_isize(10, -3), Some(7));
        assert_eq!(usize_plus_isize(10, -52), None);
        assert_eq!(
            usize_plus_isize(usize::MAX, isize::MIN),
            Some(1usize.rotate_right(1) - 1)
        );
    }
}
