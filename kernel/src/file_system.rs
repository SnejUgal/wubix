use std::{collections::HashMap, future::Future};
use types::{
    fs::{Metadata, Permissions},
    ids::{GroupId, Inode, UserId},
    io::IoResult,
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use unix_str::{UnixStr, UnixString};

pub mod any;
pub mod dev;
pub mod idb;
pub mod pipe;
pub mod temporary;

pub use any::{Any, AnyForWasm};
pub use dev::DevFileSystem;
pub use idb::IdbFileSystem;
pub use temporary::TemporaryFileSystem;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum AnyEntry<F: File, D: Directory> {
    File(F),
    Directory(D),
}

pub trait FileSystem {
    type File: File;
    type Directory: Directory;

    type GetEntryFuture: Future<Output = IoResult<AnyEntry<Self::File, Self::Directory>>>;
    fn get_entry(&self, inode: Inode) -> Self::GetEntryFuture;

    type GetRootFuture: Future<Output = IoResult<Self::Directory>>;
    fn get_root(&self) -> Self::GetRootFuture;

    type CreateFileFuture: Future<Output = IoResult<Self::File>>;
    fn create_file(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateFileFuture;

    type CreateDirectoryFuture: Future<Output = IoResult<Self::Directory>>;
    fn create_directory(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateDirectoryFuture;
}

pub trait Entry {
    fn inode(&self) -> Inode;
}

pub trait File: Entry {
    type FileDescriptor: FileDescriptor;
    type OpenFuture: Future<Output = IoResult<Self::FileDescriptor>>;
    fn open(&self, mode: OpenMode) -> Self::OpenFuture;
}

pub trait FileDescriptor {
    type ReadMetadataFuture: Future<Output = IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture;

    type SetOwnerIdFuture: Future<Output = IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture;

    type SetOwnerGroupIdFuture: Future<Output = IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture;

    type SetPermissionsFuture: Future<Output = IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture;

    type ReadFuture: Future<Output = IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture;

    type WriteFuture: Future<Output = IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture;

    type FlushFuture: Future<Output = IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture;

    type SeekFuture: Future<Output = IoResult<usize>>;
    fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture;
}

pub trait Directory: Entry {
    type ReadMetadataFuture: Future<Output = IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture;

    type SetOwnerIdFuture: Future<Output = IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture;

    type SetOwnerGroupIdFuture: Future<Output = IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture;

    type SetPermissionsFuture: Future<Output = IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture;

    type ReadFuture: Future<Output = IoResult<HashMap<UnixString, Inode>>>;
    fn read(&self) -> Self::ReadFuture;

    type GetEntryFuture: Future<Output = IoResult<Inode>>;
    fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture;

    type HasEntryFuture: Future<Output = IoResult<bool>>;
    fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture;

    type PutEntryFuture: Future<Output = IoResult<()>>;
    fn put_entry(&self, name: &UnixStr, inode: Inode) -> Self::PutEntryFuture;

    type RemoveEntryFuture: Future<Output = IoResult<()>>;
    fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture;

    type IsEmptyFuture: Future<Output = IoResult<bool>>;
    fn is_empty(&self) -> Self::IsEmptyFuture;
}
