use super::{Directory, Entry, File, FileDescriptor, FileSystem};
use crate::util::usize_plus_isize;
use futures::future::{ready, Ready};
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    rc::{Rc, Weak},
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{DeviceId, GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use unix_str::{UnixStr, UnixString};

type AnyEntry = super::AnyEntry<TemporaryFile, TemporaryDirectory>;

const TMPFS_DEIVCE_MAJOR_NUMBER: u64 = 0x100;

thread_local! {
    static TMPFS_DEVICE_MINOR_NUMBER_COUNTER: Cell<u64> = Cell::new(1);
}

#[derive(Debug)]
struct TemporaryFsData {
    inodes: RefCell<HashMap<Inode, WeakInnerAnyEntry>>,
    minor_number: u64,
}

#[derive(Debug, Clone)]
pub struct TemporaryFileSystem {
    fs: Rc<TemporaryFsData>,
    next_inode: Cell<Inode>,
    root: InnerAnyEntry,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
struct InnerEntry<T> {
    inode: Inode,
    owner_id: UserId,
    group_id: GroupId,
    permissions: Permissions,
    contents: T,
}

#[derive(Debug, Clone)]
pub struct TemporaryFile {
    fs: Rc<TemporaryFsData>,
    inner: Rc<RefCell<InnerEntry<Vec<u8>>>>,
}

#[derive(Debug, Clone)]
pub struct TemporaryDescriptor {
    fs: Rc<TemporaryFsData>,
    mode: OpenMode,
    inner: Rc<RefCell<InnerEntry<Vec<u8>>>>,
    offset: Cell<usize>,
}

type DirectoryInner = InnerEntry<HashMap<UnixString, InnerAnyEntry>>;

#[derive(Debug, Clone)]
pub struct TemporaryDirectory {
    fs: Rc<TemporaryFsData>,
    inner: Rc<RefCell<DirectoryInner>>,
}

#[derive(Debug, Clone)]
enum InnerAnyEntry {
    File(Rc<RefCell<InnerEntry<Vec<u8>>>>),
    Directory(Rc<RefCell<DirectoryInner>>),
}

#[derive(Debug, Clone)]
enum WeakInnerAnyEntry {
    File(Weak<RefCell<InnerEntry<Vec<u8>>>>),
    Directory(Weak<RefCell<DirectoryInner>>),
}

impl Default for TemporaryFileSystem {
    fn default() -> Self {
        let root = InnerEntry {
            inode: Inode(1),
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: Permissions {
                owner: ClassPermissions::ALL,
                group: ClassPermissions::READ_ONLY_EXECUTABLE,
                others: ClassPermissions::READ_ONLY_EXECUTABLE,
                is_sticky: false,
                set_group_id: false,
                set_user_id: false,
            },
            contents: HashMap::new(),
        };
        let root = InnerAnyEntry::Directory(Rc::new(RefCell::new(root)));

        let mut inodes = HashMap::new();
        inodes.insert(Inode(1), root.downgrade());

        let minor_number = TMPFS_DEVICE_MINOR_NUMBER_COUNTER.with(|counter| {
            let minor_number = counter.get();
            counter.set(minor_number + 1);
            minor_number
        });
        let data = TemporaryFsData {
            inodes: RefCell::new(inodes),
            minor_number,
        };

        Self {
            fs: Rc::new(data),
            next_inode: Cell::new(Inode(2)),
            root,
        }
    }
}

impl Entry for TemporaryFile {
    fn inode(&self) -> Inode {
        self.inner.borrow().inode
    }
}

impl File for TemporaryFile {
    type FileDescriptor = TemporaryDescriptor;

    type OpenFuture = Ready<IoResult<Self::FileDescriptor>>;
    fn open(&self, mode: OpenMode) -> Self::OpenFuture {
        let mut offset = 0;

        match mode {
            OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => {
                if options.should_truncate {
                    self.inner.borrow_mut().contents.clear();
                }
                if options.should_append {
                    offset = self.inner.borrow().contents.len();
                }
            }
            _ => (),
        }

        ready(Ok(TemporaryDescriptor {
            fs: Rc::clone(&self.fs),
            mode,
            inner: Rc::clone(&self.inner),
            offset: Cell::new(offset),
        }))
    }
}

impl FileDescriptor for TemporaryDescriptor {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        let lock = self.inner.borrow();
        ready(Ok(Metadata {
            kind: EntryKind::File,
            device: DeviceId(TMPFS_DEIVCE_MAJOR_NUMBER | self.fs.minor_number),
            inode: lock.inode,
            owner_id: lock.owner_id,
            group_id: lock.group_id,
            permissions: lock.permissions,
            size: lock.contents.len(),
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
        self.inner.borrow_mut().owner_id = owner;
        ready(Ok(()))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
        self.inner.borrow_mut().group_id = group;
        ready(Ok(()))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
        self.inner.borrow_mut().permissions = permissions;
        ready(Ok(()))
    }

    type ReadFuture = Ready<IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        if let OpenMode::WriteOnly(_) = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        let lock = self.inner.borrow();
        let current_offset = self.offset.get();
        let slice = match lock.contents.get(current_offset..) {
            Some(slice) => slice,
            None => return ready(Err(IoErrorKind::InvalidOffset)),
        };
        let read_end = slice.len().min(count);
        self.offset.set(current_offset + read_end);

        ready(Ok(slice[..read_end].to_vec()))
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        let write_options = match self.mode {
            OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => options,
            OpenMode::ReadOnly => return ready(Err(IoErrorKind::PermissionDenied)),
        };

        let mut lock = self.inner.borrow_mut();
        let contents_len = lock.contents.len();
        let offset = if write_options.should_append {
            contents_len
        } else {
            self.offset.get()
        };
        if offset > lock.contents.len() {
            return ready(Err(IoErrorKind::InvalidOffset));
        }
        lock.contents
            .resize((offset + buffer.len()).max(contents_len), 0);
        lock.contents[offset..offset + buffer.len()].copy_from_slice(buffer);
        self.offset.set(offset + buffer.len());

        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture {
        match seek_from {
            SeekFrom::Start(offset) => self.offset.set(offset),
            SeekFrom::End(offset) => {
                let end = self.inner.borrow().contents.len();
                let new_offset = match usize_plus_isize(end, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
            SeekFrom::Current(offset) => {
                let current = self.offset.get();
                let new_offset = match usize_plus_isize(current, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
        }

        ready(Ok(self.offset.get()))
    }
}

impl Drop for TemporaryDescriptor {
    fn drop(&mut self) {
        if Rc::strong_count(&self.inner) == 1 {
            self.fs
                .inodes
                .borrow_mut()
                .remove(&self.inner.borrow().inode);
        }
    }
}

impl Entry for TemporaryDirectory {
    fn inode(&self) -> Inode {
        self.inner.borrow().inode
    }
}

impl Directory for TemporaryDirectory {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        let lock = self.inner.borrow();
        ready(Ok(Metadata {
            kind: EntryKind::Directory,
            device: DeviceId(TMPFS_DEIVCE_MAJOR_NUMBER | self.fs.minor_number),
            inode: lock.inode,
            owner_id: lock.owner_id,
            group_id: lock.group_id,
            permissions: lock.permissions,
            size: lock.contents.len(),
        }))
    }

    type ReadFuture = Ready<IoResult<HashMap<UnixString, Inode>>>;
    fn read(&self) -> Self::ReadFuture {
        let inner = self.inner.borrow();
        let entries = inner
            .contents
            .iter()
            .map(|(name, entry)| (name.clone(), entry.inode()))
            .collect();
        ready(Ok(entries))
    }

    type GetEntryFuture = Ready<IoResult<Inode>>;
    fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
        if let Some(entry) = self.inner.borrow().contents.get(name) {
            ready(Ok(entry.inode()))
        } else {
            ready(Err(IoErrorKind::NotFound))
        }
    }

    type HasEntryFuture = Ready<IoResult<bool>>;
    fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
        ready(Ok(self.inner.borrow().contents.contains_key(name)))
    }

    type PutEntryFuture = Ready<IoResult<()>>;
    fn put_entry(&self, name: &UnixStr, inode: Inode) -> Self::PutEntryFuture {
        let mut lock = self.inner.borrow_mut();

        if lock.contents.contains_key(name) {
            return ready(Err(IoErrorKind::AlreadyExists));
        }

        let inodes = self.fs.inodes.borrow();
        let entry = inodes
            .get(&inode)
            .and_then(WeakInnerAnyEntry::upgrade)
            .ok_or(IoErrorKind::NotFound);
        let entry = match entry {
            Ok(entry) => entry,
            Err(error) => return ready(Err(error)),
        };

        lock.contents.insert(name.to_owned(), entry);

        ready(Ok(()))
    }

    type RemoveEntryFuture = Ready<IoResult<()>>;
    fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture {
        if let Some(entry) = self.inner.borrow_mut().contents.remove(name) {
            if entry.strong_count() == 1 {
                self.fs.inodes.borrow_mut().remove(&entry.inode());
            }

            ready(Ok(()))
        } else {
            ready(Err(IoErrorKind::NotFound))
        }
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
        self.inner.borrow_mut().owner_id = owner;
        ready(Ok(()))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
        self.inner.borrow_mut().group_id = group;
        ready(Ok(()))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
        self.inner.borrow_mut().permissions = permissions;
        ready(Ok(()))
    }

    type IsEmptyFuture = Ready<IoResult<bool>>;
    fn is_empty(&self) -> Self::IsEmptyFuture {
        ready(Ok(self.inner.borrow().contents.is_empty()))
    }
}

impl FileSystem for TemporaryFileSystem {
    type File = TemporaryFile;
    type Directory = TemporaryDirectory;

    type GetEntryFuture = Ready<IoResult<AnyEntry>>;
    fn get_entry(&self, inode: Inode) -> Self::GetEntryFuture {
        let entry = self
            .fs
            .inodes
            .borrow()
            .get(&inode)
            .and_then(WeakInnerAnyEntry::upgrade)
            .ok_or(IoErrorKind::NotFound)
            .map(|entry| match entry {
                InnerAnyEntry::File(inner) => AnyEntry::File(TemporaryFile {
                    fs: Rc::clone(&self.fs),
                    inner: Rc::clone(&inner),
                }),
                InnerAnyEntry::Directory(inner) => AnyEntry::Directory(TemporaryDirectory {
                    fs: Rc::clone(&self.fs),
                    inner: Rc::clone(&inner),
                }),
            });

        ready(entry)
    }

    type GetRootFuture = Ready<IoResult<Self::Directory>>;
    fn get_root(&self) -> Self::GetRootFuture {
        if let InnerAnyEntry::Directory(root) = &self.root {
            ready(Ok(TemporaryDirectory {
                fs: Rc::clone(&self.fs),
                inner: Rc::clone(&root),
            }))
        } else {
            panic!("tmpfs root is a file");
        }
    }

    type CreateFileFuture = Ready<IoResult<Self::File>>;
    fn create_file(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateFileFuture {
        let inode = {
            let Inode(inode) = self.next_inode.get();
            self.next_inode.replace(Inode(inode + 1))
        };

        let mut lock = self.fs.inodes.borrow_mut();
        let entry = Rc::new(RefCell::new(InnerEntry {
            inode,
            owner_id,
            group_id,
            permissions,
            contents: Vec::new(),
        }));

        lock.insert(inode, WeakInnerAnyEntry::File(Rc::downgrade(&entry)));
        let file = TemporaryFile {
            fs: Rc::clone(&self.fs),
            inner: entry,
        };

        ready(Ok(file))
    }

    type CreateDirectoryFuture = Ready<IoResult<Self::Directory>>;
    fn create_directory(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateDirectoryFuture {
        let inode = {
            let Inode(inode) = self.next_inode.get();
            self.next_inode.replace(Inode(inode + 1))
        };

        let mut lock = self.fs.inodes.borrow_mut();
        let entry = Rc::new(RefCell::new(InnerEntry {
            inode,
            owner_id,
            group_id,
            permissions,
            contents: HashMap::new(),
        }));

        lock.insert(inode, WeakInnerAnyEntry::Directory(Rc::downgrade(&entry)));
        let directory = TemporaryDirectory {
            fs: Rc::clone(&self.fs),
            inner: entry,
        };

        ready(Ok(directory))
    }
}

impl InnerAnyEntry {
    fn downgrade(&self) -> WeakInnerAnyEntry {
        match self {
            Self::File(file) => WeakInnerAnyEntry::File(Rc::downgrade(file)),
            Self::Directory(directory) => WeakInnerAnyEntry::Directory(Rc::downgrade(directory)),
        }
    }

    fn inode(&self) -> Inode {
        match self {
            Self::File(file) => file.borrow().inode,
            Self::Directory(directory) => directory.borrow().inode,
        }
    }

    fn strong_count(&self) -> usize {
        match self {
            Self::File(file) => Rc::strong_count(file),
            Self::Directory(directory) => Rc::strong_count(directory),
        }
    }
}

impl WeakInnerAnyEntry {
    fn upgrade(&self) -> Option<InnerAnyEntry> {
        match self {
            Self::File(file) => file.upgrade().map(InnerAnyEntry::File),
            Self::Directory(directory) => directory.upgrade().map(InnerAnyEntry::Directory),
        }
    }
}
