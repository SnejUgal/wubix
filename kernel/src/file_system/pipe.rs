use super::FileDescriptor;
use crate::notify::Notify;
use futures::future::{ready, LocalBoxFuture, Ready};
use std::{
    cell::{Cell, RefCell},
    rc::Rc,
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{DeviceId, GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::seek::SeekFrom,
};

const PIPE_PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::READ_WRITE,
    group: ClassPermissions::NONE,
    others: ClassPermissions::NONE,
};

thread_local! {
    static INODE_COUNTER: Cell<Inode> = Cell::new(Inode(1));
}

const PIPE_DEVICE: DeviceId = DeviceId(0xd);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum PipeKind {
    ReadingHalf,
    WritingHalf,
}

#[derive(Debug)]
pub struct Pipe {
    kind: PipeKind,
    buffer: Rc<RefCell<Vec<u8>>>,
    inode: Inode,
    user_id: UserId,
    group_id: GroupId,
    is_write_half_closed: Rc<Cell<bool>>,
    write_waiters: Rc<Notify>,
}

impl FileDescriptor for Pipe {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::Pipe,
            device: PIPE_DEVICE,
            inode: self.inode,
            owner_id: self.user_id,
            group_id: self.group_id,
            permissions: PIPE_PERMISSIONS,
            size: 0,
        }))
    }

    type ReadFuture = LocalBoxFuture<'static, IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        let kind = self.kind;
        let buffer = Rc::clone(&self.buffer);
        let is_write_half_closed = Rc::clone(&self.is_write_half_closed);
        let input_waiters = Rc::clone(&self.write_waiters);

        Box::pin(async move {
            if kind == PipeKind::WritingHalf {
                return Err(IoErrorKind::PermissionDenied);
            }

            if buffer.borrow().is_empty() {
                if is_write_half_closed.get() {
                    return Ok(Vec::new());
                }
                input_waiters.wait().await;
            }

            let mut buffer = buffer.borrow_mut();
            let read_end = buffer.len().min(count);
            let mut read = buffer.split_off(read_end);
            std::mem::swap(&mut read, &mut *buffer);
            Ok(read)
        })
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        if self.kind == PipeKind::ReadingHalf {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        if buffer.is_empty() {
            return ready(Ok(0));
        }

        self.buffer.borrow_mut().extend_from_slice(buffer);
        self.write_waiters.notify_all();
        ready(Ok(buffer.len()))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, _: SeekFrom) -> Self::SeekFuture {
        ready(Err(IoErrorKind::UnsupportedOperation))
    }
}

impl Drop for Pipe {
    fn drop(&mut self) {
        if self.kind == PipeKind::WritingHalf {
            self.is_write_half_closed.set(true);
            self.write_waiters.notify_all();
        }
    }
}

pub fn create_pipe(user_id: UserId, group_id: GroupId) -> (Pipe, Pipe) {
    let buffer = Rc::new(RefCell::new(Vec::new()));
    let inode = INODE_COUNTER.with(|counter| {
        let Inode(current_inode) = counter.get();
        counter.replace(Inode(current_inode + 1))
    });
    let write_waiters = Rc::new(Notify::default());
    let is_write_half_closed = Rc::new(Cell::new(false));

    let reading_half = Pipe {
        kind: PipeKind::ReadingHalf,
        buffer: Rc::clone(&buffer),
        inode,
        user_id,
        group_id,
        is_write_half_closed: Rc::clone(&is_write_half_closed),
        write_waiters: Rc::clone(&write_waiters),
    };

    let writing_half = Pipe {
        kind: PipeKind::WritingHalf,
        buffer,
        inode,
        user_id,
        group_id,
        is_write_half_closed,
        write_waiters,
    };

    (reading_half, writing_half)
}
