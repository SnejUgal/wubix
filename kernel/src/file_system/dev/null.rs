use crate::file_system::FileDescriptor;
use std::future::{ready, Ready};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};

use super::DEVFS_DEVICE;

const PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::READ_WRITE,
    group: ClassPermissions::READ_WRITE,
    others: ClassPermissions::READ_WRITE,
};

#[derive(Debug, Clone)]
pub struct NullDevice {
    pub inode: Inode,
    pub mode: OpenMode,
}

impl FileDescriptor for NullDevice {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::CharacterDevice,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = Ready<IoResult<Vec<u8>>>;
    fn read(&self, _: usize) -> Self::ReadFuture {
        if let OpenMode::WriteOnly(_) = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        ready(Ok(Vec::new()))
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        if let OpenMode::ReadOnly = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, _: SeekFrom) -> Self::SeekFuture {
        ready(Err(IoErrorKind::UnsupportedOperation))
    }
}
