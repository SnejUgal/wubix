use crate::{
    file_system::{Directory, Entry, FileDescriptor},
    util::usize_plus_isize,
};
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    future::{ready, Ready},
    rc::Rc,
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use unix_str::{UnixStr, UnixString};
use web_sys::{window, Storage};

use super::{DevFile, DevFsData, DeviceKind, InnerAnyEntry, DEVFS_DEVICE};

const DIRECTORY_PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::ALL,
    group: ClassPermissions::READ_ONLY_EXECUTABLE,
    others: ClassPermissions::READ_ONLY_EXECUTABLE,
};

const FILE_PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::READ_WRITE,
    group: ClassPermissions::READ_ONLY,
    others: ClassPermissions::READ_ONLY,
};

#[derive(Debug, Clone)]
pub struct LocalStorage {
    fs: Rc<DevFsData>,
    inode: Inode,
    assigned_inodes: Rc<RefCell<HashMap<String, Inode>>>,
    storage: Storage,
}

#[derive(Debug, Clone)]
pub struct LocalStorageKey {
    inode: Inode,
    key: String,
    storage: Storage,
    value: RefCell<Vec<u8>>,
    mode: OpenMode,
    offset: Rc<Cell<usize>>,
}

impl LocalStorage {
    pub(super) fn new(inode: Inode, fs: Rc<DevFsData>) -> Self {
        Self {
            fs,
            inode,
            assigned_inodes: Rc::new(RefCell::new(HashMap::new())),
            storage: get_local_storage(),
        }
    }

    fn get_entries(&self) -> impl Iterator<Item = (UnixString, Inode)> + '_ {
        let length = self.storage.length().unwrap();

        (0..length).map(move |index| {
            let key = self.storage.key(index).unwrap().unwrap();

            let inode = self.assigned_inodes.borrow().get(&key).copied();
            let inode = if let Some(inode) = inode {
                inode
            } else {
                self.assign_inode(&key, None)
            };

            (UnixString::from(key), inode)
        })
    }

    fn assign_inode(&self, key: &str, inode: Option<Inode>) -> Inode {
        let inode = if let Some(inode) = inode {
            inode
        } else {
            let inode = self.fs.next_inode.get();
            self.fs.next_inode.set(Inode(inode.0 + 1));
            inode
        };
        self.assigned_inodes
            .borrow_mut()
            .insert(key.to_owned(), inode);
        self.fs.inodes.borrow_mut().insert(
            inode,
            InnerAnyEntry::File(DevFile {
                inode,
                device_kind: DeviceKind::LocalStorageKey {
                    key: key.to_owned(),
                },
            }),
        );
        inode
    }
}

impl Entry for LocalStorage {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl Directory for LocalStorage {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::Directory,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: DIRECTORY_PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = Ready<IoResult<HashMap<UnixString, Inode>>>;
    fn read(&self) -> Self::ReadFuture {
        ready(Ok(self.get_entries().collect()))
    }

    type GetEntryFuture = Ready<IoResult<Inode>>;
    fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
        let name = match name.to_str() {
            Some(name) => name,
            None => return ready(Err(IoErrorKind::NotFound)),
        };

        if self.storage.get_item(name).unwrap().is_none() {
            return ready(Err(IoErrorKind::NotFound));
        }

        let inode = self.assigned_inodes.borrow().get(name).copied();
        let inode = if let Some(inode) = inode {
            inode
        } else {
            self.assign_inode(name, None)
        };

        ready(Ok(inode))
    }

    type HasEntryFuture = Ready<IoResult<bool>>;
    fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
        ready(Ok(self.get_entries().any(|(key, _)| key == name)))
    }

    type PutEntryFuture = Ready<IoResult<()>>;
    fn put_entry(&self, name: &UnixStr, inode: Inode) -> Self::PutEntryFuture {
        let name = match name.to_str() {
            Some(name) => name,
            None => return ready(Err(IoErrorKind::InvalidArgument)),
        };

        if self.storage.get_item(name).unwrap().is_some() {
            return ready(Err(IoErrorKind::AlreadyExists));
        }

        if self.fs.created_files.borrow_mut().remove(&inode).is_none() {
            return ready(Err(IoErrorKind::InvalidArgument));
        }

        self.assign_inode(name, Some(inode));
        self.storage.set_item(name, "").unwrap();
        ready(Ok(()))
    }

    type RemoveEntryFuture = Ready<IoResult<()>>;
    fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture {
        let name = match name.to_str() {
            Some(name) => name,
            None => return ready(Err(IoErrorKind::NotFound)),
        };

        if self.storage.get_item(name).unwrap().is_none() {
            return ready(Err(IoErrorKind::NotFound));
        }

        self.storage.remove_item(name).unwrap();
        self.assigned_inodes.borrow_mut().remove(name);
        ready(Ok(()))
    }

    type IsEmptyFuture = Ready<IoResult<bool>>;
    fn is_empty(&self) -> Self::IsEmptyFuture {
        ready(Ok(self.storage.length().unwrap() == 0))
    }
}

impl LocalStorageKey {
    pub fn new(inode: Inode, key: String, mode: OpenMode) -> IoResult<Self> {
        let storage = get_local_storage();
        let should_truncate = match mode {
            OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => options.should_truncate,
            OpenMode::ReadOnly => false,
        };
        let value = if should_truncate {
            Vec::new()
        } else {
            storage
                .get_item(&key)
                .unwrap()
                .ok_or(IoErrorKind::NotFound)?
                .into_bytes()
        };

        Ok(Self {
            inode,
            key,
            value: RefCell::new(value),
            storage,
            mode,
            offset: Rc::new(Cell::new(0)),
        })
    }
}

impl Entry for LocalStorageKey {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl FileDescriptor for LocalStorageKey {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::CharacterDevice,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: FILE_PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = Ready<IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        if let OpenMode::WriteOnly(_) = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        let value = self.value.borrow();
        let offset = self.offset.get();
        let length = count.min(value.len() - offset);
        self.offset.set(offset + length);
        ready(Ok(value[offset..offset + length].to_owned()))
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        let write_options = match self.mode {
            OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => options,
            OpenMode::ReadOnly => return ready(Err(IoErrorKind::PermissionDenied)),
        };

        let mut value = self.value.borrow_mut();
        let contents_len = value.len();
        let offset = if write_options.should_append {
            contents_len
        } else {
            self.offset.get()
        };
        value.resize((offset + buffer.len()).max(contents_len), 0);
        value[offset..offset + buffer.len()].copy_from_slice(buffer);
        self.offset.set(offset + buffer.len());

        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        let value = self.value.borrow();
        let value = String::from_utf8_lossy(&value);
        if self.storage.set_item(&self.key, &value).is_err() {
            return ready(Err(IoErrorKind::NoSpace));
        }

        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture {
        match seek_from {
            SeekFrom::Start(offset) => self.offset.set(offset),
            SeekFrom::End(offset) => {
                let end = self.value.borrow().len();
                let new_offset = match usize_plus_isize(end, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
            SeekFrom::Current(offset) => {
                let current = self.offset.get();
                let new_offset = match usize_plus_isize(current, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
        }

        ready(Ok(self.offset.get()))
    }
}

impl Drop for LocalStorageKey {
    fn drop(&mut self) {
        let _ = self.flush();
    }
}

fn get_local_storage() -> Storage {
    window().unwrap().local_storage().unwrap().unwrap()
}
