use crate::{
    file_system::FileDescriptor,
    frame_buffer::FrameBuffer,
    virtual_terminal::{VirtualTerminal, VirtualTerminals},
};
use futures::future::{LocalBoxFuture};
use std::{
    cell::{Cell, RefCell},
    fmt::Write,
    rc::Rc,future::{ready, Ready}
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};

use super::DEVFS_DEVICE;

const PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::READ_WRITE,
    group: ClassPermissions::WRITE_ONLY,
    others: ClassPermissions::NONE,
};

#[derive(Debug, Clone)]
pub struct TtyDevice {
    pub inode: Inode,
    pub mode: OpenMode,
    pub output_buffer: Rc<RefCell<Vec<u8>>>,
    pub input_buffer: Rc<RefCell<Vec<u8>>>,
    pub terminal: Rc<RefCell<VirtualTerminal>>,
    pub frame_buffer: Rc<RefCell<FrameBuffer>>,
}

#[derive(Debug, Clone)]
pub struct Tty0Device {
    pub inode: Inode,
    pub mode: OpenMode,
    pub output_buffer: Rc<RefCell<Vec<u8>>>,
    pub input_buffer: Rc<RefCell<Vec<u8>>>,
    pub terminals: VirtualTerminals,
    pub active: Rc<Cell<usize>>,
    pub frame_buffer: Rc<RefCell<FrameBuffer>>,
}

impl FileDescriptor for TtyDevice {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::CharacterDevice,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(5),
            permissions: PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
        self.terminal.borrow_mut().owner = owner;
        ready(Ok(()))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = LocalBoxFuture<'static, IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        let this = self.clone();
        Box::pin(async move {
            if !this.input_buffer.borrow().is_empty() {
                let old_buffer = &mut *this.input_buffer.borrow_mut();
                let mut new_buffer = old_buffer.split_off(count.min(old_buffer.len()));
                std::mem::swap(old_buffer, &mut new_buffer);
                return Ok(new_buffer);
            }

            loop {
                let mut terminal = this.terminal.borrow_mut();
                if terminal.input.len() > 1 {
                    let line = terminal.input.pop_front().unwrap().into_bytes();
                    if line.len() > count {
                        *this.input_buffer.borrow_mut() = line[count..].to_vec();
                        return Ok(line[..count].to_vec());
                    }
                    return Ok(line);
                }
                let waiter = terminal.input_waiters.wait();
                drop(terminal);
                waiter.await
            }
        })
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        self.output_buffer.borrow_mut().extend_from_slice(buffer);
        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        let mut terminal = self.terminal.borrow_mut();
        let output = std::mem::take(&mut *self.output_buffer.borrow_mut());
        terminal
            .write_str(&String::from_utf8_lossy(&output))
            .unwrap();
        terminal.render(self.frame_buffer.borrow_mut().lock());
        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, _: SeekFrom) -> Self::SeekFuture {
        ready(Ok(0))
    }
}

impl FileDescriptor for Tty0Device {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::CharacterDevice,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(5),
            permissions: PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::UnsupportedOperation))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = LocalBoxFuture<'static, IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        let this = self.clone();
        Box::pin(async move {
            if !this.input_buffer.borrow().is_empty() {
                let old_buffer = &mut *this.input_buffer.borrow_mut();
                let mut new_buffer = old_buffer.split_off(count.min(old_buffer.len()));
                std::mem::swap(old_buffer, &mut new_buffer);
                return Ok(new_buffer);
            }

            let active = this.active.get();
            let terminals = this.terminals.borrow();
            loop {
                let mut terminal = terminals[active].borrow_mut();
                if terminal.input.len() > 1 {
                    let line = terminal.input.pop_front().unwrap().into_bytes();
                    if line.len() > count {
                        *this.input_buffer.borrow_mut() = line[count..].to_vec();
                        return Ok(line[..count].to_vec());
                    }
                    return Ok(line);
                }
                let waiter = terminal.input_waiters.wait();
                drop(terminal);
                waiter.await
            }
        })
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        self.output_buffer.borrow_mut().extend_from_slice(buffer);
        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        let active_terminal = self.active.get();
        let terminal = Rc::clone(&self.terminals.borrow()[active_terminal]);
        let mut terminal = terminal.borrow_mut();

        let output = std::mem::take(&mut *self.output_buffer.borrow_mut());
        terminal
            .write_str(&String::from_utf8_lossy(&output))
            .unwrap();
        terminal.render(self.frame_buffer.borrow_mut().lock());
        ready(Ok(()))
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, _: SeekFrom) -> Self::SeekFuture {
        ready(Ok(0))
    }
}

impl Drop for TtyDevice {
    fn drop(&mut self) {
        let _ = self.flush();
    }
}

impl Drop for Tty0Device {
    fn drop(&mut self) {
        let _ = self.flush();
    }
}
