use crate::file_system::FileDescriptor;
use std::{cell::RefCell, future::{ready, Ready}};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use wasm_bindgen::prelude::*;

use super::DEVFS_DEVICE;

const PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::READ_WRITE,
    group: ClassPermissions::NONE,
    others: ClassPermissions::NONE,
};

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = kernel, js_name = executeCode, catch)]
    fn execute_code(code: String) -> Result<String, JsValue>;
}

/// This is a device for executing JS code on the main thread. This has a few useful applications,
/// for example:
///
/// - `upload-file` needs to add an `<input type="file">` element to the DOM, click on the element
///   when the user presses ENTER to open a `Choose file` dialog, and then send the file back
///   to its worker, which will then write the file to the file system;
/// - A window manager would like to manipulate the canvas directly than via `/dev/fb0` for more
///   performance.
///
/// However, the main thread is also where the kernel runs and contains kernel structures and direct
/// kernel calls. For this reason, only root can write to this device.
///
/// For the executed code to communicate with the process, `window.processes` of type
/// `Map<number, Worker>`, where the key is the PID, is available. The code may post messages
/// to the worker. It is recommended that the messages don't have any other properties other than
/// `custom`, which then may be of any type, to not interfer with Wubix's message handlers.
/// The process may set up its own message handler which will process these messages.
///
/// This device buffers the code written to it. Once a read or flush occurs, the code is executed
/// via `new Function`, its return value is serialized with `JSON.stringify` and available
/// for reading from this device. If the code throws an exception, flushing or reading results
/// in an error.
#[derive(Debug, Clone)]
pub struct MainThreadDevice {
    pub inode: Inode,
    pub mode: OpenMode,
    pub code: RefCell<Vec<u8>>,
    pub evaluation_result: RefCell<Vec<u8>>,
}

impl MainThreadDevice {
    fn execute_code(&self) -> IoResult<()> {
        let code = String::from_utf8(std::mem::take(&mut *self.code.borrow_mut()))
            .or(Err(IoErrorKind::InvalidArgument))?;
        if let Ok(result) = execute_code(code) {
            self.evaluation_result
                .borrow_mut()
                .extend_from_slice(&result.as_bytes());
            Ok(())
        } else {
            Err(IoErrorKind::CorruptedExecutable)
        }
    }
}

impl FileDescriptor for MainThreadDevice {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::CharacterDevice,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: PERMISSIONS,
            size: 0,
        }))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type ReadFuture = Ready<IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        if let OpenMode::WriteOnly(_) = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        if !self.code.borrow().is_empty() {
            if let Err(error) = self.execute_code() {
                return ready(Err(error));
            }
        }

        let evaluation_result = &mut *self.evaluation_result.borrow_mut();
        let mut read = evaluation_result.split_off(count);
        std::mem::swap(evaluation_result, &mut read);
        ready(Ok(read.to_vec()))
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        if let OpenMode::ReadOnly = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        self.code.borrow_mut().extend_from_slice(buffer);
        ready(Ok(buffer.len()))
    }

    type FlushFuture = Ready<IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        ready(self.execute_code())
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, _: SeekFrom) -> Self::SeekFuture {
        ready(Err(IoErrorKind::UnsupportedOperation))
    }
}

impl Drop for MainThreadDevice {
    fn drop(&mut self) {
        let _ = self.execute_code();
    }
}
