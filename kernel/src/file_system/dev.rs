use super::{Directory, Entry, File, FileDescriptor, FileSystem};
use crate::{
    frame_buffer::FrameBuffer,
    virtual_terminal::{VirtualTerminal, VirtualTerminals},
};
use futures::future::LocalBoxFuture;
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    future::{ready, Ready},
    rc::Rc,
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{DeviceId, GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};

mod local_storage;
mod main_thread;
mod null;
mod tty;
mod zero;

use unix_str::{UnixStr, UnixString};
pub use {local_storage::*, main_thread::*, null::*, tty::*, zero::*};

const DIRECTORY_PERMISSIONS: Permissions = Permissions {
    set_user_id: false,
    set_group_id: false,
    is_sticky: false,
    owner: ClassPermissions::ALL,
    group: ClassPermissions::READ_ONLY_EXECUTABLE,
    others: ClassPermissions::READ_ONLY_EXECUTABLE,
};
const DEVFS_DEVICE: DeviceId = DeviceId(6);

type AnyEntry = super::AnyEntry<DevFile, DevDirectory>;

#[derive(Debug, Clone)]
struct DevFsData {
    inodes: RefCell<HashMap<Inode, InnerAnyEntry>>,
    next_inode: Cell<Inode>,
    created_files: RefCell<HashMap<Inode, (UserId, GroupId, Permissions)>>,
}

#[derive(Debug, Clone)]
pub struct DevFileSystem {
    data: Rc<DevFsData>,
}

#[derive(Debug, Clone)]
pub enum InnerAnyEntry {
    File(DevFile),
    Directory(DevDirectory),
}

#[derive(Debug, Clone)]
pub enum DevDirectory {
    Root(Root),
    LocalStorage(LocalStorage),
}

#[derive(Debug, Clone)]
pub struct DevFile {
    inode: Inode,
    device_kind: DeviceKind,
}

#[derive(Debug, Clone)]
pub struct Root {
    inode: Inode,
    contents: Rc<RefCell<HashMap<UnixString, Inode>>>,
}

#[derive(Debug, Clone)]
enum DeviceKind {
    Null,
    Zero,
    Tty {
        terminal: Rc<RefCell<VirtualTerminal>>,
        frame_buffer: Rc<RefCell<FrameBuffer>>,
    },
    Tty0 {
        terminals: VirtualTerminals,
        active: Rc<Cell<usize>>,
        frame_buffer: Rc<RefCell<FrameBuffer>>,
    },
    LocalStorageKey {
        key: String,
    },
    MainThread,
    Placeholder,
}

#[derive(Debug, Clone)]
pub enum Device {
    Null(NullDevice),
    Zero(ZeroDevice),
    Tty0(Tty0Device),
    Tty(TtyDevice),
    MainThread(MainThreadDevice),
    LocalStorageKey(LocalStorageKey),
}

impl DevFileSystem {
    pub fn new(
        virtual_terminals: VirtualTerminals,
        active_virtual_terminal: Rc<Cell<usize>>,
        frame_buffer: Rc<RefCell<FrameBuffer>>,
    ) -> Self {
        let mut inodes = HashMap::new();
        let mut root_contents = HashMap::new();

        inodes.insert(
            Inode(2),
            InnerAnyEntry::File(DevFile {
                inode: Inode(2),
                device_kind: DeviceKind::Null,
            }),
        );
        root_contents.insert(UnixString::from("null"), Inode(2));

        inodes.insert(
            Inode(3),
            InnerAnyEntry::File(DevFile {
                inode: Inode(3),
                device_kind: DeviceKind::Zero,
            }),
        );
        root_contents.insert(UnixString::from("zero"), Inode(3));

        inodes.insert(
            Inode(4),
            InnerAnyEntry::File(DevFile {
                inode: Inode(4),
                device_kind: DeviceKind::Tty0 {
                    terminals: Rc::clone(&virtual_terminals),
                    active: active_virtual_terminal,
                    frame_buffer: Rc::clone(&frame_buffer),
                },
            }),
        );
        root_contents.insert(UnixString::from("tty0"), Inode(4));

        inodes.insert(
            Inode(5),
            InnerAnyEntry::File(DevFile {
                inode: Inode(5),
                device_kind: DeviceKind::MainThread,
            }),
        );
        root_contents.insert(UnixString::from("main_thread"), Inode(5));

        let next_inode = Cell::new(Inode(7));
        let inodes = RefCell::new(inodes);
        let data = Rc::new(DevFsData {
            inodes,
            next_inode,
            created_files: RefCell::new(HashMap::new()),
        });

        data.inodes.borrow_mut().insert(
            Inode(6),
            InnerAnyEntry::Directory(DevDirectory::LocalStorage(LocalStorage::new(
                Inode(6),
                Rc::clone(&data),
            ))),
        );
        root_contents.insert(UnixString::from("local_storage"), Inode(6));

        for (index, terminal) in virtual_terminals.borrow().iter().enumerate() {
            let inode = data.next_inode.get();
            data.next_inode.set(Inode(inode.0 + 1));

            data.inodes.borrow_mut().insert(
                inode,
                InnerAnyEntry::File(DevFile {
                    inode,
                    device_kind: DeviceKind::Tty {
                        terminal: Rc::clone(&terminal),
                        frame_buffer: Rc::clone(&frame_buffer),
                    },
                }),
            );
            let name = UnixString::from(format!("tty{}", index + 1));
            root_contents.insert(name, inode);
        }

        data.inodes.borrow_mut().insert(
            Inode(1),
            InnerAnyEntry::Directory(DevDirectory::Root(Root {
                inode: Inode(1),
                contents: Rc::new(RefCell::new(root_contents)),
            })),
        );

        Self { data }
    }
}

impl FileSystem for DevFileSystem {
    type File = DevFile;
    type Directory = DevDirectory;

    type GetEntryFuture = Ready<IoResult<AnyEntry>>;
    fn get_entry(&self, inode: Inode) -> Self::GetEntryFuture {
        let entry = self
            .data
            .inodes
            .borrow()
            .get(&inode)
            .ok_or(IoErrorKind::NotFound)
            .map(|entry| match entry {
                InnerAnyEntry::File(file) => AnyEntry::File(file.clone()),
                InnerAnyEntry::Directory(directory) => AnyEntry::Directory(directory.clone()),
            });

        ready(entry)
    }

    type GetRootFuture = Ready<IoResult<Self::Directory>>;
    fn get_root(&self) -> Self::GetRootFuture {
        let inode = Inode(1);
        let lock = self.data.inodes.borrow();

        let root = lock.get(&inode).expect("No devfs root");
        let dir = match root {
            InnerAnyEntry::Directory(directory) => directory.clone(),
            InnerAnyEntry::File(_) => panic!("devfs root is a file"),
        };

        ready(Ok(dir))
    }

    type CreateFileFuture = Ready<IoResult<Self::File>>;
    fn create_file(
        &self,
        user_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateFileFuture {
        let inode = self.data.next_inode.get();
        self.data.next_inode.set(Inode(inode.0 + 1));
        self.data
            .created_files
            .borrow_mut()
            .insert(inode, (user_id, group_id, permissions));
        let file = DevFile {
            inode,
            device_kind: DeviceKind::Placeholder,
        };
        ready(Ok(file))
    }

    type CreateDirectoryFuture = Ready<IoResult<Self::Directory>>;
    fn create_directory(
        &self,
        _: UserId,
        _: GroupId,
        _: Permissions,
    ) -> Self::CreateDirectoryFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }
}

impl Entry for DevFile {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl File for DevFile {
    type FileDescriptor = Device;
    type OpenFuture = Ready<IoResult<Self::FileDescriptor>>;

    fn open(&self, mode: OpenMode) -> Self::OpenFuture {
        ready(Ok(match &self.device_kind {
            DeviceKind::Null => Device::Null(NullDevice {
                inode: self.inode,
                mode,
            }),
            DeviceKind::Zero => Device::Zero(ZeroDevice {
                inode: self.inode,
                mode,
            }),
            DeviceKind::Tty {
                terminal,
                frame_buffer,
            } => Device::Tty(TtyDevice {
                inode: self.inode,
                mode,
                output_buffer: Rc::new(RefCell::new(Vec::new())),
                input_buffer: Rc::new(RefCell::new(Vec::new())),
                terminal: Rc::clone(terminal),
                frame_buffer: Rc::clone(frame_buffer),
            }),
            DeviceKind::Tty0 {
                terminals,
                active,
                frame_buffer,
            } => Device::Tty0(Tty0Device {
                inode: self.inode,
                mode,
                output_buffer: Rc::new(RefCell::new(Vec::new())),
                input_buffer: Rc::new(RefCell::new(Vec::new())),
                terminals: Rc::clone(terminals),
                active: Rc::clone(active),
                frame_buffer: Rc::clone(frame_buffer),
            }),
            DeviceKind::MainThread => Device::MainThread(MainThreadDevice {
                inode: self.inode,
                mode,
                code: RefCell::new(Vec::new()),
                evaluation_result: RefCell::new(Vec::new()),
            }),
            DeviceKind::LocalStorageKey { key } => {
                match LocalStorageKey::new(self.inode, key.clone(), mode) {
                    Ok(device) => Device::LocalStorageKey(device),
                    Err(error) => return ready(Err(error)),
                }
            }
            DeviceKind::Placeholder => unreachable!(
                "tried to open a device file which wasn't put in devfs and therefore hasn't been
                 assigned a backing device"
            ),
        }))
    }
}

impl Entry for Root {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl Directory for Root {
    type ReadMetadataFuture = Ready<IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        ready(Ok(Metadata {
            kind: EntryKind::Directory,
            device: DEVFS_DEVICE,
            inode: self.inode,
            owner_id: UserId(0),
            group_id: GroupId(0),
            permissions: DIRECTORY_PERMISSIONS,
            size: self.contents.borrow().len(),
        }))
    }

    type ReadFuture = Ready<IoResult<HashMap<UnixString, Inode>>>;
    fn read(&self) -> Self::ReadFuture {
        ready(Ok(self.contents.borrow().clone()))
    }

    type GetEntryFuture = Ready<IoResult<Inode>>;
    fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
        if let Some(&inode) = self.contents.borrow().get(name) {
            ready(Ok(inode))
        } else {
            ready(Err(IoErrorKind::NotFound))
        }
    }

    type HasEntryFuture = Ready<IoResult<bool>>;
    fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
        ready(Ok(self.contents.borrow().contains_key(name)))
    }

    type PutEntryFuture = Ready<IoResult<()>>;
    fn put_entry(&self, _: &UnixStr, _: Inode) -> Self::PutEntryFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type RemoveEntryFuture = Ready<IoResult<()>>;
    fn remove_entry(&self, _: &UnixStr) -> Self::RemoveEntryFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerIdFuture = Ready<IoResult<()>>;
    fn set_owner_id(&self, _: UserId) -> Self::SetOwnerIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
    fn set_owner_group_id(&self, _: GroupId) -> Self::SetOwnerGroupIdFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type SetPermissionsFuture = Ready<IoResult<()>>;
    fn set_permissions(&self, _: Permissions) -> Self::SetPermissionsFuture {
        ready(Err(IoErrorKind::PermissionDenied))
    }

    type IsEmptyFuture = Ready<IoResult<bool>>;
    fn is_empty(&self) -> Self::IsEmptyFuture {
        ready(Ok(self.contents.borrow().is_empty()))
    }
}

macro_rules! impl_descriptor {
    ($($var:ident),*) => {
        impl FileDescriptor for Device {
            type ReadMetadataFuture = Ready<IoResult<Metadata>>;
            fn read_metadata(&self) -> Self::ReadMetadataFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.read_metadata()
                    ),*
                }
            }

            type SetOwnerIdFuture = Ready<IoResult<()>>;
            fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.set_owner_id(owner)
                    ),*
                }
            }

            type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
            fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.set_owner_group_id(group)
                    ),*
                }
            }

            type SetPermissionsFuture = Ready<IoResult<()>>;
            fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.set_permissions(permissions)
                    ),*
                }
            }

            type ReadFuture = LocalBoxFuture<'static, IoResult<Vec<u8>>>;
            fn read(&self, count: usize) -> Self::ReadFuture {
                let this = self.clone();
                Box::pin(async move {
                    match this {
                        $(
                            Self::$var(descriptor) => descriptor.read(count).await
                        ),*
                    }
                })
            }

            type WriteFuture = Ready<IoResult<usize>>;
            fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.write(buffer)
                    ),*
                }
            }

            type FlushFuture = Ready<IoResult<()>>;
            fn flush(&self) -> Self::FlushFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.flush()
                    ),*
                }
            }

            type SeekFuture = Ready<IoResult<usize>>;
            fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture {
                match self {
                    $(
                        Self::$var(descriptor) => descriptor.seek(seek_from)
                    ),*
                }
            }
        }
    }
}

impl_descriptor!(Zero, Null, Tty, Tty0, MainThread, LocalStorageKey);

macro_rules! impl_directory {
    ($($var:ident),*) => {
        impl Entry for DevDirectory {
            fn inode(&self) -> Inode {
                match self {
                    $(
                        Self::$var(directory) => directory.inode()
                    ),*
                }
            }
        }

        impl Directory for DevDirectory {
            type ReadMetadataFuture = Ready<IoResult<Metadata>>;
            fn read_metadata(&self) -> Self::ReadMetadataFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.read_metadata()
                    ),*
                }
            }

            type SetOwnerIdFuture = Ready<IoResult<()>>;
            fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.set_owner_id(owner)
                    ),*
                }
            }

            type SetOwnerGroupIdFuture = Ready<IoResult<()>>;
            fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.set_owner_group_id(group)
                    ),*
                }
            }

            type SetPermissionsFuture = Ready<IoResult<()>>;
            fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.set_permissions(permissions)
                    ),*
                }
            }

            type ReadFuture = Ready<IoResult<HashMap<UnixString, Inode>>>;
            fn read(&self) -> Self::ReadFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.read()
                    ),*
                }
            }

            type GetEntryFuture = Ready<IoResult<Inode>>;
            fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.get_entry(name)
                    ),*
                }
            }

            type HasEntryFuture = Ready<IoResult<bool>>;
            fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.has_entry(name)
                    ),*
                }
            }

            type PutEntryFuture = Ready<IoResult<()>>;
            fn put_entry(&self, name: &UnixStr, inode: Inode) -> Self::PutEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.put_entry(name, inode)
                    ),*
                }
            }

            type RemoveEntryFuture = Ready<IoResult<()>>;
            fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.remove_entry(name)
                    ),*
                }
            }

            type IsEmptyFuture = Ready<IoResult<bool>>;
            fn is_empty(&self) -> Self::IsEmptyFuture {
                match self {
                    $(
                        Self::$var(directory) => directory.is_empty()
                    ),*
                }
            }
        }
    }
}

impl_directory!(Root, LocalStorage);
