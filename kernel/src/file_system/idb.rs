use super::{Directory, Entry, File, FileDescriptor, FileSystem};
use crate::util::usize_plus_isize;
use futures::future::{ready, LocalBoxFuture, Ready};
use js_sys::Promise;
use serde::{
    de::{DeserializeOwned, Deserializer},
    ser::{SerializeSeq, Serializer},
    Deserialize, Serialize,
};
use std::{
    cell::{Cell, RefCell},
    collections::HashMap,
    hash::Hash,
    rc::Rc,
};
use types::{
    fs::{ClassPermissions, EntryKind, Metadata, Permissions},
    ids::{DeviceId, GroupId, Inode, UserId},
    io::{IoErrorKind, IoResult},
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use unix_str::{UnixStr, UnixString};
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = idbfs)]
    fn mount(id: String) -> Promise;

    #[wasm_bindgen(js_namespace = idbfs, js_name = getInode)]
    fn get_inode(id: String, inode: u32) -> Promise;

    #[wasm_bindgen(js_namespace = idbfs, js_name = createInode)]
    fn create_inode(id: String, value: JsValue) -> Promise;

    #[wasm_bindgen(js_namespace = idbfs, js_name = updateInode)]
    fn update_inode(id: String, inode: u32, value: JsValue) -> Promise;

    #[wasm_bindgen(js_namespace = idbfs, js_name = readFile)]
    fn read_file(id: String, inode: u32) -> Promise;

    #[wasm_bindgen(js_namespace = idbfs, js_name = writeFile)]
    fn write_file(id: String, inode: u32, contents: Vec<u8>) -> Promise;
}

async fn await_promise<T: DeserializeOwned>(promise: Promise) -> T {
    let future: JsFuture = promise.into();
    let result = future.await.unwrap();
    serde_wasm_bindgen::from_value(result).unwrap()
}

fn serialize_hash_map<S: Serializer, K, V>(
    value: &HashMap<K, V>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
    K: Serialize,
    V: Serialize,
{
    let mut sequence = serializer.serialize_seq(Some(value.len()))?;
    for entry in value {
        sequence.serialize_element(&entry)?;
    }
    sequence.end()
}

fn deserialize_hash_map<'de, D, K, V>(deserializer: D) -> Result<HashMap<K, V>, D::Error>
where
    D: Deserializer<'de>,
    K: Deserialize<'de> + Eq + Hash,
    V: Deserialize<'de>,
{
    let vec = Vec::deserialize(deserializer)?;
    Ok(vec.into_iter().collect())
}

const IDBFS_DEIVCE_MAJOR_NUMBER: u64 = 0x800;

thread_local! {
    static IDBFS_DEVICE_MINOR_NUMBER_COUNTER: Cell<u64> = Cell::new(1);
}

#[derive(Debug)]
struct IdbFsData {
    id: String,
    minor_number: u64,
}

#[derive(Debug)]
pub struct IdbFileSystem {
    fs: Rc<IdbFsData>,
}

#[derive(Debug)]
pub struct IdbFile {
    fs: Rc<IdbFsData>,
    inode: Inode,
}

#[derive(Debug)]
pub struct IdbDirectory {
    fs: Rc<IdbFsData>,
    inode: Inode,
}

#[derive(Debug, Serialize, Deserialize)]
enum NodeKind {
    Directory(
        #[serde(
            serialize_with = "serialize_hash_map",
            deserialize_with = "deserialize_hash_map"
        )]
        HashMap<Vec<u8>, Inode>,
    ),
    File,
}

#[derive(Debug, Serialize, Deserialize)]
struct Node {
    kind: NodeKind,
    permissions: Permissions,
    owner_id: UserId,
    group_id: GroupId,
}

#[derive(Debug)]
pub struct IdbFileDescriptor {
    fs: Rc<IdbFsData>,
    buffer: RefCell<Vec<u8>>,
    offset: Cell<usize>,
    mode: OpenMode,
    inode: Inode,
}

type AnyEntry = super::AnyEntry<IdbFile, IdbDirectory>;

impl IdbFileSystem {
    pub async fn new(id: String) -> Self {
        await_promise::<()>(mount(id.clone())).await;

        let node: Option<Node> = await_promise(get_inode(id.clone(), 1)).await;

        if node.is_none() {
            let root = Node {
                kind: NodeKind::Directory(HashMap::new()),
                group_id: GroupId(0),
                owner_id: UserId(0),
                permissions: Permissions {
                    owner: ClassPermissions::ALL,
                    group: ClassPermissions::READ_ONLY_EXECUTABLE,
                    others: ClassPermissions::READ_ONLY_EXECUTABLE,
                    is_sticky: false,
                    set_group_id: false,
                    set_user_id: false,
                },
            };
            let node = serde_wasm_bindgen::to_value(&root).unwrap();

            await_promise::<()>(update_inode(id.clone(), 1, node)).await;
        }

        let device_minor_number = IDBFS_DEVICE_MINOR_NUMBER_COUNTER.with(|counter| {
            let minor_number = counter.get();
            counter.set(minor_number + 1);
            minor_number
        });

        let common_data = IdbFsData {
            id,
            minor_number: device_minor_number,
        };

        Self {
            fs: Rc::new(common_data),
        }
    }
}

impl FileSystem for IdbFileSystem {
    type File = IdbFile;
    type Directory = IdbDirectory;

    type GetEntryFuture = LocalBoxFuture<'static, IoResult<AnyEntry>>;
    fn get_entry(&self, inode: Inode) -> Self::GetEntryFuture {
        let fs = Rc::clone(&self.fs);
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                Ok(match node.kind {
                    NodeKind::File => AnyEntry::File(IdbFile { fs, inode }),
                    NodeKind::Directory(_) => AnyEntry::Directory(IdbDirectory { fs, inode }),
                })
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type GetRootFuture = Ready<IoResult<Self::Directory>>;
    fn get_root(&self) -> Self::GetRootFuture {
        ready(Ok(IdbDirectory {
            fs: Rc::clone(&self.fs),
            inode: Inode(1),
        }))
    }

    type CreateFileFuture = LocalBoxFuture<'static, IoResult<Self::File>>;
    fn create_file(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateFileFuture {
        let fs = Rc::clone(&self.fs);
        Box::pin(async move {
            let node = Node {
                kind: NodeKind::File,
                permissions,
                owner_id,
                group_id,
            };
            let node = serde_wasm_bindgen::to_value(&node).unwrap();
            let inode: u32 = await_promise(create_inode(String::clone(&fs.id), node)).await;

            await_promise::<()>(write_file(String::clone(&fs.id), inode, Vec::new())).await;
            Ok(IdbFile {
                fs,
                inode: Inode(inode),
            })
        })
    }

    type CreateDirectoryFuture = LocalBoxFuture<'static, IoResult<Self::Directory>>;
    fn create_directory(
        &self,
        owner_id: UserId,
        group_id: GroupId,
        permissions: Permissions,
    ) -> Self::CreateDirectoryFuture {
        let fs = Rc::clone(&self.fs);
        Box::pin(async move {
            let node = Node {
                kind: NodeKind::Directory(HashMap::new()),
                permissions,
                owner_id,
                group_id,
            };
            let node = serde_wasm_bindgen::to_value(&node).unwrap();
            let inode: u32 = await_promise(create_inode(String::clone(&fs.id), node)).await;
            Ok(IdbDirectory {
                fs,
                inode: Inode(inode),
            })
        })
    }
}

impl Entry for IdbFile {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl Entry for IdbDirectory {
    fn inode(&self) -> Inode {
        self.inode
    }
}

impl FileDescriptor for IdbFileDescriptor {
    type ReadMetadataFuture = LocalBoxFuture<'static, IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let size = self.buffer.borrow().len();
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                match node.kind {
                    NodeKind::File => Ok(Metadata {
                        kind: EntryKind::File,
                        device: DeviceId(IDBFS_DEIVCE_MAJOR_NUMBER | fs.minor_number),
                        inode,
                        owner_id: node.owner_id,
                        group_id: node.group_id,
                        permissions: node.permissions,
                        size,
                    }),
                    NodeKind::Directory(_) => Err(IoErrorKind::IsDirectory),
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetOwnerIdFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.owner_id = owner;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetOwnerGroupIdFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.group_id = group;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetPermissionsFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.permissions = permissions;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type ReadFuture = Ready<IoResult<Vec<u8>>>;
    fn read(&self, count: usize) -> Self::ReadFuture {
        if let OpenMode::WriteOnly(_) = self.mode {
            return ready(Err(IoErrorKind::PermissionDenied));
        }

        let current_offset = self.offset.get();
        let buffer = self.buffer.borrow();
        let slice = match buffer.get(current_offset..) {
            Some(slice) => slice,
            None => return ready(Err(IoErrorKind::InvalidOffset)),
        };
        let read_end = slice.len().min(count);
        self.offset.set(current_offset + read_end);

        ready(Ok(slice[..read_end].to_vec()))
    }

    type WriteFuture = Ready<IoResult<usize>>;
    fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
        let write_options = match self.mode {
            OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => options,
            OpenMode::ReadOnly => return ready(Err(IoErrorKind::PermissionDenied)),
        };

        let mut inner_buffer = self.buffer.borrow_mut();
        let contents_len = inner_buffer.len();
        let offset = if write_options.should_append {
            contents_len
        } else {
            self.offset.get()
        };
        if offset > inner_buffer.len() {
            return ready(Err(IoErrorKind::InvalidOffset));
        }
        inner_buffer.resize((offset + buffer.len()).max(contents_len), 0);
        inner_buffer[offset..offset + buffer.len()].copy_from_slice(buffer);
        self.offset.set(offset + buffer.len());

        ready(Ok(buffer.len()))
    }

    type FlushFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn flush(&self) -> Self::FlushFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let buffer = self.buffer.borrow().to_owned();
        Box::pin(async move {
            await_promise::<()>(write_file(String::clone(&fs.id), inode.0, buffer)).await;

            Ok(())
        })
    }

    type SeekFuture = Ready<IoResult<usize>>;
    fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture {
        match seek_from {
            SeekFrom::Start(offset) => self.offset.set(offset),
            SeekFrom::End(offset) => {
                let end = self.buffer.borrow().len();
                let new_offset = match usize_plus_isize(end, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
            SeekFrom::Current(offset) => {
                let current = self.offset.get();
                let new_offset = match usize_plus_isize(current, offset) {
                    Some(new_offset) => new_offset,
                    None => return ready(Err(IoErrorKind::InvalidOffset)),
                };
                self.offset.set(new_offset);
            }
        }

        ready(Ok(self.offset.get()))
    }
}

impl Drop for IdbFileDescriptor {
    fn drop(&mut self) {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let buffer = self.buffer.borrow().to_owned();
        wasm_bindgen_futures::spawn_local(async move {
            await_promise::<()>(write_file(String::clone(&fs.id), inode.0, buffer)).await;
        });
    }
}

impl File for IdbFile {
    type FileDescriptor = IdbFileDescriptor;
    type OpenFuture = LocalBoxFuture<'static, IoResult<Self::FileDescriptor>>;
    fn open(&self, mode: OpenMode) -> Self::OpenFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let (should_truncate, should_append) = match mode {
                OpenMode::ReadWrite(options) | OpenMode::WriteOnly(options) => {
                    (options.should_truncate, options.should_append)
                }
                _ => (false, false),
            };

            let buffer = if should_truncate {
                Vec::new()
            } else {
                await_promise(read_file(String::clone(&fs.id), inode.0)).await
            };

            let offset = if should_append { buffer.len() } else { 0 };

            Ok(IdbFileDescriptor {
                buffer: RefCell::new(buffer),
                fs,
                inode,
                offset: Cell::new(offset),
                mode,
            })
        })
    }
}

impl Directory for IdbDirectory {
    type ReadMetadataFuture = LocalBoxFuture<'static, IoResult<Metadata>>;
    fn read_metadata(&self) -> Self::ReadMetadataFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                match node.kind {
                    NodeKind::File => Err(IoErrorKind::NotDirectory),
                    NodeKind::Directory(contents) => Ok(Metadata {
                        kind: EntryKind::Directory,
                        device: DeviceId(IDBFS_DEIVCE_MAJOR_NUMBER | fs.minor_number),
                        inode,
                        owner_id: node.owner_id,
                        group_id: node.group_id,
                        permissions: node.permissions,
                        size: contents.len(),
                    }),
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetOwnerIdFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.owner_id = owner;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetOwnerGroupIdFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.group_id = group;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type SetPermissionsFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                node.permissions = permissions;
                let node = serde_wasm_bindgen::to_value(&node).unwrap();
                await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;

                Ok(())
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type ReadFuture = LocalBoxFuture<'static, IoResult<HashMap<UnixString, Inode>>>;
    fn read(&self) -> Self::ReadFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                if let NodeKind::Directory(contents) = node.kind {
                    Ok(contents
                        .into_iter()
                        .map(|(name, inode)| (UnixString::from_vec(name), inode))
                        .collect())
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type GetEntryFuture = LocalBoxFuture<'static, IoResult<Inode>>;
    fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let name = name.to_owned();
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                if let NodeKind::Directory(contents) = node.kind {
                    contents
                        .get(name.as_bytes())
                        .copied()
                        .ok_or(IoErrorKind::NotFound)
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type HasEntryFuture = LocalBoxFuture<'static, IoResult<bool>>;
    fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let name = name.to_owned();
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                if let NodeKind::Directory(contents) = node.kind {
                    Ok(contents.contains_key(name.as_bytes()))
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type PutEntryFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn put_entry(&self, name: &UnixStr, put_inode: Inode) -> Self::PutEntryFuture {
        let fs = Rc::clone(&self.fs);
        let this_inode = self.inode;
        let name = name.to_owned();
        Box::pin(async move {
            let node: Option<Node> =
                await_promise(get_inode(String::clone(&fs.id), this_inode.0)).await;

            if let Some(mut node) = node {
                if let NodeKind::Directory(contents) = &mut node.kind {
                    if contents.contains_key(name.as_bytes()) {
                        return Err(IoErrorKind::AlreadyExists);
                    }

                    contents.insert(name.into_vec(), put_inode);
                    let node = serde_wasm_bindgen::to_value(&node).unwrap();
                    await_promise::<()>(update_inode(String::clone(&fs.id), this_inode.0, node))
                        .await;
                    Ok(())
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type RemoveEntryFuture = LocalBoxFuture<'static, IoResult<()>>;
    fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        let name = name.to_owned();
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(mut node) = node {
                if let NodeKind::Directory(contents) = &mut node.kind {
                    contents.remove(name.as_bytes());
                    let node = serde_wasm_bindgen::to_value(&node).unwrap();
                    await_promise::<()>(update_inode(String::clone(&fs.id), inode.0, node)).await;
                    Ok(())
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }

    type IsEmptyFuture = LocalBoxFuture<'static, IoResult<bool>>;
    fn is_empty(&self) -> Self::IsEmptyFuture {
        let fs = Rc::clone(&self.fs);
        let inode = self.inode;
        Box::pin(async move {
            let node: Option<Node> = await_promise(get_inode(String::clone(&fs.id), inode.0)).await;

            if let Some(node) = node {
                if let NodeKind::Directory(contents) = node.kind {
                    Ok(contents.is_empty())
                } else {
                    Err(IoErrorKind::NotDirectory)
                }
            } else {
                Err(IoErrorKind::NotFound)
            }
        })
    }
}
