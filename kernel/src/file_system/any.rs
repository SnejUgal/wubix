use super::{
    dev::{DevDirectory, DevFile, DevFileSystem, Device},
    idb::{IdbDirectory, IdbFile, IdbFileDescriptor, IdbFileSystem},
    pipe::Pipe,
    temporary::{TemporaryDescriptor, TemporaryDirectory, TemporaryFile, TemporaryFileSystem},
    AnyEntry, Directory, Entry, File, FileDescriptor, FileSystem, Metadata, Permissions,
};
use futures::future::LocalBoxFuture;
use std::{collections::HashMap, rc::Rc};
use types::{
    ids::{GroupId, Inode, UserId},
    io::IoResult,
    system_calls::{open::OpenMode, seek::SeekFrom},
};
use unix_str::{UnixStr, UnixString};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct AnyForWasm {
    pub(crate) inner: Any,
}

#[derive(Debug, Clone)]
pub enum Any {
    Temporary(Rc<TemporaryFileSystem>),
    Dev(Rc<DevFileSystem>),
    Idb(Rc<IdbFileSystem>),
}

#[derive(Debug, Clone)]
pub enum AnyFile {
    Temporary(Rc<TemporaryFile>),
    Dev(Rc<DevFile>),
    Idb(Rc<IdbFile>),
}

#[derive(Debug, Clone)]
pub enum AnyDirectory {
    Temporary(Rc<TemporaryDirectory>),
    Dev(Rc<DevDirectory>),
    Idb(Rc<IdbDirectory>),
}

#[derive(Debug, Clone)]
pub enum AnyDescriptor {
    Temporary(Rc<TemporaryDescriptor>),
    Dev(Rc<Device>),
    Idb(Rc<IdbFileDescriptor>),
    Pipe(Rc<Pipe>),
}

impl Any {
    pub fn is_eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Temporary(this), Self::Temporary(other)) => Rc::ptr_eq(this, other),
            (Self::Temporary(_), _) => false,
            (Self::Dev(this), Self::Dev(other)) => Rc::ptr_eq(this, other),
            (Self::Dev(_), _) => false,
            (Self::Idb(this), Self::Idb(other)) => Rc::ptr_eq(this, other),
            (Self::Idb(_), _) => false,
        }
    }
}

impl From<TemporaryFileSystem> for Any {
    fn from(tmpfs: TemporaryFileSystem) -> Self {
        Self::Temporary(Rc::new(tmpfs))
    }
}

impl From<DevFileSystem> for Any {
    fn from(devfs: DevFileSystem) -> Self {
        Self::Dev(Rc::new(devfs))
    }
}

impl From<IdbFileSystem> for Any {
    fn from(idbfs: IdbFileSystem) -> Self {
        Self::Idb(Rc::new(idbfs))
    }
}

macro_rules! impl_file_system {
    ($($var:ident),*) => {
        impl FileSystem for Any {
            type File = AnyFile;
            type Directory = AnyDirectory;

            type GetEntryFuture = LocalBoxFuture<'static, IoResult<AnyEntry<Self::File, Self::Directory>>>;
            fn get_entry(&self, inode: Inode) -> Self::GetEntryFuture {
                let this = self.clone();
                Box::pin(async move {
                    Ok(match this {
                        $(
                            Self::$var(fs) => match fs.get_entry(inode).await? {
                                AnyEntry::File(file) =>
                                    AnyEntry::File(AnyFile::$var(Rc::new(file))),
                                AnyEntry::Directory(directory) =>
                                    AnyEntry::Directory(AnyDirectory::$var(Rc::new(directory))),
                            }
                        ),*
                    })
                })
            }

            type GetRootFuture = LocalBoxFuture<'static, IoResult<Self::Directory>>;
            fn get_root(&self) -> Self::GetRootFuture {
                let this = self.clone();
                Box::pin(async move {
                    Ok(match this {
                        $(
                            Self::$var(fs) => AnyDirectory::$var(Rc::new(fs.get_root().await?))
                        ),*
                    })
                })
            }

            type CreateFileFuture = LocalBoxFuture<'static, IoResult<Self::File>>;
            fn create_file(
                &self,
                owner_id: UserId,
                group_id: GroupId,
                permissions: Permissions,
            ) -> Self::CreateFileFuture {
                let this = self.clone();
                Box::pin(async move {
                    Ok(match this {
                        $(
                            Self::$var(fs) => AnyFile::$var(Rc::new(fs.create_file(owner_id, group_id, permissions).await?))
                        ),*
                    })
                })
            }

            type CreateDirectoryFuture = LocalBoxFuture<'static, IoResult<Self::Directory>>;
            fn create_directory(
                &self,
                owner_id: UserId,
                group_id: GroupId,
                permissions: Permissions,
            ) -> Self::CreateDirectoryFuture {
                let this = self.clone();
                Box::pin(async move {
                    Ok(match this {
                        $(
                            Self::$var(fs) => AnyDirectory::$var(Rc::new(FileSystem::create_directory(&*fs, owner_id, group_id, permissions).await?))
                        ),*
                    })
                })
            }
        }
    };
}

macro_rules! impl_entry {
    ($enum:ident, $($var:ident),*) => {
        impl Entry for $enum {
            fn inode(&self) -> Inode {
                match self {
                    $(
                        Self::$var(entry) => entry.inode()
                    ),*
                }
            }
        }
    };
}

macro_rules! impl_file {
    ($($var:ident),*) => {
        impl File for AnyFile {
            type FileDescriptor = AnyDescriptor;
            type OpenFuture = LocalBoxFuture<'static, IoResult<Self::FileDescriptor>>;
            fn open(&self, mode: OpenMode) -> Self::OpenFuture {
                let this = self.clone();
                Box::pin(async move {
                    Ok(match this {
                        $(
                            Self::$var(file) => AnyDescriptor::$var(Rc::new(file.open(mode).await?))
                        ),*
                    })
                })
            }
        }
    };
}

macro_rules! impl_file_descriptor {
    ($($var:ident),*) => {
        impl FileDescriptor for AnyDescriptor {
            type ReadFuture = LocalBoxFuture<'static, IoResult<Vec<u8>>>;
            fn read(&self, count: usize) -> Self::ReadFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.read(count))
                    ),*
                }
            }

            type WriteFuture = LocalBoxFuture<'static, IoResult<usize>>;
            fn write(&self, buffer: &[u8]) -> Self::WriteFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.write(buffer))
                    ),*
                }
            }

            type ReadMetadataFuture = LocalBoxFuture<'static, IoResult<Metadata>>;
            fn read_metadata(&self) -> Self::ReadMetadataFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.read_metadata())
                    ),*
                }
            }

            type SetOwnerIdFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.set_owner_id(owner))
                    ),*
                }
            }

            type SetOwnerGroupIdFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.set_owner_group_id(group))
                    ),*
                }
            }

            type SetPermissionsFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.set_permissions(permissions))
                    ),*
                }
            }

            type FlushFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn flush(&self) -> Self::FlushFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.flush())
                    ),*
                }
            }

            type SeekFuture = LocalBoxFuture<'static, IoResult<usize>>;
            fn seek(&self, seek_from: SeekFrom) -> Self::SeekFuture {
                match self {
                    $(
                        Self::$var(descriptor) => Box::pin(descriptor.seek(seek_from))
                    ),*
                }
            }
        }
    };
}

macro_rules! impl_directory {
    ($($var:ident),*) => {
        impl Directory for AnyDirectory {
            type ReadFuture = LocalBoxFuture<'static, IoResult<HashMap<UnixString, Inode>>>;
            fn read(&self) -> Self::ReadFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.read())
                    ),*
                }
            }

            type GetEntryFuture = LocalBoxFuture<'static, IoResult<Inode>>;
            fn get_entry(&self, name: &UnixStr) -> Self::GetEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.get_entry(name))
                    ),*
                }
            }

            type HasEntryFuture = LocalBoxFuture<'static, IoResult<bool>>;
            fn has_entry(&self, name: &UnixStr) -> Self::HasEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.has_entry(name))
                    ),*
                }
            }

            type PutEntryFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn put_entry(&self, name: &UnixStr, inode: Inode) -> Self::PutEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.put_entry(name, inode))
                    ),*
                }
            }

            type RemoveEntryFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn remove_entry(&self, name: &UnixStr) -> Self::RemoveEntryFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.remove_entry(name))
                    ),*
                }
            }

            type ReadMetadataFuture = LocalBoxFuture<'static, IoResult<Metadata>>;
            fn read_metadata(&self) -> Self::ReadMetadataFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.read_metadata())
                    ),*
                }
            }

            type SetOwnerIdFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_owner_id(&self, owner: UserId) -> Self::SetOwnerIdFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.set_owner_id(owner))
                    ),*
                }
            }

            type SetOwnerGroupIdFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_owner_group_id(&self, group: GroupId) -> Self::SetOwnerGroupIdFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.set_owner_group_id(group))
                    ),*
                }
            }

            type SetPermissionsFuture = LocalBoxFuture<'static, IoResult<()>>;
            fn set_permissions(&self, permissions: Permissions) -> Self::SetPermissionsFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.set_permissions(permissions))
                    ),*
                }
            }

            type IsEmptyFuture = LocalBoxFuture<'static, IoResult<bool>>;
            fn is_empty(&self) -> Self::IsEmptyFuture {
                match self {
                    $(
                        Self::$var(directory) => Box::pin(directory.is_empty())
                    ),*
                }
            }
        }
    };
}

impl_file_system!(Temporary, Dev, Idb);
impl_entry!(AnyFile, Temporary, Dev, Idb);
impl_entry!(AnyDirectory, Temporary, Dev, Idb);
impl_file!(Temporary, Dev, Idb);
impl_file_descriptor!(Temporary, Dev, Pipe, Idb);
impl_directory!(Temporary, Dev, Idb);
