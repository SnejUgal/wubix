macro_rules! printk {
    ($kernel:expr; $($argument:tt)*) => {{
        let active_terminal = $kernel.active_virtual_terminal.get();
        let terminals = $kernel.virtual_terminals.borrow();
        let tty = &mut *terminals[active_terminal].borrow_mut();
        printk!(tty, $kernel.frame_buffer; $($argument)*);
    }};
    ($tty:expr, $frame_buffer:expr; $($argument:tt)*) => {{
        use std::fmt::Write;

        write!($tty, "[{:>12.6}] ", crate::kernel::now() / 1000.0).unwrap();
        writeln!($tty, $($argument)*).unwrap();
        $tty.render($frame_buffer.borrow_mut().lock());
    }};
}

pub mod file_system;
mod frame_buffer;
mod initramfs;
mod kernel;
mod notify;
mod util;
mod virtual_terminal;
