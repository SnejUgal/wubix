use std::{
    cell::RefCell,
    collections::VecDeque,
    future::Future,
    pin::Pin,
    rc::Rc,
    task::{Context, Poll, Waker},
};

#[derive(Debug, Default)]
pub struct Notify {
    waiters: RefCell<VecDeque<Rc<RefCell<WaiterState>>>>,
}

#[derive(Debug)]
enum WaiterState {
    Preparing,
    Waiting(Waker),
    Done,
}

#[derive(Debug)]
pub struct Waiter(Rc<RefCell<WaiterState>>);

impl Notify {
    pub fn wait(&self) -> Waiter {
        let state = Rc::new(RefCell::new(WaiterState::Preparing));
        self.waiters.borrow_mut().push_back(Rc::clone(&state));
        Waiter(state)
    }

    pub fn notify(&self) {
        let mut waiters = self.waiters.borrow_mut();
        if let Some(waiter) = waiters.front() {
            let waker = if let WaiterState::Waiting(waker) = &*waiter.borrow() {
                waker.clone()
            } else {
                return;
            };
            *waiter.borrow_mut() = WaiterState::Done;
            waiters.pop_front();
            waker.wake()
        }
    }

    pub fn notify_all(&self) {
        let mut waiters = self.waiters.borrow_mut();
        for waiter in waiters.drain(..) {
            let waker = if let WaiterState::Waiting(waker) = &*waiter.borrow() {
                waker.clone()
            } else {
                continue;
            };
            *waiter.borrow_mut() = WaiterState::Done;
            waker.wake()
        }
    }
}

impl Future for Waiter {
    type Output = ();

    fn poll(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Self::Output> {
        let mut state = self.0.borrow_mut();
        match &*state {
            WaiterState::Preparing => {
                *state = WaiterState::Waiting(context.waker().clone());
                Poll::Pending
            }
            WaiterState::Waiting(_) => Poll::Pending,
            WaiterState::Done => Poll::Ready(()),
        }
    }
}
