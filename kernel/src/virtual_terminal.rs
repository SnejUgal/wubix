use crate::{
    frame_buffer::{FrameBufferLock, Pixel},
    notify::Notify,
};
use std::{
    cell::RefCell,
    collections::VecDeque,
    fmt::{self, Write},
    iter,
    rc::Rc,
};
use types::ids::UserId;

mod colors;
mod fonts;
mod graphics_rendition;

use graphics_rendition::GraphicsRendition;

const CHAR_WIDTH: usize = 8;
const CHAR_HEIGHT: usize = 16;

pub type VirtualTerminalRef = Rc<RefCell<VirtualTerminal>>;
pub type VirtualTerminals = Rc<RefCell<Vec<VirtualTerminalRef>>>;

#[derive(Debug)]
pub struct VirtualTerminal {
    lines: Vec<String>,
    fonts: fonts::Set,
    initial_graphics_rendition: GraphicsRendition,
    scroll_offset: usize,
    cursor: Cursor,
    size: (usize, usize),
    dpi: usize,
    is_blink_visible: bool,
    pub owner: UserId,
    pub input: VecDeque<String>,
    pub input_waiters: Notify,
}

#[derive(Debug)]
struct Cursor {
    line: usize,
    column: usize,
}

impl VirtualTerminal {
    pub fn new((width, height): (usize, usize), dpi: usize) -> Self {
        Self {
            lines: vec![String::new()],
            fonts: fonts::load_set(),
            initial_graphics_rendition: GraphicsRendition::default(),
            scroll_offset: 0,
            cursor: Cursor { line: 0, column: 0 },
            size: (width / (CHAR_WIDTH * dpi), height / (CHAR_HEIGHT * dpi)),
            dpi,
            is_blink_visible: true,
            owner: UserId(0),
            input: vec![String::new()].into(),
            input_waiters: Notify::default(),
        }
    }

    pub fn render(&self, mut frame_buffer: FrameBufferLock<'_>) {
        let mut graphics_rendition = self.initial_graphics_rendition;
        let (columns_max, lines_max) = self.size;

        fill_rectangle(
            &mut frame_buffer,
            (0, 0),
            (columns_max * CHAR_WIDTH, lines_max * CHAR_HEIGHT),
            self.dpi,
            graphics_rendition.get_background(),
        );

        let lines = real_lines(&self.lines, columns_max)
            .skip(self.scroll_offset)
            .take(lines_max);

        for (line_index, line) in lines.enumerate() {
            let line_y = line_index * CHAR_HEIGHT;
            let mut column_index = 0;
            let mut columns_rendered = 0;

            let mut iterator = line.chars().enumerate();
            while let Some((index, character)) = iterator.next() {
                let character_x = column_index * CHAR_WIDTH;
                match character {
                    '\r' => {
                        column_index = 0;
                        continue;
                    }
                    '\x08' => {
                        column_index = column_index.saturating_sub(1);
                        continue;
                    }
                    '\t' => {
                        let tab_width = (8 - column_index % 8).min(columns_max - column_index);
                        column_index += tab_width;
                        columns_rendered = columns_rendered.max(column_index);
                        continue;
                    }
                    '\x1B' => {
                        let escape_code_length =
                            graphics_rendition.apply_escape_code(line.chars().skip(index));
                        let _ = (&mut iterator)
                            .take(escape_code_length.saturating_sub(1))
                            .last();
                        continue;
                    }
                    '\0'..='\x19' => continue,
                    ' '..='~' => (),
                    _ => continue,
                };

                let font = if graphics_rendition.bold {
                    &self.fonts.bold
                } else {
                    &self.fonts.default
                };
                let mut unicode = [0; 2];
                let bitmap = font.get_glyph(character.encode_utf16(&mut unicode));

                for (bitmap_line_index, bitmap_line) in bitmap.iter().enumerate() {
                    let y = line_y + bitmap_line_index;

                    for pixel_index in 0..CHAR_WIDTH {
                        let color = if bitmap_line & (1 << (7 - pixel_index)) != 0 {
                            graphics_rendition.get_foreground(self.is_blink_visible)
                        } else {
                            graphics_rendition.get_background()
                        };

                        let x = character_x + pixel_index;
                        fill_rectangle(&mut frame_buffer, (x, y), (1, 1), self.dpi, color);
                    }
                }

                if graphics_rendition.underscore {
                    fill_rectangle(
                        &mut frame_buffer,
                        (character_x, line_y + CHAR_HEIGHT - 4),
                        (CHAR_WIDTH, 1),
                        self.dpi,
                        graphics_rendition.get_foreground(self.is_blink_visible),
                    );
                }

                if graphics_rendition.underline {
                    fill_rectangle(
                        &mut frame_buffer,
                        (character_x, line_y + CHAR_HEIGHT - 2),
                        (CHAR_WIDTH, 1),
                        self.dpi,
                        graphics_rendition.get_foreground(self.is_blink_visible),
                    );
                }

                if graphics_rendition.crossed {
                    fill_rectangle(
                        &mut frame_buffer,
                        (character_x, line_y + CHAR_HEIGHT / 2 - 1),
                        (CHAR_WIDTH, 1),
                        self.dpi,
                        graphics_rendition.get_foreground(self.is_blink_visible),
                    );
                }

                column_index += 1;
                columns_rendered = columns_rendered.max(column_index);
            }
        }

        if self.is_blink_visible {
            let line = self.cursor.line.saturating_sub(self.scroll_offset);

            if line < self.size.1 {
                fill_rectangle(
                    &mut frame_buffer,
                    (
                        self.cursor.column * CHAR_WIDTH,
                        line * CHAR_HEIGHT + CHAR_HEIGHT - 1,
                    ),
                    (CHAR_WIDTH, 1),
                    self.dpi,
                    graphics_rendition.get_foreground(self.is_blink_visible),
                )
            }
        }
    }

    pub fn process_key(&mut self, input: &str) {
        match input {
            "\n" | "\r" => {
                self.input.back_mut().unwrap().push('\n');
                self.input.push_back(String::new());
                self.write_char('\n').unwrap();
                self.input_waiters.notify();
            }
            "\x04" => {
                self.input.push_back(String::new());
                self.input_waiters.notify();
            }
            "\x08" => {
                let removed = self.input.back_mut().unwrap().pop();
                let last_line = self.lines.last_mut().unwrap();
                match removed {
                    Some('\x01'..='\x1f') => {
                        // a control character is displayed with two characters. So if we removed
                        // a control character, we need to remove two characters from the output
                        last_line.pop();
                        last_line.pop();
                    }
                    Some(_) => {
                        last_line.pop();
                    }
                    None => (),
                }
                self.position_cursor();
            }
            _ => {
                self.input.back_mut().unwrap().push_str(input);
                let mut echoed_input = input.to_owned();
                for escaped in 1..=0x1f {
                    let visible = (escaped - 1 + b'A') as char;
                    echoed_input = echoed_input.replace(escaped as char, &format!("^{}", visible));
                }
                self.write_str(&echoed_input).unwrap();
            }
        }
    }

    pub fn blink(&mut self, frame_buffer: FrameBufferLock<'_>) {
        self.is_blink_visible = !self.is_blink_visible;
        self.render(frame_buffer);
    }

    pub fn scroll_page_up(&mut self, frame_buffer: FrameBufferLock<'_>) {
        self.scroll_offset = self.scroll_offset.saturating_sub(self.size.1);
        self.render(frame_buffer);
    }

    pub fn scroll_page_down(&mut self, frame_buffer: FrameBufferLock<'_>) {
        let max_scroll = real_lines(&self.lines, self.size.0)
            .count()
            .saturating_sub(self.size.1);
        self.scroll_offset = (self.scroll_offset + self.size.1).min(max_scroll);
        self.render(frame_buffer);
    }

    pub fn position_cursor(&mut self) {
        let (last_real_line_index, last_real_line) = real_lines(&self.lines, self.size.0)
            .enumerate()
            .last()
            .unwrap();

        self.cursor = Cursor {
            column: calculate_last_column(last_real_line),
            line: last_real_line_index,
        };

        if self.cursor.column == self.size.0 {
            self.cursor.column = 0;
            self.cursor.line += 1;
        }

        if last_real_line_index + 1 > self.size.1 {
            self.scroll_offset = last_real_line_index + 1 - self.size.1;
        }
    }
}

impl Write for VirtualTerminal {
    fn write_str(&mut self, text: &str) -> fmt::Result {
        let mut lines = text.split('\n');
        let first_line = lines.next().unwrap();
        self.lines.last_mut().unwrap().push_str(first_line);
        self.lines.extend(lines.map(ToOwned::to_owned));

        self.position_cursor();

        Ok(())
    }
}

fn real_lines(lines: &[String], columns: usize) -> impl Iterator<Item = &str> {
    lines.iter().flat_map(move |line| {
        let mut characters = line.char_indices();
        let mut column_index: usize = 0;
        let mut columns_rendered = 0;
        let mut slice_start = 0;
        let mut has_ended = false;

        iter::from_fn(move || {
            while let Some((index, character)) = characters.next() {
                match character {
                    '\r' => {
                        column_index = 0;
                        continue;
                    }
                    '\x08' => {
                        column_index = column_index.saturating_sub(1);
                        continue;
                    }
                    '\t' => {
                        let tab_width = 8 - column_index % 8;
                        column_index += tab_width;
                    }
                    '\x1B' => {
                        let escape_code_length =
                            graphics_rendition::get_escape_code_length(line.chars().skip(index));
                        let _ = (&mut characters)
                            .take(escape_code_length.saturating_sub(1))
                            .last();
                        continue;
                    }
                    '\0'..='\x19' => continue,
                    ' '..='~' => column_index += 1,
                    _ => continue,
                };

                columns_rendered = columns_rendered.max(column_index);
                if columns_rendered >= columns {
                    column_index = 0;
                    columns_rendered = 0;
                    let slice_start = std::mem::replace(&mut slice_start, index + 1);
                    return Some(&line[slice_start..=index]);
                }
            }

            if has_ended {
                None
            } else {
                has_ended = true;
                let slice_start = std::mem::replace(&mut slice_start, line.len());
                Some(&line[slice_start..])
            }
        })
    })
}

fn fill_rectangle(
    frame_buffer: &mut FrameBufferLock<'_>,
    (x, y): (usize, usize),
    (width, height): (usize, usize),
    dpi: usize,
    color: Pixel,
) {
    for y in y * dpi..(y + height) * dpi {
        for x in x * dpi..(x + width) * dpi {
            frame_buffer[(x, y)] = color;
        }
    }
}

fn calculate_last_column(line: &str) -> usize {
    let mut column_index: usize = 0;

    let mut characters = line.chars().enumerate();
    while let Some((index, character)) = characters.next() {
        match character {
            '\r' => {
                column_index = 0;
                continue;
            }
            '\x08' => {
                column_index = column_index.saturating_sub(1);
                continue;
            }
            '\t' => {
                let tab_width = 8 - column_index % 8;
                column_index += tab_width;
            }
            '\x1B' => {
                let escape_code_length =
                    graphics_rendition::get_escape_code_length(line.chars().skip(index));
                let _ = (&mut characters)
                    .take(escape_code_length.saturating_sub(1))
                    .last();
                continue;
            }
            '\0'..='\x19' => continue,
            ' '..='~' => column_index += 1,
            _ => continue,
        };
    }

    column_index
}
