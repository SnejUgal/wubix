#!/usr/bin/env node
const { Volume, createFsFromVolume } = require(`memfs`);
const { promises: fs } = require(`fs`);
const webpack = require(`webpack`);

const main = async () => {
  const package = JSON.parse(await fs.readFile(`pkg/package.json`, `utf8`));
  console.log(`pkg2wef: running webpack...`);

  await fs.writeFile(
    `pkg/index.js`,
    `window.executable = import("./kernel").then(async (kernel) => {
      const { memory } = await import("./kernel_bg");
      return { ...kernel, memory };
     });`
  );

  const outputFileSystem = Object.create(createFsFromVolume(new Volume()));
  outputFileSystem.join = require(`memory-fs/lib/join`);

  const compiler = webpack({
    mode: `development`,
    entry: `./pkg/index.js`,
    output: {
      filename: `${package.name}.js`,
      path: `/`,
    },
    target: `web`,
  });
  compiler.outputFileSystem = outputFileSystem;

  const [error, stats] = await new Promise((resolve) => {
    compiler.run((error, stats) => resolve([error, stats]));
  });
  await fs.unlink(`pkg/index.js`);

  if (error) {
    throw error;
  }

  if (
    (stats.errors && stats.errors.length > 0) ||
    (stats.warnings && stats.warnings.length > 0)
  ) {
    stats.errors && stats.errors.forEach((error) => console.error(error));
    stats.warnings && stats.warnings.forEach((error) => console.error(error));
    process.exit(1);
  }

  console.log(`pkg2wef: compiling kernel.wef...`);

  let mainChunk = outputFileSystem.readFileSync(`/kernel.js`, `utf8`);

  mainChunk = mainChunk.replace(
    `__webpack_require__.p + "" + chunkId + ".kernel.js"`,
    `window.getFileBlob(chunkId + ".kernel.js")`
  );
  mainChunk = mainChunk.replace(
    /var req \= fetch\(__webpack_require__\.p \+ \"\" \+ (\{\"\.\/pkg\/(?:[\w\d]+)_bg\.wasm\"\:\"(?:[\w\d]+)\"\}\[wasmModuleId\] \+ \"\.module\.wasm\")\);/,
    `var req = fetch(window.getFileBlob($1));`
  );
  mainChunk = Buffer.from(mainChunk);

  const metadata = {
    entry: `kernel.js`,
    files: [[`kernel.js`, mainChunk.length]],
  };
  const finalContents = [...mainChunk];

  outputFileSystem.readdirSync(`/`).forEach((fileName) => {
    if (fileName == `kernel.js`) {
      return;
    }
    const contents = outputFileSystem.readFileSync(`/${fileName}`);
    metadata.files.push([fileName, contents.length]);
    for (const byte of contents) {
      finalContents.push(byte);
    }
  });

  finalContents.unshift(...Buffer.from(JSON.stringify(metadata)), 0);
  await fs.writeFile(`kernel.wef`, Buffer.from(finalContents));

  console.log(`pkg2wef: done`);
};

main();
