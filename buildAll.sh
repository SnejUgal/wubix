# Build the kernel...
cd kernel
npm install
npm run build

# ...and the browser part
cd ../browser
npm install
npm run build
