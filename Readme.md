# [Wubix][try-wubix]

![Wubix running in browser](./wubixScreenshot.png)

[Wubix][try-wubix] is a Unix-like OSish running in the browser, written in Rust using WebAssembly —
try it [here][try-wubix]. It uses [`coreutils`] and currently [`tinysh`] as its shell. And yes,
you can write programs for Wubix! More on that below.

[`coreutils`]: https://gitlab.com/wubix-crates/coreutils
[`tinysh`]: https://gitlab.com/wubix-crates/tinysh
[try-wubix]: https://wubix.snejugal.ru

## How does it work?

Wubix pretty much resembles a real operating system. First, when you open Wubix in your browser,
the initial JS code serves as a bootloader: it (down)loads the kernel, loads initramfs (or
generates a very basic one on the first boot), and then actually starts the kernel. The kernel
initializes devices, the virtual file system, and then starts `systemw`, Wubix's init system.

Wubix has its own executable format, WEF, for programs. Think of it as a tarball of WASM and JS
needed to start the program. Each process is started in its own worker, as it provides more security
and robustness (if a program is stuck in an inifite loop, you can press Ctrl + C and the kernel
will terminate its worker). When a program wants to make a system call, the worker posts a message
to the main thread, which will respond with the result of the call.

From this stems the asynchronous nature of Wubix. You can't block in WebAssembly like you can
on real hardware, so Wubix uses Promises and Futures extensively. Wubix's standard library,
`librust`, is an asynchronous mirror of Rust's `std` (much like `async-std`, except that we can't
wrap `std` and have to reimplement the standard library). `std` doesn't cover all Unix system calls,
so `librust` provides the `sys` module with thin wrappers around all system calls.

## Features and limitations

- Wubix can't do CPU scheduling, so we can't suspend and resume processes, block processes, and
  there can never be a true `fork` system call. Instead, Wubix has an `execute` system call that
  spawns another process directly.
- Wubix has a completely different ABI, so Wubix's system calls have properly spelled names and
  they're type-safe.
- Wubix can't have TCP and UDP stacks, at least real and not emulated ones. If you want to make
  an HTTP request, use browser APIs, or `reqwest` that's compatible with WASM. Unix sockets should
  be possible, but they're not implemented yet.
- Wubix doesn't have soft links because they're terrible to deal with. That said, we won't mind
  a merge request that implements them properly, but don't expect us to implement them.
- Permissions are there, but some permission checks may be missing because permissions are boring.
  If you find a missing check, file an issue and we'll fix it, or you may want to create a merge
  request instead.
- Simply cross-compiling existing programs for Wubix is hard; some effort to adapt it for Wubix will
  be needed. For Rust, we chose the easier way since it has `async/await` supports built into the
  language: `librust` resembles `std` as much as possible, but with `async/await` added where
  required. This is how we port [`coreutils`] to Wubix utility-by-utility; see also the below
  section on how to write programs for Wubix in Rust. Speaking of C, it's possible to write Wubix
  programs in C using emscripten's Asyncify feature, and we even have a [proof of concept][wlibc],
  but oh god is it terrible, please just use Rust instead of C.
- No known attempts at self-hosting have been made so far.

[wlibc]: https://gitlab.com/wubix-crates/wlibc

## Writing and compiling programs for Wubix

First off, you'll need Rust, Cargo, wasm-pack, Node.js and npm. If you don't have Rust or Node.js,
install them yourself as that's very much OS-specific. npm usually comes with Node.js, and Cargo
comes with Rust. To install `wasm-pack`, run `cargo install wasm-pack`. Here we will describe
writing a Wubix program from scratch, but it's easy to infer how to adapt an existing program
for Wubix.

Now we need to create a new project. First, run `wasm-pack new $PROJECT_NAME`, which will create
a new directory with the given name. Then, in the newly created project, run `npm init -y` and
`npm install gitlab:wubix-crates/pkg2wef`. The latter command installs `pkg2wef`, a script that
compiles `wasm-pack`'s build artefacts into a WEF. As a final preparation step, add these
dependencies in your Cargo.toml (the default dependencies are not needed):

```toml
[dependencies]
librust = { git = "https://gitlab.com/SnejUgal/wubix.git" }
wasm-bindgen = "0.2"
wasm-bindgen-futures = "0.4"
```

Let's compile a hello world program. Here's what it looks like:

```rust
use wasm_bindgen::prelude::*;
use librust::println;

#[wasm_bindgen]
pub async fn start() {
  println!("Hello world!").await;
}
```

Notice that we have `pub async fn start` instead of `fn main` and we also `.await` the `println!`.
Basically, any operation, especially I/O, in `librust` is asynchronous, except `process::exit`,
which is artifically blocking, and operations that don't need to call the kernel (e.g. operations
with process arguments and environment variables are done completely in the userspace).

Now let's compile it! First run `wasm-pack build`, then `npx pkg2wef` (npx comes with npm)
(if you want, you may add a script for this in your `package.json`). The first command will generate
a `pkg` directory in your project, and the latter one will generate `$PROJECT_NAME.wef`
in the project's root. Now you need to upload the generated executable into Wubix using
the `upload-file` command, then running `chmod +x` on it. Then all you need is to run the uploaded
executable:

```bash
$ ./$PROJECT_NAME.wef
Hello world!
```

When writing a program, you may want to use crates from `crates.io`. Please make sure that these
crates don't operate with the OS, or that you don't use parts that operate with the OS, for they
will simply not work with Wubix. Instead, we maintain [`wubix-crates`], where we fork crates and
adapt them for Wubix. Look for your crate there, and if it's there, add it from git.

A few notes on `librust`:

- `librust` is not thread-safe and can't be because `wasm-bindgen` is not thread-safe. Fortunately,
  Rust won't let you shoot in your foot.
- A few `librust` modules are simply re-exports from other crates. They're mostly compatible with
  `std`, except that `Path` is a re-export from a generic crate and doesn't implement methods that
  manipulate the filesystem. Instead, these methods are implemented on a trait, `PathExt`, which
  is available both in the `path` module and in the prelude.

[`wubix-crates`]: https://gitlab.com/wubix-crates

## The project's structure

This repository contains the very core parts:

- `kernel` is the kernel of Wubix;
- `browser` is somewhat the hardware for the kernel;
- `librust` is Wubix's standard library.

(`types` is a metacrate that shares types between the kernel and librust.)

There's another core part, `pkg2wef`, which collects `wasm-pack`'s build artefacts and compiles
a Wubix Executable File (WEF). Its source code is in [its own repository][pkg2wef], mainly because
npm cannot into monorepos (and it's hard to point to a specific issue because they're constantly
switching repos and issue trackers from forums to GitHub; why work when you can pretend that you
work?).

`systemw` used to be developed in this repository as well. But, as Wubix evolved, it started to make
more sense to develop `systemw` in [its own repository][systemw].

As mentioned above, there's a GitLab group, [`wubix-crates`], that serves as a registry of crates
ported to Wubix. But if you want to create a new project there, file an issue in
[`wpm`'s registry's repo][wpm-registry] (so far it's the most suitable repo for this kind of stuff).

[pkg2wef]: https://gitlab.com/wubix-crates/pkg2wef
[systemw]: https://gitlab.com/wubix-crates/systemw
[wpm-registry]: https://gitlab.com/wubix-crates/wubix-package-registry

## Building

```bash
./buildAll.sh
```

Each component also has its own Readme with build instructions. The `buildAll.sh` also shows
the order in which the components must be built.

## But... why?

Just for fun :) One may learn from this how to write WebAssembly applications or how Unix-like
systems work, though, and it's an interesting project on its own.
