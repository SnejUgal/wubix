const CopyWebpackPlugin = require(`copy-webpack-plugin`);
const path = require(`path`);

module.exports = {
  entry: `./bootloader.js`,
  output: {
    path: path.resolve(__dirname, `dist`),
    filename: `bootloader.js`,
  },
  module: {
    rules: [
      { test: /\.wef$/, use: [`file-loader`] },
      { test: /\.worker\.js$/, use: [`worker-loader`] },
    ],
  },
  mode: `development`,
  plugins: [new CopyWebpackPlugin({ patterns: [`index.html`] })],
};
