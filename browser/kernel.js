import {
  context,
  printLine,
  updateCanvasSize,
  bootParameters,
} from "./bootloader";
import * as idbfs from "./idbfs";
import ProcessWorker from "./process.worker.js";
import * as wef from "./wef";

import kernelWefUrl from "kernel";
import systemwWefUrl from "systemw";
import wpmWefUrl from "wpm/wpm.wef";

let kernelRef;
let memory;

let frameBuffer;
const renderFrameBuffer = () => {
  const buffer = new Uint8ClampedArray(
    memory.buffer,
    frameBuffer.buffer,
    frameBuffer.width * frameBuffer.height * 4
  );
  const imageData = new ImageData(
    buffer,
    frameBuffer.width,
    frameBuffer.height
  );
  context.putImageData(imageData, 0, 0);
};

const handleKeyUp = (event) => {
  if (event.code != `F12`) {
    event.preventDefault();
  }
  kernelRef.keyboardInterrupt(0, event.code);
};

const handleKeyDown = (event) => {
  if (event.code != `F12`) {
    event.preventDefault();
  }
  kernelRef.keyboardInterrupt(event.repeat ? 2 : 1, event.code);
};

let blinkInterruptTimeout = null;
const blinkInterrupt = () => {
  kernelRef.blinkInterrupt();
  blinkInterruptTimeout = setTimeout(blinkInterrupt, 400);
};

window.processes = new Map();
window.idbfs = idbfs;
window.kernel = {
  rerender: requestAnimationFrame.bind(window, renderFrameBuffer),
  panic(message) {
    for (const process of processes.values()) {
      process.terminate();
    }

    const error = new Error(message);
    console.error(error);

    window.removeEventListener(`keyup`, handleKeyUp);
    window.removeEventListener(`keydown`, handleKeyDown);
    clearTimeout(blinkInterruptTimeout);

    setTimeout(() => kernelRef.panic(message, error.stack));
  },
  now: performance.now.bind(performance),
  async startProcess(processId, file, args, variables) {
    const metadataEnd = file.indexOf(0);
    if (metadataEnd == -1) {
      throw new Error(`Invalid executable file`);
    }

    const metadataBytes = Buffer.from(file.slice(0, metadataEnd));
    const metadata = JSON.parse(metadataBytes.toString(`utf8`));
    const files = new Map();

    let offset = metadataEnd + 1;
    for (const [fileName, length] of metadata.files) {
      const contents = Uint8Array.from(file.slice(offset, offset + length));
      const mimeType = fileName.endsWith(`.js`)
        ? `text/javascript`
        : `application/wasm`;

      offset += length;
      const blob = URL.createObjectURL(
        new File([contents], fileName, { type: mimeType })
      );
      files.set(fileName, blob);
    }

    const worker = new ProcessWorker();
    processes.set(processId, worker);

    return new Promise((resolve, reject) => {
      worker.addEventListener(`message`, async (event) => {
        if (event.data.id === 0) {
          if (event.data.error) {
            reject(new Error(event.data.error));
          } else {
            resolve();
          }
          return;
        }

        if (event.data.systemCall === `getProcessId`) {
          worker.postMessage({ id: event.data.id, result: processId });
          return;
        }

        if (event.data.systemCall === `exit`) {
          this.terminateProcess(processId);
          await kernelRef
            .clone()
            .exitSystemCall(processId, ...event.data.arguments);
          return;
        }

        const method = `${event.data.systemCall}SystemCall`;
        try {
          const result = await kernelRef
            .clone()
            [method](processId, ...event.data.arguments);
          worker.postMessage({ id: event.data.id, result });
        } catch (error) {
          console.error(error);
        }
      });

      worker.postMessage({
        files,
        entry: metadata.entry,
        arguments: args,
        variables,
      });
    });
  },
  terminateProcess(processId) {
    const worker = window.processes.get(processId);
    if (worker) {
      worker.terminate();
      window.processes.delete(processId);
    }
  },
  executeCode(code) {
    return JSON.stringify(Function(code)()) || "";
  },
};

const executeKernel = (file) =>
  new Promise((resolve) => {
    const { files, entry } = wef.parse(file);
    const script = document.createElement(`script`);
    window.getFileBlob = (fileName) => files.get(fileName);
    script.src = files.get(entry);
    script.addEventListener(`load`, () => {
      window.executable.then(resolve);
    });
    document.head.append(script);
  });

const download = async (url) => {
  const request = await fetch(url);
  return request.arrayBuffer();
};

const firstBoot = async () => {
  const [kernelWef, systemwWef, wpmWef] = await Promise.all([
    download(kernelWefUrl),
    download(systemwWefUrl),
    download(wpmWefUrl),
  ]);
  const kernel = await executeKernel(Buffer.from(kernelWef));
  const { Kernel, Parameters, Initramfs } = kernel;
  memory = kernel.memory;

  printLine("Preparing initramfs for first boot...");
  let initramfs = Initramfs.new();
  initramfs = await initramfs.createDirectory(`/bin`);
  initramfs = await initramfs.createDirectory(`/dev`);
  initramfs = await initramfs.createFile(`/boot/wubix`, Buffer.from(kernelWef));
  initramfs = await initramfs.createExecutable(
    `/bin/init`,
    Buffer.from(systemwWef)
  );
  initramfs = await initramfs.createExecutable(`/bin/wpm`, Buffer.from(wpmWef));

  return { initramfs: initramfs.intoAny(), Kernel, Parameters };
};

const equals = (firstName, secondName) => {
  if (firstName.length !== secondName.length) {
    return false;
  }

  return firstName.every((byte, index) => byte == secondName[index]);
};

const loadIdbFile = async (id, path) => {
  if (!path.startsWith(`/`)) {
    throw new Error(`The path ${path} is not absolute.`);
  }

  if (path.endsWith(`/`)) {
    throw new Error(`The path ${path} has a trailing slash.`);
  }

  const pathComponents = path
    .slice(1)
    .split(`/`)
    .map((x) => Buffer.from(x, `utf-8`));

  await idbfs.mount(id);

  try {
    let inode = 1;
    outer: for (const directory of pathComponents) {
      const node = await idbfs.getInode(id, inode);
      if (!node.kind.Directory) {
        throw new Error(`The path ${path} does not point to a file.`);
      }

      for (const [name, fileInode] of node.kind.Directory) {
        if (!equals(name, directory)) {
          continue;
        }

        inode = fileInode;
        continue outer;
      }

      throw new Error(`The path ${path} does not exist.`);
    }

    return idbfs.readFile(id, inode);
  } finally {
    await idbfs.unmount(id);
  }
};

const loadKernel = async (parameters) => {
  if (parameters === null) {
    return firstBoot();
  }

  if (typeof parameters.kernel !== `object` || parameters.kernel === null) {
    throw new Error(`Missing "kernel" key in boot parameters.`);
  }

  if (parameters.kernel.type !== `idbfs`) {
    throw new Error(
      `Unknown file system type specified in "kernel"."type": ${parameters.kernel.type}.`
    );
  }

  if (typeof parameters.kernel.id !== `string`) {
    throw new Error(`Missing "kernel"."id" in boot parameters.`);
  }

  if (typeof parameters.kernel.path !== `string`) {
    throw new Error(`Missing "kernel"."path" in boot parameters.`);
  }

  if (
    typeof parameters.initramfs !== `object` ||
    parameters.initramfs === null
  ) {
    throw new Error(`Missing "initramfs" key in boot parameters.`);
  }

  if (parameters.initramfs.type !== `idbfs`) {
    throw new Error(
      `Unknown file system type specified in "initramfs"."type": ${parameters.kernel.type}.`
    );
  }

  if (typeof parameters.initramfs.id !== `string`) {
    throw new Error(`Missing "initramfs"."id" in boot parameters.`);
  }

  const kernelWef = await loadIdbFile(
    parameters.kernel.id,
    parameters.kernel.path
  );
  const kernel = await executeKernel(kernelWef);
  const { Kernel, Parameters, Initramfs } = kernel;
  memory = kernel.memory;
  const initramfs = await Initramfs.from_idbfs(parameters.initramfs.id);

  return { initramfs, Kernel, Parameters };
};

export const start = async () => {
  const { initramfs, Kernel, Parameters } = await loadKernel(bootParameters);

  const parameters = Parameters.new(
    Math.floor(window.innerWidth * window.devicePixelRatio),
    Math.floor(window.innerHeight * window.devicePixelRatio),
    Math.round(window.devicePixelRatio),
    initramfs
  );
  const kernel = Kernel.new(parameters);

  frameBuffer = kernel.getFrameBuffer();
  updateCanvasSize();

  kernelRef = kernel.intoRef();

  printLine(`Booting...`);
  await kernelRef.clone().init();

  window.addEventListener("keyup", handleKeyUp);
  window.addEventListener("keydown", handleKeyDown);
  blinkInterruptTimeout = setTimeout(blinkInterrupt, 250);
};
