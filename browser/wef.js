export const parse = (bytes) => {
  const metadataEnd = bytes.indexOf(0);
  if (metadataEnd == -1) {
    throw new Error(`Invalid executable file`);
  }

  const metadataBytes = Buffer.from(bytes.slice(0, metadataEnd));
  const metadata = JSON.parse(metadataBytes.toString(`utf8`));
  const files = new Map();

  let offset = metadataEnd + 1;
  for (const [fileName, length] of metadata.files) {
    const contents = Uint8Array.from(bytes.slice(offset, offset + length));
    const mimeType = fileName.endsWith(`.js`)
      ? `text/javascript`
      : `application/wasm`;

    offset += length;
    const blob = URL.createObjectURL(
      new File([contents], fileName, { type: mimeType })
    );
    files.set(fileName, blob);
  }

  return { files, entry: metadata.entry };
};
