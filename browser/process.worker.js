let nextMessageId = 1;
const waitingForSystemCalls = new Map();

self.systemCalls = {
  sleep(duration) {
    return new Promise((resolve) => setTimeout(resolve, duration));
  },
  async panic(message) {
    // this has to be done in JS because Rust futures don't run after
    // panicking
    await systemCalls.write(2, message);
    await systemCalls.flush(2);
    await systemCalls.exit(1);
  },
};

// kernel system calls as opposed to “system calls” that can be done without
// calling the kernel (e.g. `sleep`).
const kernelSystemCalls = [
  `exit`,
  `open`,
  `read`,
  `write`,
  `flush`,
  `seek`,
  `readMetadata`,
  `readFileMetadata`,
  `setPermissions`,
  `setFilePermissions`,
  `close`,
  `getProcessId`,
  `readDirectory`,
  `unlinkFile`,
  `removeDirectory`,
  `canonicalize`,
  `createDirectory`,
  `createHardLink`,
  `rename`,
  `getCurrentWorkingDirectory`,
  `setCurrentWorkingDirectory`,
  `getExecutablePath`,
  `createPipe`,
  `execute`,
  `awaitProcess`,
  `mount`,
  `unmount`,
  `duplicate`,
  `kill`,
];

for (const systemCall of kernelSystemCalls) {
  self.systemCalls[systemCall] = (...args) => {
    let id = nextMessageId++;
    self.postMessage({
      id,
      systemCall,
      arguments: args,
    });
    return new Promise((resolve) => waitingForSystemCalls.set(id, resolve));
  };
}

self.addEventListener(`message`, async (event) => {
  if (event.data.files != undefined) {
    self.getFileBlob = (filename) => event.data.files.get(filename);
    self.importScripts(event.data.files.get(event.data.entry));
    const executable = self.executable;
    const resolved = await executable;

    if (resolved && `start` in resolved) {
      self.postMessage({ id: 0, error: null });

      setTimeout(async () => {
        if (`__librust_init` in resolved) {
          resolved.__librust_init(event.data.arguments, event.data.variables);
        }

        await resolved.start();
        await self.systemCalls.exit(0);
      });
    } else {
      self.postMessage({ id: 0, error: `No start method` });
    }
  } else if (event.data.result != undefined) {
    let resolve = waitingForSystemCalls.get(event.data.id);
    waitingForSystemCalls.delete(event.data.id);
    resolve(event.data.result);
  }
});
