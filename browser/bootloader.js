import * as kernel from "./kernel";

const canvas = document.querySelector(`canvas`);
export const context = canvas.getContext(`2d`);

const FONT_SIZE = 12 * window.devicePixelRatio;
const LINE_HEIGHT = 1.25 * FONT_SIZE;

export const updateCanvasSize = () => {
  canvas.width = Math.floor(window.innerWidth * window.devicePixelRatio);
  canvas.height = Math.floor(window.innerHeight * window.devicePixelRatio);

  canvas.style.setProperty(`width`, `${canvas.width / window.devicePixelRatio}px`);
  canvas.style.setProperty(`height`, `${canvas.height / window.devicePixelRatio}px`);

  context.font = `${FONT_SIZE}px monospace`;
  context.fillStyle = `rgb(204, 204, 204)`;
};

let currentLine = 0;
export const printLine = (line) => {
  if (currentLine >= Math.floor(canvas.height / LINE_HEIGHT)) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    currentLine = 0;
  }

  currentLine += 1;
  context.fillText(line, 0, currentLine * LINE_HEIGHT);
};

let bootStage = `booting`;
export let continueBooting = Promise.resolve();

const loadBootParameters = () => {
  const value = localStorage.getItem(`bootloader`);
  if (value === null) {
    return null;
  }

  try {
    return JSON.parse(value);
  } catch (error) {
    printLine(`Failed to parse boot parameters:`);
    printLine(`  ${error.message}`);
    printLine(`Dropping to boot menu.`);
    enterBootMenu();
  }
};
export let bootParameters = loadBootParameters();
const printBootParameters = () => {
  if (bootParameters !== null) {
    printLine(`These parameters will be used to boot the kernel:`);
    const lines = JSON.stringify(bootParameters, null, 4).split(`\n`);
    lines.forEach((line) => printLine(line));
  } else {
    printLine(`No parameters are set.`);
  }
};

let exitBootMenu = () => {};
const printBootMenu = () => {
  printBootParameters();
  printLine(`Possible actions:`);
  printLine(`- Press E to overwrite boot parameters;`);
  printLine(`- Press S to permanently save boot parameters;`);
  printLine(`- Press Enter to continue booting;`);
};
const enterBootMenu = () => {
  window.addEventListener(`keydown`, handleKeyDown);
  continueBooting = new Promise((resolve) => (exitBootMenu = resolve));
  bootStage = `bootMenu`;
  printBootMenu();
};

const handleKeyDown = (event) => {
  switch (bootStage) {
    case `booting`: {
      if (event.code !== `ControlLeft`) {
        return;
      }

      printLine(`Paused booting and entering the boot menu...`);
      enterBootMenu();

      break;
    }

    case `bootMenu`: {
      if (event.code === `Enter`) {
        printLine(`Continuing booting.`);
        bootStage = `booting`;
        exitBootMenu();
      }

      if (event.code === `KeyE`) {
        const rawParameters = prompt(
          `Provide new parameters as JSON.`,
          JSON.stringify(bootParameters)
        );
        try {
          bootParameters = JSON.parse(rawParameters);
        } catch (error) {
          printLine(`Failed to parse JSON: ${error.message}.`);
        }
        printBootMenu();
      }

      if (event.code === `KeyS`) {
        localStorage.setItem(`bootloader`, JSON.stringify(bootParameters));
        printLine(`Saved boot parameters permanently.`);
      }

      break;
    }
  }
};

window.addEventListener(`keydown`, handleKeyDown);

updateCanvasSize();
printLine("Press Control to enter the boot menu.");
printLine("Loading the kernel...");

(async () => {
  if (bootParameters && bootParameters.dropIntoBootMenu) {
    printLine("Dropping into boot menu as specified in boot parameters.");
    enterBootMenu();
  } else {
    await new Promise((resolve) => setTimeout(resolve, 50));
  }

  while (true) {
    try {
      await continueBooting;
      window.removeEventListener(`keydown`, handleKeyDown);
      await kernel.start();
      break;
    } catch (error) {
      console.error(error);
      printLine(error.message);
      printLine(`Dropping into boot menu...`);
      enterBootMenu();
    }
  }
})();
