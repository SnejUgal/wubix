# Wubix‘s browser part

The browser part somewhat serves as the hardware. It boots the kernel,
renders on the screen, provides access to devices, lets proccesses make system
calls.

## Building

```bash
npm install # once
npm start # for development
npm run build # for production
```
