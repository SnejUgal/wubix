import * as idb from "idb";

const mountedFileSystems = new Map();

export const mount = async (id) => {
  const database = await idb.openDB(id, 1, {
    async upgrade(database) {
      database.createObjectStore(`inodes`, { autoIncrement: true });
      database.createObjectStore(`fileContents`);
    },
  });
  mountedFileSystems.set(id, database);
};

export const unmount = async (id) => {
  mountedFileSystems.delete(id);
};

export const getInode = async (id, inode) => {
  const database = mountedFileSystems.get(id);
  const transaction = database.transaction(`inodes`).objectStore(`inodes`);
  return transaction.get(inode);
};

export const createInode = async (id, value) => {
  const database = mountedFileSystems.get(id);
  const transaction = database
    .transaction(`inodes`, `readwrite`)
    .objectStore(`inodes`);
  return transaction.put(value);
};

export const updateInode = async (id, inode, value) => {
  const database = mountedFileSystems.get(id);
  const transaction = database
    .transaction(`inodes`, `readwrite`)
    .objectStore(`inodes`);
  await transaction.put(value, inode);
};

export const readFile = async (id, inode) => {
  const database = mountedFileSystems.get(id);
  const transaction = database
    .transaction(`fileContents`)
    .objectStore(`fileContents`);
  return transaction.get(inode);
};

export const writeFile = async (id, inode, contents) => {
  const database = mountedFileSystems.get(id);
  const transaction = database
    .transaction(`fileContents`, `readwrite`)
    .objectStore(`fileContents`);
  await transaction.put(contents, inode);
};
