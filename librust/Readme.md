# The Wubix standard library

The Wubix standard library is the foundation for Wubix programs, a complement
to the Rust standard library with OS operations adapted to Wubix.

## Using in a crate

```toml
[dependencies]
librust = { git = "https://gitlab.com/snejugal/wubix.git" }
```

If contributing to this repository, however, you need to add `librust` by path:

```toml
librust = { path = "../librust" }
```

## Building

```bash
wasm-pack build
```

Note that you don‘t need to build `librust` separately, as `cargo` will build
it when building your program. Running the above command is only useful
during development to check if `librust` compiles.
