//! A module for working with processes.

mod command;

pub use command::*;

/// Returns the process ID assigned by Wubix.
pub async fn id() -> u32 {
    crate::sys::get_process_id().await
}

/// Terminates the current process with the specified exit code.
///
/// This function will never return. The exit code is passed through to Wubix
/// and will be available for consumption by another process.
#[allow(clippy::empty_loop)] // no, you're wrong
pub fn exit(return_code: i32) -> ! {
    crate::sys::exit(return_code);
    loop {
        // the `exit` system call may actually return, but we don't want that.
        // This loop is here to prevent executing anything after calling `exit`.
        // This shouldn't however loop for too long, as the process will be
        // terminated as soon as possible.
    }
}
