use crate::{
    ffi::{OsStr, OsString},
    path::PathBuf,
};
use std::{
    fmt::{self, Display, Formatter},
    iter::Map,
    slice::Split,
};

/// An iterator that splits an environment variable into paths according to Unix
/// conventions.
#[derive(Debug)]
pub struct SplitPaths<'a> {
    iterator: Map<Split<'a, u8, SeparatorPredicator>, BytesToPath>,
}

type BytesToPath = fn(&[u8]) -> PathBuf;
fn bytes_to_path(bytes: &[u8]) -> PathBuf {
    PathBuf::from(OsString::from_vec(bytes.to_owned()))
}

type SeparatorPredicator = fn(&u8) -> bool;
fn is_path_separator(b: &u8) -> bool {
    *b == b':'
}

/// Parses input according to Unix conventions for the PATH environment
/// variable.
pub fn split_paths<P: AsRef<OsStr> + ?Sized>(path: &P) -> SplitPaths<'_> {
    let path = path.as_ref();
    SplitPaths {
        iterator: path
            .as_bytes()
            .split(is_path_separator as fn(&u8) -> bool)
            .map(bytes_to_path as fn(&[u8]) -> PathBuf),
    }
}

impl<'a> Iterator for SplitPaths<'a> {
    type Item = PathBuf;

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iterator.size_hint()
    }
}

/// The error type for operations on the PATH variable. Possibly returned from
/// the [`env::join_paths`] function.
///
/// [`env::join_paths`]: ./fn.join_paths.html
#[derive(Debug)]
pub struct JoinPathsError;

/// Joins a collection of paths appropriately for the `PATH` environment variable.
pub fn join_paths<I, T>(paths: I) -> Result<Vec<u8>, JoinPathsError>
where
    I: Iterator<Item = T>,
    T: AsRef<OsStr>,
{
    let mut joined = Vec::new();

    for (i, path) in paths.enumerate() {
        let path = path.as_ref();
        if i > 0 {
            joined.push(b':')
        }
        if path.as_bytes().contains(&b':') {
            return Err(JoinPathsError);
        }
        joined.extend_from_slice(path.as_bytes());
    }
    Ok(joined)
}

impl Display for JoinPathsError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "path segment contains separator `:`")
    }
}

impl std::error::Error for JoinPathsError {}
