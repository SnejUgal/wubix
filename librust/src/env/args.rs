use crate::ffi::OsString;
use std::vec::IntoIter;

#[derive(Debug)]
pub struct Args {
    inner: ArgsOs,
}

pub fn args() -> Args {
    Args { inner: args_os() }
}

#[derive(Debug)]
pub struct ArgsOs {
    args: IntoIter<OsString>,
}

pub fn args_os() -> ArgsOs {
    ArgsOs {
        args: super::ARGUMENTS.with(|cell| cell.borrow().clone().into_iter()),
    }
}

impl Iterator for ArgsOs {
    type Item = OsString;

    fn next(&mut self) -> Option<Self::Item> {
        self.args.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.args.size_hint()
    }
}

impl ExactSizeIterator for ArgsOs {
    fn len(&self) -> usize {
        self.args.len()
    }
}

impl DoubleEndedIterator for ArgsOs {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.args.next_back()
    }
}

impl Iterator for Args {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|s| s.into_string().unwrap())
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}

impl ExactSizeIterator for Args {
    fn len(&self) -> usize {
        self.inner.len()
    }
}

impl DoubleEndedIterator for Args {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.inner.next_back().map(|s| s.into_string().unwrap())
    }
}
