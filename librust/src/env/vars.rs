use crate::ffi::{OsStr, OsString};
use std::{collections::hash_map::IntoIter, convert::AsRef, fmt};

/// The error type for operations interacting with environment variables.
/// Possibly returned from the `env::var` function.
#[derive(Debug)]
pub enum VarError {
    NotPresent,
    NotUnicode(OsString),
}

impl fmt::Display for VarError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            VarError::NotPresent => write!(f, "environment variable not found"),
            VarError::NotUnicode(ref s) => {
                write!(f, "environment variable was not valid unicode: {:?}", s)
            }
        }
    }
}

/// An iterator over a snapshot of the environment variables of this process.
#[derive(Debug)]
pub struct Vars {
    inner: VarsOs,
}

/// Returns an iterator of (variable, value) pairs of strings,
/// for all the environment variables of the current process.
pub fn vars() -> Vars {
    Vars { inner: vars_os() }
}

/// An iterator over a snapshot of the environment variables of this process.
#[derive(Debug)]
pub struct VarsOs {
    vars: IntoIter<OsString, OsString>,
}

/// Returns an iterator of (variable, value) pairs of OS strings,
/// for all the environment variables of the current process.
pub fn vars_os() -> VarsOs {
    VarsOs {
        vars: super::ENVIRONMENT_VARIABLES.with(|cell| cell.borrow().clone().into_iter()),
    }
}

impl Iterator for Vars {
    type Item = (String, String);

    fn next(&mut self) -> Option<(String, String)> {
        self.inner
            .next()
            .map(|(key, value)| (key.into_string().unwrap(), value.into_string().unwrap()))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}

impl Iterator for VarsOs {
    type Item = (OsString, OsString);

    fn next(&mut self) -> Option<Self::Item> {
        self.vars.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.vars.size_hint()
    }
}

/// Sets the environment variable `key` to the value `value` for the currently running process.
pub fn set_var<K: AsRef<OsStr>, V: AsRef<OsStr>>(key: K, value: V) {
    super::ENVIRONMENT_VARIABLES.with(|cell| {
        cell.borrow_mut()
            .insert(key.as_ref().to_owned(), value.as_ref().to_owned())
    });
}

/// Fetches the environment variable `key` from the current process.
pub fn var<K: AsRef<OsStr>>(key: K) -> Result<String, VarError> {
    match var_os(key) {
        Some(s) => s.into_string().map_err(VarError::NotUnicode),
        None => Err(VarError::NotPresent),
    }
}

/// Fetches the environment variable `key` from the current process,
/// returning `None` if the variable isn't set.
pub fn var_os<K: AsRef<OsStr>>(key: K) -> Option<OsString> {
    super::ENVIRONMENT_VARIABLES.with(|cell| cell.borrow().get(key.as_ref()).map(Clone::clone))
}

/// Removes an environment variable from the environment of the currently running process.
pub fn remove_var<K: AsRef<OsStr>>(key: K) {
    super::ENVIRONMENT_VARIABLES.with(|cell| cell.borrow_mut().remove(key.as_ref()));
}
