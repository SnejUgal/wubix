//! Filesystem manipulations.

use crate::{
    ffi::OsString,
    io,
    path::{Path, PathBuf},
};
use std::convert::AsRef;
use types::fs::{self, ClassPermissions, EntryKind};

mod dir_builder;
mod file;
mod read_dir;
mod remove;
pub use {dir_builder::*, file::*, read_dir::*, remove::*};

/// Metadata information about the file.
#[derive(Debug, Clone)]
pub struct Metadata {
    inner: fs::Metadata,
}

/// A structure representing a type of file with accessors for each file type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct FileType {
    inner: EntryKind,
}

/// Representation of the various permissions on a file.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Permissions {
    inner: fs::Permissions,
}

/// Given a path, query the file system to get information about a file, directory, etc.
pub async fn metadata<P: AsRef<Path>>(path: P) -> io::Result<Metadata> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::read_metadata(path).await;

    match result {
        Ok(inner) => Ok(Metadata { inner }),
        Err(error) => Err(error.into()),
    }
}

/// Given a path, query the file system to get information about a file, directory, etc without
/// following the link.
pub async fn symlink_metadata<P: AsRef<Path>>(path: P) -> io::Result<Metadata> {
    metadata(path).await
}

/// Returns the canonical, absolute form of a path with all intermediate
/// components normalized and symbolic links resolved.
pub async fn canonicalize<P: AsRef<Path>>(path: P) -> io::Result<PathBuf> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::canonicalize(path).await;

    match result {
        Ok(path) => Ok(PathBuf::from(OsString::from_vec(path))),
        Err(error) => Err(error.into()),
    }
}

/// Changes the permissions found on a file or a directory.
pub async fn set_permissions<P: AsRef<Path>>(path: P, permissions: Permissions) -> io::Result<()> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::set_permissions(path, permissions.inner).await;

    result.map_err(Into::into)
}

/// Creates a new hard link on the filesystem.
pub async fn hard_link<P: AsRef<Path>, Q: AsRef<Path>>(
    source: P,
    destination: Q,
) -> io::Result<()> {
    let source = source.as_ref().as_unix_str().as_bytes().to_owned();
    let destination = destination.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::create_hard_link(source, destination).await;

    result.map_err(Into::into)
}

#[allow(clippy::len_without_is_empty)]
impl Metadata {
    /// Returns the file type for this metadata.
    pub fn file_type(&self) -> FileType {
        FileType {
            inner: self.inner.kind,
        }
    }

    /// Tests whether this file type represents a directory. The result is
    /// mutually exclusive to the result of [`is_file`], only zero or one of
    /// these tests may pass.
    ///
    /// [`is_file`]: #method.is_file
    pub fn is_dir(&self) -> bool {
        self.inner.kind == EntryKind::Directory
    }

    /// Tests whether this file type represents a regular file. The result is
    /// mutually exclusive to the result of [`is_dir`], only zero or one of
    /// these tests may pass.
    ///
    /// [`is_dir`]: #method.is_dir
    pub fn is_file(&self) -> bool {
        self.inner.kind == EntryKind::File
    }

    /// Returns the size of the file in bytes.
    pub fn len(&self) -> u64 {
        self.inner.size as u64
    }

    /// Returns the permissions of the file.
    pub fn permissions(&self) -> Permissions {
        Permissions {
            inner: self.inner.permissions,
        }
    }

    /// Returns the file's device ID.
    pub fn dev(&self) -> u64 {
        self.inner.device.0
    }

    /// Returns the inode of this file.
    pub fn ino(&self) -> u64 {
        self.inner.inode.0 as u64
    }

    /// Returns the rights applied to this file.
    pub fn mode(&self) -> u32 {
        self.permissions().mode()
    }

    /// Returns the user ID of the owner of this file.
    pub fn uid(&self) -> u32 {
        self.inner.owner_id.0
    }

    /// Returns the group ID of the owner of this file.
    pub fn gid(&self) -> u32 {
        self.inner.group_id.0
    }

    /// Returns the total size of this file in bytes.
    pub fn size(&self) -> u64 {
        self.inner.size as u64
    }
}

impl FileType {
    /// Tests whether this file type represents a directory. The result is mutually exclusive to
    /// the results of [`is_file`], [`is_symlink`], [`is_fifo`], [`is_block_device`] and
    /// [`is_char_device`], only one of these tests may pass.
    ///
    /// [`is_file`]: Self::is_file
    /// [`is_symlink`]: Self::is_symlink
    /// [`is_fifo`]: Self::is_fifo
    /// [`is_block_device`]: Self::is_block_device
    /// [`is_char_device`]: Self::is_char_device
    pub fn is_dir(&self) -> bool {
        self.inner == EntryKind::Directory
    }

    /// Tests whether this file type represents a regular file. The result is mutually exclusive to
    /// the results of [`is_dir`], [`is_symlink`], [`is_fifo`], [`is_block_device`] and
    /// [`is_char_device`], only one of these tests  may pass.
    ///
    /// [`is_dir`]: Self::is_dir
    /// [`is_symlink`]: Self::is_symlink
    /// [`is_fifo`]: Self::is_fifo
    /// [`is_block_device`]: Self::is_block_device
    /// [`is_char_device`]: Self::is_char_device
    pub fn is_file(&self) -> bool {
        self.inner == EntryKind::File
    }

    /// Tests whether this file type represents a symlink. The result is mutually exclusive to
    /// the results of [`is_dir`], [`is_file`], [`is_fifo`], [`is_block_device`] and
    /// [`is_char_device`], only one of these tests may pass.
    ///
    /// [`is_dir`]: Self::is_dir
    /// [`is_file`]: Self::is_file
    /// [`is_fifo`]: Self::is_fifo
    /// [`is_block_device`]: Self::is_block_device
    /// [`is_char_device`]: Self::is_char_device
    pub fn is_symlink(&self) -> bool {
        false
    }

    /// Tests whether this file type represents a pipe. The result is mutually exclusive to
    /// the results of [`is_dir`], [`is_file`], [`is_symlink`], [`is_block_device`] and
    /// [`is_char_device`], only one of these tests may pass.
    ///
    /// [`is_dir`]: Self::is_dir
    /// [`is_file`]: Self::is_file
    /// [`is_symlink`]: Self::is_symlink
    /// [`is_block_device`]: Self::is_block_device
    /// [`is_char_device`]: Self::is_char_device
    pub fn is_fifo(&self) -> bool {
        self.inner == EntryKind::Pipe
    }

    /// Tests whether this file type represents a block device. The result is mutually exclusive to
    /// the results of [`is_dir`], [`is_file`], [`is_symlink`], [`is_fifo`] and
    /// [`is_char_device`], only one of these tests may pass.
    ///
    /// [`is_dir`]: Self::is_dir
    /// [`is_file`]: Self::is_file
    /// [`is_symlink`]: Self::is_symlink
    /// [`is_fifo`]: Self::is_fifo
    /// [`is_char_device`]: Self::is_char_device
    pub fn is_block_device(&self) -> bool {
        false
    }

    /// Tests whether this file type represents a block device. The result is mutually exclusive to
    /// the results of [`is_dir`], [`is_file`], [`is_symlink`], [`is_fifo`] and
    /// [`is_block_device`], only one of these tests may pass.
    ///
    /// [`is_dir`]: Self::is_dir
    /// [`is_file`]: Self::is_file
    /// [`is_symlink`]: Self::is_symlink
    /// [`is_fifo`]: Self::is_fifo
    /// [`is_block_device`]: Self::is_block_device
    pub fn is_char_device(&self) -> bool {
        self.inner == EntryKind::CharacterDevice
    }
}

impl Permissions {
    /// Creates a new instance of `Permissions` from the given set of Unix
    /// permission bits.
    pub fn from_mode(mode: u32) -> Self {
        Self {
            inner: fs::Permissions {
                set_user_id: mode & 0o4000 != 0,
                set_group_id: mode & 0o2000 != 0,
                is_sticky: mode & 0o1000 != 0,
                owner: ClassPermissions {
                    can_read: mode & 0o0400 != 0,
                    can_write: mode & 0o0200 != 0,
                    can_execute: mode & 0o0100 != 0,
                },
                group: ClassPermissions {
                    can_read: mode & 0o0040 != 0,
                    can_write: mode & 0o0020 != 0,
                    can_execute: mode & 0o0010 != 0,
                },
                others: ClassPermissions {
                    can_read: mode & 0o0004 != 0,
                    can_write: mode & 0o0002 != 0,
                    can_execute: mode & 0o0001 != 0,
                },
            },
        }
    }

    /// Sets the underlying bits for this set of permissions.
    pub fn set_mode(&mut self, mode: u32) {
        *self = Self::from_mode(mode);
    }

    /// Returns this set of permissions as `u32`.
    pub fn mode(&self) -> u32 {
        ((self.inner.set_user_id as u32) << 11)
            | ((self.inner.set_group_id as u32) << 10)
            | ((self.inner.is_sticky as u32) << 9)
            | ((self.inner.owner.can_read as u32) << 8)
            | ((self.inner.owner.can_write as u32) << 7)
            | ((self.inner.owner.can_execute as u32) << 6)
            | ((self.inner.group.can_read as u32) << 5)
            | ((self.inner.group.can_write as u32) << 4)
            | ((self.inner.group.can_execute as u32) << 3)
            | ((self.inner.others.can_read as u32) << 2)
            | ((self.inner.others.can_write as u32) << 1)
            | (self.inner.others.can_execute as u32)
    }

    /// Returns `true` if the file is non-writable.
    ///
    /// *Note:* this doesn't check if the file is readable to be compatible
    /// with `std`.
    pub fn readonly(&self) -> bool {
        !self.inner.owner.can_write && !self.inner.group.can_write && !self.inner.others.can_write
    }

    /// Sets all write permissions to `!readonly`.
    ///
    /// This operation does *not* modify the filesystem. To do that,
    /// use `fs::set_permissions`.
    ///
    /// *Note:* this doesn't affect read permissions to be compatible with `std`.
    pub fn set_readonly(&mut self, readonly: bool) {
        self.inner.owner.can_write = !readonly;
        self.inner.group.can_write = !readonly;
        self.inner.others.can_write = !readonly;
    }
}
