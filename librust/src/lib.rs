//! # The Wubix standard library
//!
//! The Wubix standard library is the foundation for Wubix programs,
//! a complement to the Rust standard library with OS operations adapted
//! to Wubix.

pub use futures::stream;

pub mod env;
pub mod ffi;
pub mod fs;
pub mod io;
pub mod path;
pub mod process;
pub mod sys;
pub mod thread;

mod init;
mod panic;

/// A prelude for `librust`.
pub mod prelude {
    pub use crate::path::PathExt as _;
}
