//! Utilities related to FFI bindings.

pub use unix_str::{UnixStr as OsStr, UnixString as OsString};
