//! Wrappers around raw Wubix system calls.
//!
//! The purpose of this module is to mitigate the hassle of transforming
//! JS types into native Rust ones and vice versa, especially Promises.
//! Providing an idiomatic, type-safe API, however, is _not_ the purpose
//! this module; that's what the rest of the crate is for. You should only
//! use this module if you want to use a system call not covered by`std`,
//! e.g. `mount`.

use js_sys::Promise;
use serde::de::DeserializeOwned;
use types::{
    fs::Permissions,
    ids::FileDescriptorId,
    system_calls::{
        await_process,
        canonicalize::CanonicalizeResult,
        close::CloseResult,
        create_directory::CreateDirectoryResult,
        create_hard_link::CreateHardLinkResult,
        create_pipe::CreatePipeResult,
        duplicate::DuplicateResult,
        execute::{ExecuteParameters, ExecuteResult},
        flush::FlushResult,
        get_current_working_directory::GetCurrentWorkingDirectoryResult,
        get_executable_path::GetExecutablePathResult,
        kill,
        mount::{MountOptions, MountResult},
        open::{OpenMode, OpenResult},
        read::ReadResult,
        read_directory::ReadDirectoryResult,
        read_metadata::ReadMetadataResult,
        remove_directory::RemoveDirectoryResult,
        rename::RenameResult,
        seek::{SeekFrom, SeekResult},
        set_current_working_directory::SetCurrentWorkingDirectoryResult,
        set_permissions::SetPermissionsResult,
        unlink_file::UnlinkFileResult,
        write::WriteResult,
    },
};
use wasm_bindgen_futures::JsFuture;

pub use types;

pub mod raw {
    //! Even more low-level system calls. Use with extra care; wrapped versions
    //! in the parent module should be enough for most cases.

    use js_sys::Promise;
    use wasm_bindgen::prelude::*;

    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn canonicalize(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn exit(return_code: i32);

        #[wasm_bindgen(js_namespace = systemCalls, js_name = getProcessId)]
        pub fn get_process_id() -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn sleep(duration: i32) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn panic(message: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn open(path: Vec<u8>, mode: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn read(descriptor: u32, count: usize) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn write(descriptor: u32, buffer: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn flush(descriptor: u32) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn seek(descriptor: u32, seek_from: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = readFileMetadata)]
        pub fn read_file_metadata(descriptor: u32) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn close(descriptor: u32) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = readMetadata)]
        pub fn read_metadata(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = readDirectory)]
        pub fn read_directory(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = unlinkFile)]
        pub fn unlink_file(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = removeDirectory)]
        pub fn remove_directory(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = setFilePermissions)]
        pub fn set_file_permissions(descriptor: u32, permissions: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = setPermissions)]
        pub fn set_permissions(path: Vec<u8>, permissions: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = createDirectory)]
        pub fn create_directory(path: Vec<u8>, permissions: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = createHardLink)]
        pub fn create_hard_link(origin: Vec<u8>, target: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn rename(origin: Vec<u8>, target: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = getCurrentWorkingDirectory)]
        pub fn get_current_working_directory() -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = setCurrentWorkingDirectory)]
        pub fn set_current_working_directory(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = getExecutablePath)]
        pub fn get_executable_path() -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = createPipe)]
        pub fn create_pipe() -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn execute(parameters: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls, js_name = awaitProcess)]
        pub fn await_process(parameters: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn mount(path: Vec<u8>, mount_options: JsValue) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn unmount(path: Vec<u8>) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn duplicate(descriptor: u32) -> Promise;

        #[wasm_bindgen(js_namespace = systemCalls)]
        pub fn kill(parameters: JsValue) -> Promise;
    }
}

async fn await_system_call<T: DeserializeOwned>(promise: Promise) -> T {
    let future: JsFuture = promise.into();
    let result = future.await.unwrap();
    serde_wasm_bindgen::from_value(result).unwrap()
}

/// Canonicalizes the given path.
pub async fn canonicalize(path: Vec<u8>) -> CanonicalizeResult {
    await_system_call(raw::canonicalize(path)).await
}

/// Exit with the specified return code. Note that due to the asynchronous nature of communication
/// with the kernel, this function may return and the process will continue running for a while.
pub fn exit(return_code: i32) {
    raw::exit(return_code);
}

/// Gets the process's ID.
pub async fn get_process_id() -> u32 {
    await_system_call(raw::get_process_id()).await
}

/// Sleeps for at least the givem amount of milliseconds.
pub async fn sleep(duration: i32) {
    await_system_call(raw::sleep(duration)).await
}

/// Prints the message to `stderr`, flushes and exits with exit code `1`. The only reason this
/// system call exists is that the above actions are asynchronous, but after a panic `wasm_bindgen`
/// doesn't progress any Future.
pub fn panic(message: Vec<u8>) {
    let _ = raw::panic(message);
}

/// Opens a file and returns a file descriptor.
pub async fn open(path: Vec<u8>, mode: OpenMode) -> OpenResult {
    let mode = serde_wasm_bindgen::to_value(&mode).unwrap();
    await_system_call(raw::open(path, mode)).await
}

/// Reads the given amount of bytes from the descriptor.
pub async fn read(descriptor: FileDescriptorId, count: usize) -> ReadResult {
    await_system_call(raw::read(descriptor.0, count)).await
}

/// Writes the given buffer into the descriptor.
pub async fn write(descriptor: FileDescriptorId, buffer: Vec<u8>) -> WriteResult {
    await_system_call(raw::write(descriptor.0, buffer)).await
}

/// Flushes the descriptor.
pub async fn flush(descriptor: FileDescriptorId) -> FlushResult {
    await_system_call(raw::flush(descriptor.0)).await
}

/// Seeks the descriptor.
pub async fn seek(descriptor: FileDescriptorId, seek_from: SeekFrom) -> SeekResult {
    let seek_from = serde_wasm_bindgen::to_value(&seek_from).unwrap();
    await_system_call(raw::seek(descriptor.0, seek_from)).await
}

/// Reads a file's metadata given its descriptor.
pub async fn read_file_metadata(descriptor: FileDescriptorId) -> ReadMetadataResult {
    await_system_call(raw::read_file_metadata(descriptor.0)).await
}

/// Closes a descriptor.
pub async fn close(descriptor: FileDescriptorId) -> CloseResult {
    await_system_call(raw::close(descriptor.0)).await
}

/// Reads the metadatd of a file or a directory at the given path.
pub async fn read_metadata(path: Vec<u8>) -> ReadMetadataResult {
    await_system_call(raw::read_metadata(path)).await
}

/// Returns information about the directory's entries.
pub async fn read_directory(path: Vec<u8>) -> ReadDirectoryResult {
    await_system_call(raw::read_directory(path)).await
}

/// Unlinks a file.
pub async fn unlink_file(path: Vec<u8>) -> UnlinkFileResult {
    await_system_call(raw::unlink_file(path)).await
}

/// Removes a directory.
pub async fn remove_directory(path: Vec<u8>) -> RemoveDirectoryResult {
    await_system_call(raw::remove_directory(path)).await
}

/// Sets permissions of a file or a directory at the give path.
pub async fn set_permissions(path: Vec<u8>, permissions: Permissions) -> SetPermissionsResult {
    let permissions = serde_wasm_bindgen::to_value(&permissions).unwrap();
    await_system_call(raw::set_permissions(path, permissions)).await
}

/// Sets permissions of a file given its descriptor.
pub async fn set_file_permissions(
    descriptor: FileDescriptorId,
    permissions: Permissions,
) -> SetPermissionsResult {
    let permissions = serde_wasm_bindgen::to_value(&permissions).unwrap();
    await_system_call(raw::set_file_permissions(descriptor.0, permissions)).await
}

/// Creates a directory.
pub async fn create_directory(path: Vec<u8>, permissions: Permissions) -> CreateDirectoryResult {
    let permissions = serde_wasm_bindgen::to_value(&permissions).unwrap();
    await_system_call(raw::create_directory(path, permissions)).await
}

/// Creates a hard link.
pub async fn create_hard_link(origin: Vec<u8>, target: Vec<u8>) -> CreateHardLinkResult {
    await_system_call(raw::create_hard_link(origin, target)).await
}

/// Renames a file or a directory.
pub async fn rename(origin: Vec<u8>, target: Vec<u8>) -> RenameResult {
    await_system_call(raw::rename(origin, target)).await
}

/// Get's the process's working directory.
pub async fn get_current_working_directory() -> GetCurrentWorkingDirectoryResult {
    await_system_call(raw::get_current_working_directory()).await
}

/// Set's the process's working directory.
pub async fn set_current_working_directory(path: Vec<u8>) -> SetCurrentWorkingDirectoryResult {
    await_system_call(raw::set_current_working_directory(path)).await
}

/// Gets the path of the process's executable.
pub async fn get_executable_path() -> GetExecutablePathResult {
    await_system_call(raw::get_executable_path()).await
}

/// Creates a pipe and returns descriptors for reading and writing ends respectively.
pub async fn create_pipe() -> CreatePipeResult {
    await_system_call(raw::create_pipe()).await
}

/// Executes a command.
pub async fn execute(parameters: ExecuteParameters) -> ExecuteResult {
    let parameters = serde_wasm_bindgen::to_value(&parameters).unwrap();
    await_system_call(raw::execute(parameters)).await
}

/// Waits for a process to exit.
pub async fn await_process(parameters: await_process::Parameters) -> await_process::Result {
    let parameters = serde_wasm_bindgen::to_value(&parameters).unwrap();
    await_system_call(raw::await_process(parameters)).await
}

/// Mounts a file system.
pub async fn mount(path: Vec<u8>, mount_options: MountOptions) -> MountResult {
    let mount_options = serde_wasm_bindgen::to_value(&mount_options).unwrap();
    await_system_call(raw::mount(path, mount_options)).await
}

/// Unmounts a file system.
pub async fn unmount(path: Vec<u8>) -> MountResult {
    await_system_call(raw::unmount(path)).await
}

/// Duplicates a file descriptor.
pub async fn duplicate(descriptor: FileDescriptorId) -> DuplicateResult {
    await_system_call(raw::duplicate(descriptor.0)).await
}

/// Sends a signal to processes.
pub async fn kill(parameters: kill::Parameters) -> kill::Result {
    let parameters = serde_wasm_bindgen::to_value(&parameters).unwrap();
    await_system_call(raw::kill(parameters)).await
}
