//! Native threads (or one thread, to be exact).

use std::{convert::TryInto, time::Duration};

/// Sleeps for the specifies amount of time.
///
/// As it uses `setTimeout` under the hood, all of its restrictions apply here.
///
/// # Panics
///
/// This function panics if the duration is longer than `2^31 - 1` milliseconds.
pub async fn sleep(duration: Duration) {
    let duration = duration.as_millis().try_into().unwrap();
    crate::sys::sleep(duration).await
}
