//! Traits, helpers and type definitions for core I/O functionality.

use super::fs::File;
use std::{
    cell::{RefCell, RefMut},
    ops::{Deref, DerefMut},
    pin::Pin,
    rc::Rc,
    task::{Context, Poll},
};
use types::ids::FileDescriptorId;

pub use futures::io::copy;
pub use futures::io::BufWriter;
pub use futures::io::Cursor;
pub use futures::io::{empty, Empty};
pub use futures::io::{repeat, Repeat};
pub use futures::io::{sink, Sink};
pub use futures::io::{AsyncBufRead as PollBufRead, AsyncBufReadExt as BufRead, BufReader, Lines};
pub use futures::io::{
    AsyncRead as PollRead, AsyncReadExt as Read, Chain, IoSliceMut, Read as ReadFuture, Take,
};
pub use futures::io::{AsyncSeek as PollSeek, AsyncSeekExt as Seek};
pub use futures::io::{AsyncWrite as PollWrite, AsyncWriteExt as Write, IoSlice};
pub use std::io::{Error, ErrorKind, Result, SeekFrom};

/// The I/O prelude.
pub mod prelude {
    pub use super::{BufRead as _, Read as _, Seek as _, Write as _};
}
mod line_writer;

pub use line_writer::LineWriter;

type RawStdin = Rc<RefCell<BufReader<File>>>;
fn create_raw_stdin() -> RawStdin {
    let file = File::from_descriptor(FileDescriptorId(0));
    let buf_reader = BufReader::new(file);
    Rc::new(RefCell::new(buf_reader))
}

type RawStdout = Rc<RefCell<LineWriter<File>>>;
fn create_raw_stdout(descriptor: u32) -> RawStdout {
    let file = File::from_descriptor(FileDescriptorId(descriptor));
    let buf_reader = LineWriter::new(file);
    Rc::new(RefCell::new(buf_reader))
}

thread_local! {
    static STDIN: RawStdin = create_raw_stdin();
    static STDOUT: RawStdout = create_raw_stdout(1);
    static STDERR: RawStdout = create_raw_stdout(2);
}

/// A handle to the global standard output stream of the current process.
#[derive(Debug)]
pub struct Stdout {
    inner: RawStdout,
}
/// A locked reference to the `Stdout` handle.
#[derive(Debug)]
pub struct StdoutLock<'a> {
    lock: RefMut<'a, LineWriter<File>>,
}

/// Constructs a new handle to the standard output of the current process.
pub fn stdout() -> Stdout {
    Stdout {
        inner: STDOUT.with(Rc::clone),
    }
}

impl Stdout {
    /// Locks this handle to the standard output stream, returning a writable guard.
    pub fn lock(&self) -> StdoutLock<'_> {
        StdoutLock {
            lock: self.inner.try_borrow_mut().unwrap_or_else(|_| {
                panic!("tried to lock stdout when it's already locked");
            }),
        }
    }
}

impl PollWrite for StdoutLock<'_> {
    fn poll_write(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut *self.lock).poll_write(context, buffer)
    }
    fn poll_flush(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut *self.lock).poll_flush(context)
    }
    fn poll_close(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut *self.lock).poll_close(context)
    }
}

impl PollWrite for Stdout {
    fn poll_write(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut self.lock()).poll_write(context, buffer)
    }

    fn poll_flush(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut self.lock()).poll_flush(context)
    }

    fn poll_close(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut self.lock()).poll_close(context)
    }
}

/// A handle to the global standard error stream of the current process.
#[derive(Debug)]
pub struct Stderr {
    inner: RawStdout,
}

/// A locked reference to the `Stderr` handle.
#[derive(Debug)]
pub struct StderrLock<'a> {
    lock: RefMut<'a, LineWriter<File>>,
}

/// Constructs a new handle to the standard error of the current process.
pub fn stderr() -> Stderr {
    Stderr {
        inner: STDERR.with(Rc::clone),
    }
}

impl Stderr {
    /// Locks this handle to the standard error stream, returning a writable guard.
    pub fn lock(&self) -> StderrLock<'_> {
        StderrLock {
            lock: self.inner.try_borrow_mut().unwrap_or_else(|_| {
                panic!("tried to lock stderr when it's already locked");
            }),
        }
    }
}

impl PollWrite for StderrLock<'_> {
    fn poll_write(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut *self.lock).poll_write(context, buffer)
    }
    fn poll_flush(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut *self.lock).poll_flush(context)
    }
    fn poll_close(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut *self.lock).poll_close(context)
    }
}

impl PollWrite for Stderr {
    fn poll_write(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut self.lock()).poll_write(context, buffer)
    }

    fn poll_flush(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut self.lock()).poll_flush(context)
    }

    fn poll_close(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Result<()>> {
        Pin::new(&mut self.lock()).poll_close(context)
    }
}

/// A handle to the global standard input stream of the current process.
#[derive(Debug)]
pub struct Stdin {
    inner: RawStdin,
}

/// A locked reference to the `Stdin` handle.
#[derive(Debug)]
pub struct StdinLock<'a> {
    lock: RefMut<'a, BufReader<File>>,
}

/// Constructs a new handle to the standard input of the current process.
pub fn stdin() -> Stdin {
    Stdin {
        inner: STDIN.with(Rc::clone),
    }
}

impl Stdin {
    /// Locks this handle to the standard input stream, returning a readable guard.
    pub fn lock(&self) -> StdinLock {
        StdinLock {
            lock: self.inner.try_borrow_mut().unwrap_or_else(|_| {
                panic!("tried to lock stdin when it's already locked");
            }),
        }
    }

    /// Locks this handle and reads a line of input.
    pub async fn read_line(&self, buffer: &mut String) -> Result<usize> {
        self.lock().read_line(buffer).await
    }
}

impl PollRead for StdinLock<'_> {
    fn poll_read(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut *self.lock).poll_read(context, buffer)
    }
}

impl Deref for StdinLock<'_> {
    type Target = BufReader<File>;
    fn deref(&self) -> &Self::Target {
        &self.lock
    }
}

impl DerefMut for StdinLock<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.lock
    }
}

impl PollRead for Stdin {
    fn poll_read(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<Result<usize>> {
        Pin::new(&mut *self.inner.borrow_mut()).poll_read(context, buffer)
    }
}

// Raw file descriptors.
pub type RawFd = u32;

/// A trait to extract the raw file descriptor from an underlying object.
pub trait AsRawFd {
    /// Extracts the file descriptor.
    fn as_raw_fd(&self) -> RawFd;
}

impl AsRawFd for RawFd {
    fn as_raw_fd(&self) -> RawFd {
        *self
    }
}

impl AsRawFd for Stdin {
    fn as_raw_fd(&self) -> RawFd {
        0
    }
}

impl AsRawFd for StdinLock<'_> {
    fn as_raw_fd(&self) -> RawFd {
        0
    }
}

impl AsRawFd for Stdout {
    fn as_raw_fd(&self) -> RawFd {
        1
    }
}

impl AsRawFd for StdoutLock<'_> {
    fn as_raw_fd(&self) -> RawFd {
        1
    }
}

impl AsRawFd for Stderr {
    fn as_raw_fd(&self) -> RawFd {
        2
    }
}

impl AsRawFd for StderrLock<'_> {
    fn as_raw_fd(&self) -> RawFd {
        2
    }
}

/// A trait for consuming an object and gaining ownership of its raw file descriptor.
pub trait IntoRawFd {
    /// Consumes this object returning the raw file descriptor.
    fn into_raw_fd(self) -> RawFd;
}

impl IntoRawFd for RawFd {
    fn into_raw_fd(self) -> RawFd {
        self
    }
}

/// A trait for constructing an object from a raw file descriptor.
pub trait FromRawFd {
    /// Construct a new instance of `Self` from the raw file descriptor.
    ///
    /// # Safety
    ///
    /// The new instance gains ownership of the file descriptor. If this is violated, this may
    /// cause unsafety.
    unsafe fn from_raw_fd(descriptor: RawFd) -> Self;
}

impl FromRawFd for RawFd {
    unsafe fn from_raw_fd(descriptor: RawFd) -> Self {
        descriptor
    }
}

/// Writes formatted data into a buffer.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() -> Result<(), librust::io::Error> {
/// use librust::{write, fs::File};
///
/// let mut file = File::create("/tmp/file").await?;
/// write!(&mut file, "test").await?;
/// write!(&mut file, "formatted {}", "arguments").await?;
/// # Ok(())
/// # }
/// ```
#[macro_export]
macro_rules! write {
    ($destination:expr, $($argument:tt)*) => {
        async {
            let message = ::std::format!($($argument)*);
            $crate::io::Write::write_all($destination, message.as_bytes()).await
        }
    };
}

/// Writes formatted data into a buffer, with a newline appended.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() -> Result<(), librust::io::Error> {
/// use librust::{writeln, fs::File};
///
/// let mut file = File::create("/tmp/file").await?;
/// writeln!(&mut file).await?;
/// writeln!(&mut file, "test").await?;
/// writeln!(&mut file, "formatted {}", "arguments").await?;
/// # Ok(())
/// # }
/// ```
#[macro_export]
macro_rules! writeln {
    ($destination:expr) => {
        $crate::write!($destination, "\n")
    };
    ($destination:expr,) => {
        $crate::writeln!($destination)
    };
    ($destination:expr, $($argument:tt)*) => {
        async {
            let mut message = ::std::format!($($argument)*);
            message.push('\n');
            $crate::io::Write::write_all($destination, message.as_bytes()).await
        }
    };
}

/// Prints to the standard output.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() {
/// use librust::{print, io::{stdout, Write}};
///
/// print!("this ").await;
/// print!("will ").await;
/// print!("be ").await;
/// print!("on ").await;
/// print!("the ").await;
/// print!("same ").await;
/// print!("line ").await;
///
/// stdout().flush().await.unwrap();
///
/// print!("this string has a newline, why not choose println! instead?\n").await;
///
/// stdout().flush().await.unwrap();
/// # }
/// ```
#[macro_export]
macro_rules! print {
    ($($argument:tt)*) => {
        async {
            $crate::write!(&mut $crate::io::stdout(), $($argument)*).await.unwrap();
        }
    };
}

/// Prints to the standard output, with a newline.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() {
/// use librust::println;
///
/// println!().await;
/// print!("hello there!").await;
/// print!("format {} arguments", "some").await;
/// # }
/// ```
#[macro_export]
macro_rules! println {
    () => {
        async {
            let stdout = $crate::io::stdout();
            let mut lock = stdout.lock();
            $crate::writeln!(&mut lock).await.unwrap();
        }
    };
    ($($argument:tt)*) => {
        async {
            let stdout = $crate::io::stdout();
            let mut lock = stdout.lock();
            $crate::writeln!(&mut lock, $($argument)*).await.unwrap();
        }
    };
}

/// Prints to the standard error.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() {
/// use librust::eprint;
///
/// eprint!("Error: Could not complete task").await;
/// # }
/// ```
#[macro_export]
macro_rules! eprint {
    ($($argument:tt)*) => {
        async {
            $crate::write!(&mut $crate::io::stderr(), $($argument)*).await.unwrap();
        }
    };
}

/// Prints to the standard error, with a newline.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```no_run
/// # async fn start() {
/// use librust::eprintln;
///
/// eprintln!("Error: Could not complete task").await;
/// # }
/// ```
#[macro_export]
macro_rules! eprintln {
    () => {
        async {
            let stderr = $crate::io::stderr();
            let mut lock = stderr.lock();
            $crate::writeln!(&mut lock).await.unwrap();
        }
    };
    ($($argument:tt)*) => {
        async {
            let stderr = $crate::io::stderr();
            let mut lock = stderr.lock();
            $crate::writeln!(&mut lock, $($argument)*).await.unwrap();
        }
    };
}

/// Prints and returns the value of a given expression for quick and dirty
/// debugging.
///
/// This macro is similar to `std`'s, but it uses `librust::io::Write` and you
/// need to await this macro:
///
/// ```rust
/// use librust::dbg;
///
/// let a = 2;
/// let b = dbg!(a * 2).await + 1;
/// //      ^-- prints: [src/main.rs:4] a * 2 = 4
/// assert_eq!(b, 5);
/// ```
#[macro_export]
macro_rules! dbg {
    () => {
        $crate::eprintln!("[{}:{}]", ::std::file!(), ::std::line!())
    };
    ($val:expr) => {
        async {
            // Use of `match` here is intentional because it affects the lifetimes
            // of temporaries - https://stackoverflow.com/a/48732525/1063961
            match $val {
                tmp => {
                    $crate::eprintln!("[{}:{}] {} = {:#?}",
                        ::std::file!(), ::std::line!(), ::std::stringify!($val), &tmp).await;
                    tmp
                }
            }
        }
    };
    // Trailing comma with single argument is ignored
    ($val:expr,) => { $crate::dbg!($val) };
    ($($val:expr),+ $(,)?) => {
        ($($crate::dbg!($val)),+,)
    };
}
