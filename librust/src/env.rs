//! Inspection and manipulation of the process's environment.

use crate::{
    ffi::OsString,
    io,
    path::{Path, PathBuf},
};
use std::{cell::RefCell, collections::HashMap};

mod args;
mod paths;
mod vars;
pub use args::*;
pub use paths::*;
pub use vars::*;

thread_local! {
    static ARGUMENTS: RefCell<Vec<OsString>> = RefCell::new(Vec::new());
    static ENVIRONMENT_VARIABLES: RefCell<HashMap<OsString, OsString>> = RefCell::new(HashMap::new());
}

/// Returns the current working directory as a `PathBuf`.
pub async fn current_dir() -> io::Result<PathBuf> {
    let result = crate::sys::get_current_working_directory().await;

    match result {
        Ok(path) => Ok(PathBuf::from(OsString::from_vec(path))),
        Err(error) => Err(error.into()),
    }
}

/// Changes the current working directory to the specified path.
pub async fn set_current_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::set_current_working_directory(path).await;
    result.map_err(Into::into)
}

pub async fn current_exe() -> io::Result<PathBuf> {
    let result = crate::sys::get_executable_path().await;

    match result {
        Ok(path) => Ok(PathBuf::from(OsString::from_vec(path))),
        Err(error) => Err(error.into()),
    }
}

pub(crate) fn init(arguments: Vec<OsString>, environment_variables: HashMap<OsString, OsString>) {
    ARGUMENTS.with(|cell| *cell.borrow_mut() = arguments);
    ENVIRONMENT_VARIABLES.with(|cell| *cell.borrow_mut() = environment_variables);
}
