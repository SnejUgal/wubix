use crate::ffi::OsString;
use std::collections::HashMap;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn __librust_init(args: JsValue, variables: JsValue) {
    crate::panic::init();

    let arguments: Vec<Vec<u8>> = serde_wasm_bindgen::from_value(args).unwrap();
    let arguments = arguments.into_iter().map(OsString::from_vec).collect();

    let variables: HashMap<Vec<u8>, Vec<u8>> = serde_wasm_bindgen::from_value(variables).unwrap();
    let variables = variables
        .into_iter()
        .map(|(key, value)| (OsString::from_vec(key), OsString::from_vec(value)))
        .collect();

    crate::env::init(arguments, variables);
}
