use std::panic::{set_hook, PanicInfo};

fn hook(info: &PanicInfo<'_>) {
    let message = format!(
        "thread 'main' {}\n\
         note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace\n",
        info
    );
    crate::sys::panic(message.into_bytes());
}

pub fn init() {
    set_hook(Box::new(hook));
}
