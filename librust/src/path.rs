//! Path manipulations.

use crate::{
    fs::{self, Metadata, ReadDir},
    io,
};
use futures::future::LocalBoxFuture;
pub use unix_path::*;

/// A trait that implements `std::path::Path`'s methods that do I/O on
/// `librust`'s `Path` (which is simply exported from `unix_path`).
pub trait PathExt {
    /// Queries the file system to get information about a file, directory, etc.
    ///
    /// This is an alias to [`fs::metadata`].
    ///
    /// [`fs::metadata`]: ../fs/fn.metadata.html
    fn metadata(&self) -> LocalBoxFuture<'_, io::Result<Metadata>>;

    /// Queries the file system to get information about a file, directory, etc without following
    /// the link.
    ///
    /// This is an alias to [`fs::metadata`].
    ///
    /// [`fs::metadata`]: ../fs/fn.metadata.html
    fn symlink_metadata(&self) -> LocalBoxFuture<'_, io::Result<Metadata>> {
        self.metadata()
    }

    /// Returns the canonical, absolute form of the path with all intermediate
    /// components normalized and symbolic links resolved.
    ///
    /// This is an alias to [`fs::canonicalize`].
    ///
    /// [`fs::canonicalize`]: ../fs/fn.canonicalize.html
    fn canonicalize(&self) -> LocalBoxFuture<'_, io::Result<PathBuf>>;

    /// Returns an iterator over the entries within a directory.
    ///
    /// The iterator will yield instances of [`io::Result`]`<`[`DirEntry`]`>`.
    /// New errors may be encountered after an iterator is initially constructed.
    ///
    /// This is an alias to [`fs::read_dir`].
    ///
    /// [`io::Result`]: ../io/type.Result.html
    /// [`DirEntry`]: ../fs/struct.DirEntry.html
    /// [`fs::read_dir`]: ../fs/fn.read_dir.html
    fn read_dir(&self) -> LocalBoxFuture<'_, io::Result<ReadDir>>;

    /// Returns `true` if the path points at an existing entity.
    ///
    /// This function will traverse symbolic links to query information about
    /// the destination file. In case of broken symbolic links this will return
    /// `false`.
    ///
    /// If you cannot access the directory containing the file, e.g., because of
    /// a permission error, this will return `false`.
    ///
    /// This is a convenience function that coerces errors to false. If you want
    /// to check errors, call [`fs::metadata`].
    ///
    /// [`fs::metadata`]: ../../std/fs/fn.metadata.html
    fn exists(&self) -> LocalBoxFuture<'_, bool>;

    /// Returns `true` if the path exists on disk and is pointing at a regular
    /// file.
    ///
    /// This function will traverse symbolic links to query information about
    /// the destination file. In case of broken symbolic links this will return
    /// `false`.
    ///
    /// If you cannot access the directory containing the file, e.g., because of
    /// a permission error, this will return `false`.
    ///
    /// This is a convenience function that coerces errors to false. If you want
    /// to check errors, call [fs::metadata] and handle its Result. Then call
    /// [`fs::Metadata::is_file`] if it was Ok.
    ///
    /// [fs::metadata]: ../../std/fs/fn.metadata.html
    /// [fs::Metadata::is_file]: ../../std/fs/struct.Metadata.html#method.is_file
    fn is_file(&self) -> LocalBoxFuture<'_, bool>;

    /// Returns `true` if the path exists on disk and is pointing at a directory.
    ///
    /// This function will traverse symbolic links to query information about
    /// the destination file. In case of broken symbolic links this will return
    /// `false`.
    ///
    /// If you cannot access the directory containing the file, e.g., because of
    /// a permission error, this will return `false`.
    ///
    /// This is a convenience function that coerces errors to false. If you want
    /// to check errors, call [fs::metadata] and handle its Result. Then call
    /// [`fs::Metadata::is_dir`] if it was Ok.
    ///
    /// [fs::metadata]: ../../std/fs/fn.metadata.html
    /// [fs::Metadata::is_dir]: ../../std/fs/struct.Metadata.html#method.is_dir
    fn is_dir(&self) -> LocalBoxFuture<'_, bool>;
}

impl PathExt for Path {
    fn metadata(&self) -> LocalBoxFuture<'_, io::Result<fs::Metadata>> {
        Box::pin(fs::metadata(self))
    }

    fn canonicalize(&self) -> LocalBoxFuture<'_, io::Result<PathBuf>> {
        Box::pin(fs::canonicalize(self))
    }

    fn read_dir(&self) -> LocalBoxFuture<'_, io::Result<fs::ReadDir>> {
        Box::pin(fs::read_dir(self))
    }

    fn exists(&self) -> LocalBoxFuture<'_, bool> {
        Box::pin(async move { fs::metadata(self).await.is_ok() })
    }

    fn is_file(&self) -> LocalBoxFuture<'_, bool> {
        Box::pin(async move {
            fs::metadata(self)
                .await
                .map(|m| m.is_file())
                .unwrap_or(false)
        })
    }

    fn is_dir(&self) -> LocalBoxFuture<'_, bool> {
        Box::pin(async move {
            fs::metadata(self)
                .await
                .map(|m| m.is_dir())
                .unwrap_or(false)
        })
    }
}
