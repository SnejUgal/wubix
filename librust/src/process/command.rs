use crate::{
    env,
    ffi::OsStr,
    fs::{self, Descriptor, File},
    io::{self, AsRawFd, FromRawFd, IntoRawFd, PollRead, PollWrite, Read},
    path::Path,
};
use futures::future::try_join;
use io::RawFd;
use std::{
    collections::HashMap,
    io::ErrorKind,
    pin::Pin,
    task::{Context, Poll},
};
use types::{
    ids::{FileDescriptorId, GroupId, ProcessId, UserId},
    io::IoErrorKind,
    signal::Signal,
    system_calls::{
        await_process::{self, ExitedProcess, WaitFor},
        execute,
        kill::{self, Destination},
    },
};

#[derive(Debug)]
pub struct Stdio {
    kind: StdioKind,
}

#[derive(Debug)]
enum StdioKind {
    MakePipe,
    Inherit,
    Null,
    Descriptor(Descriptor),
}

#[derive(Debug)]
pub struct Command {
    executable: Vec<u8>,
    arguments: Vec<Vec<u8>>,
    environment_variables: HashMap<Vec<u8>, Vec<u8>>,
    working_directory: Option<Vec<u8>>,
    stdin: Option<Stdio>,
    stdout: Option<Stdio>,
    stderr: Option<Stdio>,
    user_id: Option<UserId>,
    group_id: Option<GroupId>,
}

#[derive(Debug)]
pub struct Child {
    process_id: ProcessId,
    status: Option<i32>,
    pub stdin: Option<ChildStdin>,
    pub stdout: Option<ChildStdout>,
    pub stderr: Option<ChildStderr>,
}

#[derive(Debug)]
pub struct ChildStdin(File);
#[derive(Debug)]
pub struct ChildStdout(File);
#[derive(Debug)]
pub struct ChildStderr(File);

#[derive(Debug)]
pub struct ExitStatus(i32);

#[derive(Debug)]
pub struct Output {
    pub status: ExitStatus,
    pub stdout: Vec<u8>,
    pub stderr: Vec<u8>,
}

/// An enum for controlling file descriptors while we're trying execute a process
enum StreamDescriptor {
    /// This descriptor comes from somewhere else, so it's not our job to close it
    Foreign(FileDescriptorId),
    /// We created this descriptor, so we need to close it when returning from `execute`
    Ours(Descriptor),
}

impl StreamDescriptor {
    fn as_file_descriptor_id(&self) -> FileDescriptorId {
        match self {
            StreamDescriptor::Foreign(descriptor) => *descriptor,
            StreamDescriptor::Ours(descriptor) => descriptor.0,
        }
    }
}

async fn prepare_stream(
    descriptor: u32,
    stream: Option<&Stdio>,
    fallback: &StdioKind,
) -> io::Result<(StreamDescriptor, Option<File>)> {
    match stream.map(|x| &x.kind).unwrap_or(fallback) {
        StdioKind::MakePipe => {
            let (reading_half, writing_half) = crate::sys::create_pipe().await?;

            if descriptor == 0 {
                Ok((
                    StreamDescriptor::Ours(Descriptor::from(reading_half)),
                    Some(File::from_descriptor(writing_half)),
                ))
            } else {
                Ok((
                    StreamDescriptor::Ours(Descriptor::from(writing_half)),
                    Some(File::from_descriptor(reading_half)),
                ))
            }
        }
        StdioKind::Inherit => Ok((
            StreamDescriptor::Foreign(FileDescriptorId(descriptor)),
            None,
        )),
        StdioKind::Descriptor(descriptor) => Ok((StreamDescriptor::Foreign(descriptor.0), None)),
        StdioKind::Null => {
            let file = File::create("/dev/null").await?;
            Ok((
                StreamDescriptor::Ours(Descriptor::from(file.into_raw_fd())),
                None,
            ))
        }
    }
}

impl Command {
    /// Constructs a new Command for launching the program at path program, with the following default configuration:
    ///
    /// - No arguments to the program;
    /// - Inherit the current process's environment;
    /// - Inherit the current process's working directory;
    /// - Inherit stdin/stdout/stderr for spawn or status, but create pipes for output;
    ///
    /// Builder methods are provided to change these defaults and otherwise configure the process.
    ///
    /// If program is not an absolute path, the PATH will be searched in an OS-defined way.
    pub fn new<S: AsRef<OsStr>>(program: S) -> Self {
        Self {
            executable: program.as_ref().as_bytes().to_vec(),
            arguments: vec![program.as_ref().as_bytes().to_vec()],
            environment_variables: crate::env::vars_os()
                .map(|(variable, value)| (variable.into_vec(), value.into_vec()))
                .collect(),
            working_directory: None,
            stdin: None,
            stdout: None,
            stderr: None,
            user_id: None,
            group_id: None,
        }
    }

    /// Adds an argument to pass to the program. Only one argument can be passed per use.
    pub fn arg<S: AsRef<OsStr>>(&mut self, arg: S) -> &mut Self {
        self.arguments.push(arg.as_ref().as_bytes().to_vec());
        self
    }

    /// Adds multiple arguments to pass to the program.
    pub fn args<I, S>(&mut self, args: I) -> &mut Command
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        for arg in args {
            self.arg(arg);
        }
        self
    }

    /// Inserts or updates an environment variable mapping.
    pub fn env<K, V>(&mut self, key: K, value: V) -> &mut Self
    where
        K: AsRef<OsStr>,
        V: AsRef<OsStr>,
    {
        self.environment_variables.insert(
            key.as_ref().as_bytes().to_vec(),
            value.as_ref().as_bytes().to_vec(),
        );
        self
    }

    /// Adds or updates multiple environment variable mappings.
    pub fn envs<I, K, V>(&mut self, vars: I) -> &mut Self
    where
        I: IntoIterator<Item = (K, V)>,
        K: AsRef<OsStr>,
        V: AsRef<OsStr>,
    {
        for (ref key, ref value) in vars {
            self.env(key, value);
        }
        self
    }

    /// Removes an environment variable mapping.
    pub fn env_remove<K: AsRef<OsStr>>(&mut self, key: K) -> &mut Self {
        self.environment_variables.remove(key.as_ref().as_bytes());
        self
    }

    /// Clears the entire environment map for the child process.
    pub fn env_clear(&mut self) -> &mut Command {
        self.environment_variables.clear();
        self
    }

    /// Sets the working directory for the child process.
    pub fn current_dir<P: AsRef<Path>>(&mut self, dir: P) -> &mut Self {
        self.working_directory = Some(dir.as_ref().as_unix_str().as_bytes().to_owned());
        self
    }

    /// Configuration for the child process's standard input (stdin) handle.
    pub fn stdin<T: Into<Stdio>>(&mut self, cfg: T) -> &mut Self {
        self.stdin = Some(cfg.into());
        self
    }

    /// Configuration for the child process's standard output (stdout) handle.
    pub fn stdout<T: Into<Stdio>>(&mut self, cfg: T) -> &mut Self {
        self.stdout = Some(cfg.into());
        self
    }

    /// Configuration for the child process's standard error (stderr) handle.
    pub fn stderr<T: Into<Stdio>>(&mut self, cfg: T) -> &mut Self {
        self.stderr = Some(cfg.into());
        self
    }

    /// Sets the child process's user ID.
    pub fn uid(&mut self, user_id: u32) -> &mut Self {
        self.user_id = Some(UserId(user_id));
        self
    }

    /// Sets the child process's group ID.
    pub fn gid(&mut self, group_id: u32) -> &mut Self {
        self.group_id = Some(GroupId(group_id));
        self
    }

    async fn execute(&self, fallback_stdio: StdioKind) -> io::Result<Child> {
        let (stdin, stdin_handle) = prepare_stream(0, self.stdin.as_ref(), &fallback_stdio).await?;
        let (stdout, stdout_handle) =
            prepare_stream(1, self.stdout.as_ref(), &fallback_stdio).await?;
        let (stderr, stderr_handle) =
            prepare_stream(2, self.stderr.as_ref(), &fallback_stdio).await?;

        let working_directory = if let Some(directory) = &self.working_directory {
            directory.clone()
        } else {
            env::current_dir()
                .await
                .unwrap()
                .as_unix_str()
                .as_bytes()
                .to_owned()
        };

        let mut parameters = execute::ExecuteParameters {
            executable: Vec::new(),
            arguments: self.arguments.clone(),
            environment_variables: self.environment_variables.clone(),
            stdin: stdin.as_file_descriptor_id(),
            stdout: stdout.as_file_descriptor_id(),
            stderr: stderr.as_file_descriptor_id(),
            user_id: self.user_id,
            group_id: self.group_id,
            working_directory,
        };

        let process_id = if self.executable.contains(&b'/') {
            parameters.executable = self.executable.clone();
            crate::sys::execute(parameters).await?
        } else {
            let searched_paths = self
                .environment_variables
                .get(b"PATH" as &[u8])
                .map(|x| x.as_slice())
                .unwrap_or(b"/bin:/usr/bin"); // according to exec(3), on real Unixes, the default
                                              // PATH depends is implementation-defined, but
                                              // generally it is `/bin:/usr/bin`.

            let mut searched_paths = env::split_paths(OsStr::from_bytes(searched_paths));
            let mut last_error = io::ErrorKind::NotFound;

            loop {
                let searched_path = match searched_paths.next() {
                    Some(searched_path) => searched_path,
                    None => return Err(io::Error::from(last_error)),
                };

                parameters.executable = searched_path
                    .join(OsStr::from_bytes(&self.executable))
                    .into_unix_string()
                    .into_vec();
                match crate::sys::execute(parameters.clone()).await {
                    Ok(process_id) => break process_id,
                    Err(IoErrorKind::IsDirectory) | Err(IoErrorKind::NotFound) => continue,
                    Err(IoErrorKind::CorruptedExecutable) => {
                        // Just like a real Unix, if the executable isn't recognized, try executing
                        // it with a shell, and fail if that fails too.
                        parameters.arguments[0] = parameters.executable;
                        parameters.arguments.insert(0, b"/bin/sh".to_vec());
                        parameters.executable = b"/bin/sh".to_vec();
                        break crate::sys::execute(parameters).await?;
                    }
                    Err(IoErrorKind::PermissionDenied) => {
                        last_error = io::ErrorKind::PermissionDenied;
                        continue;
                    }
                    Err(error) => return Err(io::Error::from(error)),
                }
            }
        };

        Ok(Child {
            process_id,
            status: None,
            stdin: stdin_handle.map(ChildStdin),
            stdout: stdout_handle.map(ChildStdout),
            stderr: stderr_handle.map(ChildStderr),
        })
    }

    /// Executes the command as a child process, returning a handle to it.
    ///
    /// By default, stdin, stdout and stderr are inherited from the parent.
    pub async fn spawn(&self) -> io::Result<Child> {
        self.execute(StdioKind::Inherit).await
    }

    /// Executes the command as a child process, returning a handle to it.
    ///
    /// By default, stdin, stdout and stderr are inherited from the parent.
    pub async fn output(&self) -> io::Result<Output> {
        let child = self.execute(StdioKind::MakePipe).await?;
        child.wait_with_output().await
    }

    /// Executes a command as a child process, waiting for it to finish and
    /// collecting its exit status.
    pub async fn status(&mut self) -> io::Result<ExitStatus> {
        let mut child = self.execute(StdioKind::Inherit).await?;
        child.wait().await
    }
}

impl AsRawFd for ChildStdout {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}

impl IntoRawFd for ChildStdout {
    fn into_raw_fd(self) -> RawFd {
        self.0.into_raw_fd()
    }
}

impl PollRead for ChildStdout {
    fn poll_read(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<std::io::Result<usize>> {
        Pin::new(&mut self.0).poll_read(context, buffer)
    }
}

impl AsRawFd for ChildStderr {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}

impl IntoRawFd for ChildStderr {
    fn into_raw_fd(self) -> RawFd {
        self.0.into_raw_fd()
    }
}

impl PollRead for ChildStderr {
    fn poll_read(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        Pin::new(&mut self.0).poll_read(context, buffer)
    }
}

impl AsRawFd for ChildStdin {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}

impl IntoRawFd for ChildStdin {
    fn into_raw_fd(self) -> RawFd {
        self.0.into_raw_fd()
    }
}

impl PollWrite for ChildStdin {
    fn poll_write(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<io::Result<usize>> {
        Pin::new(&mut self.0).poll_write(context, buffer)
    }

    fn poll_flush(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.0).poll_flush(context)
    }

    fn poll_close(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.0).poll_close(context)
    }
}

impl Stdio {
    /// A new pipe should be arranged to connect the parent and child processes.
    pub fn piped() -> Self {
        Self {
            kind: StdioKind::MakePipe,
        }
    }

    /// The child inherits from the corresponding parent descriptor.
    pub fn inherit() -> Self {
        Self {
            kind: StdioKind::Inherit,
        }
    }

    /// This stream will be ignored. This is the equivalent of attaching the
    /// stream to `/dev/null`
    pub fn null() -> Self {
        Self {
            kind: StdioKind::Null,
        }
    }
}

impl Child {
    /// Sends `SIGKILL` to the child.
    pub async fn kill(&mut self) -> io::Result<()> {
        if self.status.is_some() {
            return Err(ErrorKind::InvalidInput.into());
        }

        crate::sys::kill(kill::Parameters {
            destination: Destination::Process(self.process_id),
            signal: Some(Signal::Kill),
        })
        .await?;

        Ok(())
    }

    /// Returns the OS-assigned process identifier associated with this child.
    pub fn id(&self) -> u32 {
        self.process_id.0
    }

    /// Waits for the child to exit completely, returning the status that it
    /// exited with. This function will continue to have the same return value
    /// after it has been called at least once.
    pub async fn wait(&mut self) -> io::Result<ExitStatus> {
        if let Some(exit_code) = self.status {
            return Ok(ExitStatus(exit_code));
        }

        drop(self.stdin.take());
        let exited_process = crate::sys::await_process(await_process::Parameters {
            wait_for: WaitFor::Process(self.process_id),
            only_try: false,
        })
        .await?;

        // unwrap safety: `None` is not returned if waiting for a specific process with blocking
        let ExitedProcess { exit_code, .. } = exited_process.unwrap();
        self.status = Some(exit_code);
        Ok(ExitStatus(exit_code))
    }

    /// Tries to collect the child's exit status if it has already exited.
    ///
    /// If the child hasn't exited yet, this method returns `Ok(None)` instead of waiting until it
    /// exists and reaping it.
    pub async fn try_wait(&mut self) -> io::Result<Option<ExitStatus>> {
        if let Some(exit_code) = self.status {
            return Ok(Some(ExitStatus(exit_code)));
        }

        let exited_process = crate::sys::await_process(await_process::Parameters {
            wait_for: WaitFor::Process(self.process_id),
            only_try: true,
        })
        .await?;

        if let Some(ExitedProcess { exit_code, .. }) = exited_process {
            self.status = Some(exit_code);
            Ok(Some(ExitStatus(exit_code)))
        } else {
            Ok(None)
        }
    }

    /// Simultaneously waits for the child to exit and collect all remaining
    /// output on the stdout/stderr handles, returning an `Output`
    /// instance.
    pub async fn wait_with_output(mut self) -> io::Result<Output> {
        drop(self.stdin.take());

        let (mut stdout, mut stderr) = (Vec::new(), Vec::new());
        match (self.stdout.take(), self.stderr.take()) {
            (None, None) => {}
            (Some(mut out), None) => {
                out.read_to_end(&mut stdout).await.unwrap();
            }
            (None, Some(mut err)) => {
                err.read_to_end(&mut stderr).await.unwrap();
            }
            (Some(mut out), Some(mut err)) => {
                try_join(out.read_to_end(&mut stdout), err.read_to_end(&mut stderr))
                    .await
                    .unwrap();
            }
        }

        let status = self.wait().await?;
        Ok(Output {
            status,
            stdout,
            stderr,
        })
    }
}

impl ExitStatus {
    /// Was termination successful? Signal termination is not considered a
    /// success, and success is defined as a zero exit status.
    pub fn success(&self) -> bool {
        self.0 == 0
    }

    /// Creates a new `ExitStatus` from the raw underlying i32 return value of a process.
    pub fn from_raw(status: i32) -> Self {
        Self(status)
    }

    /// Returns the exit code of the process, if any.
    pub fn code(&self) -> Option<i32> {
        if self.0.is_negative() {
            None
        } else {
            Some(self.0)
        }
    }

    /// If the process was terminated by a signal, returns that signal.
    pub fn signal(&self) -> Option<i32> {
        if self.0.is_negative() {
            Some(self.0)
        } else {
            None
        }
    }
}

impl From<fs::File> for Stdio {
    fn from(file: fs::File) -> Self {
        Self {
            kind: StdioKind::Descriptor(Descriptor::from(file)),
        }
    }
}

impl From<ChildStdin> for Stdio {
    fn from(stdin: ChildStdin) -> Self {
        Self {
            kind: StdioKind::Descriptor(Descriptor::from(stdin.0)),
        }
    }
}

impl From<ChildStderr> for Stdio {
    fn from(stderr: ChildStderr) -> Self {
        Self {
            kind: StdioKind::Descriptor(Descriptor::from(stderr.0)),
        }
    }
}

impl From<ChildStdout> for Stdio {
    fn from(stdout: ChildStdout) -> Self {
        Self {
            kind: StdioKind::Descriptor(Descriptor::from(stdout.0)),
        }
    }
}

impl FromRawFd for Stdio {
    unsafe fn from_raw_fd(descriptor: RawFd) -> Self {
        Self {
            kind: StdioKind::Descriptor(Descriptor::from(descriptor)),
        }
    }
}
