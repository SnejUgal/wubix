use super::FileType;
use crate::{
    ffi::OsString,
    io,
    path::{Path, PathBuf},
    stream::{iter, Iter, Stream},
};
use std::{
    collections::hash_map,
    pin::Pin,
    task::{Context, Poll},
};
use types::fs::Metadata;

/// Stream over a directory's entries.
#[derive(Debug)]
pub struct ReadDir {
    path: PathBuf,
    entries: Iter<hash_map::IntoIter<Vec<u8>, Metadata>>,
}

/// Entries returned by the [`ReadDir`] stream.
///
/// [`ReadDir`]: ./struct.ReadDir.html
#[derive(Debug)]
pub struct DirEntry {
    path: PathBuf,
    name: OsString,
    metadata: Metadata,
}

/// Returns a stream over the entries within the directory.
pub async fn read_dir<P: AsRef<Path>>(path: P) -> io::Result<ReadDir> {
    let path = path.as_ref().to_owned();
    let result = crate::sys::read_directory(path.as_unix_str().as_bytes().to_owned()).await;

    let entries = match result {
        Ok(entries) => entries,
        Err(error) => return Err(error.into()),
    };

    Ok(ReadDir {
        path,
        entries: iter(entries),
    })
}

impl Stream for ReadDir {
    type Item = io::Result<DirEntry>;

    fn poll_next(mut self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let (name, metadata) = match Pin::new(&mut self.entries).poll_next(context) {
            Poll::Ready(Some(entry)) => entry,
            Poll::Ready(None) => return Poll::Ready(None),
            Poll::Pending => return Poll::Pending,
        };

        Poll::Ready(Some(Ok(DirEntry {
            path: self.path.clone(),
            name: OsString::from_vec(name),
            metadata,
        })))
    }
}

impl DirEntry {
    /// Returns the full path to this entry.
    pub fn path(&self) -> PathBuf {
        self.path.join(&self.name)
    }

    /// Returns metadata for this entry.
    pub fn metadata(&self) -> io::Result<super::Metadata> {
        Ok(super::Metadata {
            inner: self.metadata,
        })
    }

    /// Returns the file type for this entry.
    pub fn file_type(&self) -> io::Result<FileType> {
        Ok(FileType {
            inner: self.metadata.kind,
        })
    }

    /// Returns the bare file name of this entry.
    pub fn file_name(&self) -> OsString {
        self.name.clone()
    }

    /// Returns the directory's inode.
    pub fn ino(&self) -> u64 {
        self.metadata.inode.0 as u64
    }
}
