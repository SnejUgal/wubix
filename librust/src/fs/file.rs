use super::{Metadata, Permissions};
use crate::{
    io::{self, prelude::*, AsRawFd, FromRawFd, IntoRawFd, RawFd},
    path::Path,
};
use derive_destructure::destructure;
use std::{
    cell::RefCell,
    convert::{AsRef, TryInto},
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};
use types::{
    ids::FileDescriptorId,
    io::IoErrorKind,
    system_calls::{
        close::CloseResult,
        flush::FlushResult,
        open::{CreateOptions, OpenMode, WriteOptions},
        read::ReadResult,
        seek::{SeekFrom, SeekResult},
        write::WriteResult,
    },
};
use wasm_bindgen_futures::JsFuture;

#[derive(Debug, destructure)]
pub(crate) struct Descriptor(pub FileDescriptorId);

impl From<FileDescriptorId> for Descriptor {
    fn from(descriptor: FileDescriptorId) -> Self {
        Self(descriptor)
    }
}

impl From<u32> for Descriptor {
    fn from(descriptor: u32) -> Self {
        Self(FileDescriptorId(descriptor))
    }
}

impl Descriptor {
    pub fn as_raw(&self) -> u32 {
        self.0 .0
    }
}

impl Drop for Descriptor {
    fn drop(&mut self) {
        // can't block until the file is closed, but the JS runtime should
        // close it for us
        let descriptor = self.0;
        wasm_bindgen_futures::spawn_local(async move {
            let _ = crate::sys::close(descriptor).await;
        });
    }
}

/// A reference to an open file.
#[derive(Debug)]
pub struct File {
    descriptor: Descriptor,
    read_future: RefCell<Option<JsFuture>>,
    write_future: RefCell<Option<JsFuture>>,
    flush_future: RefCell<Option<JsFuture>>,
    close_future: RefCell<Option<JsFuture>>,
    seek_future: RefCell<Option<JsFuture>>,
}

/// Flags which configure how a file is opened.
///
/// This builder configures how a `File` is opened and what operations are
/// allowed on it.
#[derive(Debug, Clone)]
pub struct OpenOptions {
    read: bool,
    write: bool,
    create: bool,
    create_new: bool,
    truncate: bool,
    append: bool,
    permissions: Permissions,
}

async fn initial_buffer_size(file: &File) -> usize {
    file.metadata()
        .await
        .map(|m| m.len() as usize + 1)
        .unwrap_or(0)
}

/// Read the entire contents of a file into a vector.
///
/// This is a convenience function for using [`File::open`] and [`read_to_end`]
/// with fewer imports and without an intermediate variable.
///
/// [`File::open`]: struct.File.html#method.open
/// [`read_to_end`]: ../io/trait.Read.html#method.read_to_end
pub async fn read<P: AsRef<Path>>(path: P) -> io::Result<Vec<u8>> {
    let mut file = File::open(path).await?;
    let mut bytes = Vec::with_capacity(initial_buffer_size(&file).await);
    file.read_to_end(&mut bytes).await?;
    Ok(bytes)
}

/// Read the entire contents of a file into a string.
///
/// This is a convenience function for using [`File::open`] and [`read_to_string`]
/// with fewer imports and without an intermediate variable.
///
/// [`File::open`]: struct.File.html#method.open
/// [`read_to_string`]: ../io/trait.Read.html#method.read_to_string
pub async fn read_to_string<P: AsRef<Path>>(path: P) -> io::Result<String> {
    let mut file = File::open(path).await?;
    let mut string = String::with_capacity(initial_buffer_size(&file).await);
    file.read_to_string(&mut string).await?;
    Ok(string)
}

/// Write a slice as the entire contents of a file.
pub async fn write<P: AsRef<Path>, C: AsRef<[u8]>>(path: P, contents: C) -> io::Result<()> {
    File::create(path).await?.write_all(contents.as_ref()).await
}

/// Copies the contents of one file to another. This function will also copy
/// the permission bits of the original file to the destination file.
///
/// This function will overwrite the contents of `to`.
///
/// Note that if from and to both point to the same file, then the file will
/// likely get truncated by this operation.
///
/// On success, the total number of bytes copied is returned and it is equal
/// to the length of the to file as reported by metadata.
///
/// If you’re wanting to copy the contents of one file to another
/// and you’re working with `File`s, see the [`io::copy`] function.
///
/// [`io::copy`]: ../io/fn.copy.html
pub async fn copy<P: AsRef<Path>, Q: AsRef<Path>>(from: P, to: Q) -> io::Result<u64> {
    let from = File::open(from).await?;
    let from_metadata = from.metadata().await?;
    if !from_metadata.is_file() {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "the source path is not an existing regular file",
        ));
    }

    let permissions = from_metadata.permissions();
    let mut to = OpenOptions::new()
        .mode(permissions.mode())
        .write(true)
        .create(true)
        .truncate(true)
        .open(to)
        .await?;
    let to_metadata = to.metadata().await?;
    if to_metadata.is_file() {
        to.set_permissions(permissions).await?;
    }

    io::copy(from, &mut to).await
}

/// Rename a file or directory to a new name, replacing the original file if to already exists.
/// This will not work if the new name is on a different mount point.
pub async fn rename<P: AsRef<Path>, Q: AsRef<Path>>(from: P, to: Q) -> io::Result<()> {
    let from = from.as_ref().as_unix_str().as_bytes().to_owned();
    let to = to.as_ref().as_unix_str().as_bytes().to_owned();

    let result = crate::sys::rename(from, to).await;
    result.map_err(Into::into)
}

impl OpenOptions {
    /// Creates a new set of options for configuration.
    ///
    /// All options are initialized with `false`.
    #[allow(clippy::new_without_default)] // we're mimicking std
    pub fn new() -> Self {
        Self {
            read: false,
            write: false,
            create: false,
            create_new: false,
            truncate: false,
            append: false,
            permissions: Permissions::from_mode(0o666),
        }
    }

    /// Sets the option for read access.
    pub fn read(&mut self, read: bool) -> &mut Self {
        self.read = read;
        self
    }

    /// Sets the option for write access.
    pub fn write(&mut self, write: bool) -> &mut Self {
        self.write = write;
        self
    }

    /// Sets the option for the append mode.
    ///
    /// When `true`, writes will append to the file instead of overwriting
    /// previous contents. Setting `.write(true).append(true)` is equivalent
    /// to setting only `.append(true)`.
    ///
    /// If the file is opened with both read and append access, the position
    /// for reading will be set at the end of the file after opening and after
    /// evetry write. So, save the current position before writing and restore
    /// it before the next read.
    pub fn append(&mut self, append: bool) -> &mut Self {
        self.append = append;
        if append {
            self.write = true;
        }
        self
    }

    /// Sets the option for truncating the file at opening.
    ///
    /// The file must be opened with write access for truncate to work.
    pub fn truncate(&mut self, truncate: bool) -> &mut Self {
        self.truncate = truncate;
        self
    }

    /// Sets the option to create a new file if it doesn't exist.
    ///
    /// The file must be opened with write access or append mode for the file
    /// to be created.
    pub fn create(&mut self, create: bool) -> &mut Self {
        self.create = create;
        self
    }

    /// Sets the option to create a new file and fail if it already exists.
    ///
    /// If the call succeeds, the returned file is guaranteed to be new.
    /// If `.create_new(true)` is set, `.create` and `.truncate` are ignored.
    /// The file must be opened with write access or append mode for the file
    /// to be created.
    pub fn create_new(&mut self, create_new: bool) -> &mut Self {
        self.create_new = create_new;
        self
    }

    pub fn mode(&mut self, mode: u32) -> &mut Self {
        self.permissions = Permissions::from_mode(mode);
        self
    }

    /// Opens a file at `path` with the previously specified options.
    pub async fn open<P: AsRef<Path>>(&self, path: P) -> io::Result<File> {
        let open_mode = match (self.read, self.write) {
            (false, false) => return Err(io::ErrorKind::InvalidInput.into()),
            (true, false) => {
                if self.truncate || self.create || self.create_new {
                    return Err(io::ErrorKind::InvalidInput.into());
                }
                OpenMode::ReadOnly
            }
            (false, true) => OpenMode::WriteOnly(WriteOptions {
                should_truncate: self.truncate,
                should_append: self.append,
                create_options: if self.create || self.create_new {
                    Some(CreateOptions {
                        permissions: self.permissions.inner,
                        create_only: self.create_new,
                    })
                } else {
                    None
                },
            }),
            (true, true) => OpenMode::ReadWrite(WriteOptions {
                should_truncate: self.truncate,
                should_append: self.append,
                create_options: if self.create || self.create_new {
                    Some(CreateOptions {
                        permissions: self.permissions.inner,
                        create_only: self.create_new,
                    })
                } else {
                    None
                },
            }),
        };
        let path = path.as_ref().as_unix_str().as_bytes().to_owned();

        let result = crate::sys::open(path, open_mode).await;
        match result {
            Ok(descriptor) => Ok(File::from_descriptor(descriptor)),
            Err(error) => Err(error.into()),
        }
    }
}

impl File {
    /// Opens the file in read-only mode.
    pub async fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        OpenOptions::new().read(true).open(path).await
    }

    /// Opens the file in write-only mode.
    pub async fn create<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(path)
            .await
    }

    /// Attempts to sync all OS-internal data to disk.
    pub async fn sync_all(&self) -> io::Result<()> {
        crate::sys::flush(self.descriptor.0).await?;
        Ok(())
    }

    /// Attempts to sync all OS-internal data to disk, except that it may not
    /// synchronize file metadata.
    pub async fn sync_data(&self) -> io::Result<()> {
        self.sync_all().await
    }

    /// Queries metadata about the underlying file.
    pub async fn metadata(&self) -> io::Result<Metadata> {
        let result = crate::sys::read_file_metadata(self.descriptor.0).await;

        let metadata = match result {
            Ok(metadata) => metadata,
            Err(error) => return Err(error.into()),
        };

        Ok(Metadata { inner: metadata })
    }

    /// Creates a new `File` instance that shares the same underlying file
    /// descriptor.
    pub async fn try_clone(&self) -> io::Result<Self> {
        let new_descriptor = crate::sys::duplicate(self.descriptor.0).await?;
        Ok(Self::from_descriptor(new_descriptor))
    }

    /// Changes the permissions on the underlying file.
    pub async fn set_permissions(&self, permissions: Permissions) -> io::Result<()> {
        let result = crate::sys::set_file_permissions(self.descriptor.0, permissions.inner).await;

        match result {
            Ok(_) => Ok(()),
            Err(error) => Err(error.into()),
        }
    }

    pub(crate) fn from_descriptor(descriptor: FileDescriptorId) -> Self {
        Self {
            descriptor: Descriptor::from(descriptor),
            read_future: RefCell::new(None),
            write_future: RefCell::new(None),
            flush_future: RefCell::new(None),
            close_future: RefCell::new(None),
            seek_future: RefCell::new(None),
        }
    }
}

impl io::PollRead for &'_ File {
    fn poll_read(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        let descriptor = self.descriptor.as_raw();
        let read_future_lock = &mut *self.read_future.borrow_mut();
        let read_future = read_future_lock
            .get_or_insert_with(|| crate::sys::raw::read(descriptor, buffer.len()).into());

        let result: ReadResult = match Pin::new(read_future).poll(context) {
            Poll::Ready(result) => {
                *read_future_lock = None;
                let result = result.unwrap();
                serde_wasm_bindgen::from_value(result).unwrap()
            }
            Poll::Pending => return Poll::Pending,
        };

        match result {
            Ok(copy_from) if copy_from.len() <= buffer.len() => {
                buffer[..copy_from.len()].copy_from_slice(&copy_from);
                Poll::Ready(Ok(copy_from.len()))
            }
            Err(IoErrorKind::InvalidOffset) => Poll::Ready(Ok(0)),
            Ok(_) => Poll::Ready(Err(io::ErrorKind::InvalidInput.into())),
            Err(error) => Poll::Ready(Err(error.into())),
        }
    }
}

impl io::PollWrite for &'_ File {
    fn poll_write(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<io::Result<usize>> {
        let descriptor = self.descriptor.as_raw();
        let write_future_lock = &mut *self.write_future.borrow_mut();
        let write_future = write_future_lock
            .get_or_insert_with(|| crate::sys::raw::write(descriptor, buffer.to_vec()).into());

        let result: WriteResult = match Pin::new(write_future).poll(context) {
            Poll::Ready(result) => {
                *write_future_lock = None;
                let result = result.unwrap();
                serde_wasm_bindgen::from_value(result).unwrap()
            }
            Poll::Pending => return Poll::Pending,
        };

        match result {
            Ok(written) => Poll::Ready(Ok(written)),
            Err(IoErrorKind::InvalidOffset) => Poll::Ready(Ok(0)),
            Err(error) => Poll::Ready(Err(error.into())),
        }
    }

    fn poll_flush(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        let descriptor = self.descriptor.as_raw();
        let flush_future_lock = &mut *self.flush_future.borrow_mut();
        let flush_future =
            flush_future_lock.get_or_insert_with(|| crate::sys::raw::flush(descriptor).into());

        let result: FlushResult = match Pin::new(flush_future).poll(context) {
            Poll::Ready(result) => {
                *flush_future_lock = None;
                let result = result.unwrap();
                serde_wasm_bindgen::from_value(result).unwrap()
            }
            Poll::Pending => return Poll::Pending,
        };

        match result {
            Ok(()) => Poll::Ready(Ok(())),
            Err(error) => Poll::Ready(Err(error.into())),
        }
    }

    fn poll_close(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        let descriptor = self.descriptor.as_raw();
        let close_future_lock = &mut *self.close_future.borrow_mut();
        let close_future =
            close_future_lock.get_or_insert_with(|| crate::sys::raw::close(descriptor).into());

        let result: CloseResult = match Pin::new(close_future).poll(context) {
            Poll::Ready(result) => {
                *close_future_lock = None;
                let result = result.unwrap();
                serde_wasm_bindgen::from_value(result).unwrap()
            }
            Poll::Pending => return Poll::Pending,
        };

        match result {
            Ok(()) => Poll::Ready(Ok(())),
            Err(error) => Poll::Ready(Err(error.into())),
        }
    }
}

impl io::PollSeek for &'_ File {
    fn poll_seek(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        seek_from: io::SeekFrom,
    ) -> Poll<io::Result<u64>> {
        let descriptor = self.descriptor.as_raw();
        let seek_future_lock = &mut *self.seek_future.borrow_mut();
        let seek_future = if let Some(future) = seek_future_lock {
            future
        } else {
            let seek_from = serde_wasm_bindgen::to_value(&match seek_from {
                io::SeekFrom::Start(start) => {
                    let start = start.try_into().map_err(|_| io::ErrorKind::InvalidInput)?;
                    SeekFrom::Start(start)
                }
                io::SeekFrom::End(offset) => {
                    let offset = offset.try_into().map_err(|_| io::ErrorKind::InvalidInput)?;
                    SeekFrom::End(offset)
                }
                io::SeekFrom::Current(offset) => {
                    let offset = offset.try_into().map_err(|_| io::ErrorKind::InvalidInput)?;
                    SeekFrom::Current(offset)
                }
            })
            .unwrap();
            *seek_future_lock = Some(crate::sys::raw::seek(descriptor, seek_from).into());
            seek_future_lock.as_mut().unwrap()
        };

        let result: SeekResult = match Pin::new(seek_future).poll(context) {
            Poll::Ready(result) => {
                *seek_future_lock = None;
                let result = result.unwrap();
                serde_wasm_bindgen::from_value(result).unwrap()
            }
            Poll::Pending => return Poll::Pending,
        };

        match result {
            Ok(offset) => Poll::Ready(Ok(offset as u64)),
            Err(error) => Poll::Ready(Err(error.into())),
        }
    }
}

impl From<File> for Descriptor {
    fn from(file: File) -> Descriptor {
        file.descriptor
    }
}

impl io::PollRead for File {
    fn poll_read(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        Pin::new(&mut &*self).poll_read(context, buffer)
    }
}

impl io::PollWrite for File {
    fn poll_write(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<io::Result<usize>> {
        Pin::new(&mut &*self).poll_write(context, buffer)
    }

    fn poll_flush(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut &*self).poll_flush(context)
    }

    fn poll_close(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut &*self).poll_close(context)
    }
}

impl io::PollSeek for File {
    fn poll_seek(
        self: Pin<&mut Self>,
        context: &mut Context<'_>,
        seek_from: io::SeekFrom,
    ) -> Poll<io::Result<u64>> {
        Pin::new(&mut &*self).poll_seek(context, seek_from)
    }
}

impl AsRawFd for File {
    fn as_raw_fd(&self) -> u32 {
        self.descriptor.as_raw()
    }
}

impl IntoRawFd for File {
    fn into_raw_fd(self) -> RawFd {
        let (FileDescriptorId(descriptor),) = self.descriptor.destructure();
        descriptor
    }
}

impl FromRawFd for File {
    unsafe fn from_raw_fd(descriptor: RawFd) -> Self {
        Self::from_descriptor(FileDescriptorId(descriptor))
    }
}
