use crate::{io, path::Path, stream::TryStreamExt};
use futures::future::LocalBoxFuture;
use std::convert::AsRef;

/// Removes a file from the filesystem.
pub async fn remove_file<P: AsRef<Path>>(path: P) -> io::Result<()> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::unlink_file(path).await;

    result.map_err(Into::into)
}

/// Removes a directory from the filesystem.
pub async fn remove_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    let path = path.as_ref().as_unix_str().as_bytes().to_owned();
    let result = crate::sys::remove_directory(path).await;

    result.map_err(Into::into)
}

/// Removes a directory at this path, after removing all its contents. Use
/// carefully!
pub async fn remove_dir_all<P: AsRef<Path>>(path: P) -> io::Result<()> {
    remove_dir_all_recursive(path.as_ref()).await
}

fn remove_dir_all_recursive(path: &Path) -> LocalBoxFuture<'_, io::Result<()>> {
    Box::pin(async move {
        let mut children = super::read_dir(path).await?;
        while let Some(child) = children.try_next().await? {
            if child.file_type()?.is_dir() {
                remove_dir_all_recursive(&child.path()).await?;
            } else {
                remove_file(&child.path()).await?;
            }
        }
        remove_dir(path).await
    })
}
