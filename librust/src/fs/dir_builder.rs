use crate::{
    fs::Permissions,
    io,
    path::{Path, PathExt},
};
use futures::future::LocalBoxFuture;
use std::convert::AsRef;

#[derive(Debug)]
pub struct DirBuilder {
    is_recursive: bool,
    mode: Permissions,
}

impl DirBuilder {
    /// Creates a new set of options with default mode/security settings for all
    /// platforms and also non-recursive.
    #[allow(clippy::new_without_default)]
    pub fn new() -> DirBuilder {
        DirBuilder {
            is_recursive: false,
            mode: Permissions::from_mode(0o777),
        }
    }

    /// Indicates that directories should be created recursively, creating all
    /// parent directories. Parents that do not exist are created with the same
    /// security and permissions settings.
    /// This option defaults to `false`.
    pub fn recursive(&mut self, recursive: bool) -> &mut Self {
        self.is_recursive = recursive;
        self
    }

    /// Creates the specified directory with the options configured in this
    /// builder.
    ///
    /// It is considered an error if the directory already exists unless
    /// recursive mode is enabled.
    pub async fn create<P: AsRef<Path>>(&self, path: P) -> io::Result<()> {
        self._create(path.as_ref()).await
    }

    /// Sets the mode to create directories with. Defaults to `0o777`.
    pub fn mode(&mut self, mode: u32) -> &mut Self {
        self.mode = Permissions::from_mode(mode);
        self
    }

    async fn _create(&self, path: &Path) -> io::Result<()> {
        if self.is_recursive {
            self.create_dir_all(path).await
        } else {
            self.mkdir(path).await
        }
    }

    async fn mkdir(&self, path: &Path) -> io::Result<()> {
        let path = path.as_unix_str().as_bytes().to_owned();
        crate::sys::create_directory(path, self.mode.inner)
            .await
            .map_err(Into::into)
    }

    fn create_dir_all<'a>(&'a self, path: &'a Path) -> LocalBoxFuture<'a, io::Result<()>> {
        Box::pin(async move {
            if path == Path::new("") {
                return Ok(());
            }

            match self.mkdir(path).await {
                Ok(()) => return Ok(()),
                Err(ref e) if e.kind() == io::ErrorKind::NotFound => {}
                Err(_) if path.is_dir().await => return Ok(()),
                Err(e) => return Err(e),
            }
            match path.parent() {
                Some(p) => self.create_dir_all(p).await?,
                None => {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        "failed to create whole tree",
                    ));
                }
            }
            match self.mkdir(path).await {
                Ok(()) => Ok(()),
                Err(_) if path.is_dir().await => Ok(()),
                Err(e) => Err(e),
            }
        })
    }
}

/// Creates a new, empty directory at the provided path.
pub async fn create_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    DirBuilder::new().create(path.as_ref()).await
}

/// Recursively create a directory and all of its parent components if they are missing.
pub async fn create_dir_all<P: AsRef<Path>>(path: P) -> io::Result<()> {
    DirBuilder::new()
        .recursive(true)
        .create(path.as_ref())
        .await
}
