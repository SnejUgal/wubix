use crate::io::{self, PollWrite, Write};
use futures::{future::LocalBoxFuture, io::BufWriter};
use std::{
    cell::{Cell, RefCell},
    fmt::{self, Debug},
    future::Future,
    pin::Pin,
    rc::Rc,
    task::{Context, Poll},
};

pub struct LineWriter<W> {
    inner: Rc<RefCell<BufWriter<W>>>,
    write_future: Option<LocalBoxFuture<'static, io::Result<usize>>>,
    should_flush: Rc<Cell<bool>>,
}

impl<W: Write + Unpin + 'static> LineWriter<W> {
    /// Creates a new `LineWriter`.
    pub fn new(inner: W) -> Self {
        Self::with_capacity(1024, inner)
    }

    /// Creates a new `LineWriter` with a specified capacity for the internal
    /// buffer.
    pub fn with_capacity(capacity: usize, inner: W) -> Self {
        Self {
            inner: Rc::new(RefCell::new(BufWriter::with_capacity(capacity, inner))),
            should_flush: Rc::new(Cell::new(false)),
            write_future: None,
        }
    }
}

impl<W: Write + Unpin + 'static> PollWrite for LineWriter<W> {
    fn poll_write(
        mut self: Pin<&mut Self>,
        context: &mut Context<'_>,
        buffer: &[u8],
    ) -> Poll<io::Result<usize>> {
        let mut future = if let Some(future) = &mut self.write_future {
            future
        } else {
            let writer = Rc::clone(&self.inner);
            let should_flush = Rc::clone(&self.should_flush);
            let buffer = buffer.to_owned();

            self.write_future = Some(Box::pin(async move {
                if should_flush.get() {
                    writer.borrow_mut().flush().await?;
                    should_flush.set(false);
                }

                let index = if let Some(index) = buffer.iter().copied().rposition(|x| x == b'\n') {
                    index
                } else {
                    return writer.borrow_mut().write(&buffer).await;
                };

                let count = match writer.borrow_mut().write(&buffer[..=index]).await {
                    Ok(count) => count,
                    err => return err,
                };
                should_flush.set(true);
                if writer.borrow_mut().flush().await.is_err() || count != index + 1 {
                    return Ok(count);
                }

                Ok(count + (writer.borrow_mut().write(&buffer[index + 1..]).await).unwrap_or(0))
            }));

            self.write_future.as_mut().unwrap()
        };

        match Future::poll(Pin::new(&mut future), context) {
            result @ Poll::Ready(_) => {
                self.write_future = None;
                result
            }
            Poll::Pending => Poll::Pending,
        }
    }

    fn poll_flush(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        let should_flush = Rc::clone(&self.should_flush);

        match Pin::new(&mut *self.inner.borrow_mut()).poll_flush(context) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(result) => {
                should_flush.set(false);
                Poll::Ready(result)
            }
        }
    }

    fn poll_close(self: Pin<&mut Self>, context: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut *self.inner.borrow_mut()).poll_close(context)
    }
}

impl<W: Debug> Debug for LineWriter<W> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("LineWriter")
            .field("inner", &self.inner)
            .field("should_flush", &self.should_flush)
            .finish()
    }
}
